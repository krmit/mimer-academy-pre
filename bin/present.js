#!/usr/bin/node
"use strict"

require("colors");
const markdown=require("markdown").markdown;
const print = require('sprintf-js').sprintf;
const fs = require('fs');
const {promisify} = require('util');

fs.readFile = promisify(fs.readFile);

const argv = require('yargs')
    .usage('Usage: $0')
    .alias('f', 'file')
    .nargs('f', 1)
    .describe('f', 'File to view.')
    .help('h')
    .alias('h', 'help')
    .epilog('copyright 2018')
    .argv;

const main = async function() {
    let data = await fs.readFile(argv["file"], "utf-8");
    console.log(markdown.toHTML(data));
}

main();
