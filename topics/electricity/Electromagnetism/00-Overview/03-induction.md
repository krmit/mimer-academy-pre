---
author: krm
created: 2018-09-03
links:
  - an-introduction-to-induction
  - practical-and-fun-examples-of-induction
---
![se]
# Vad är induktion?

Att en växelström uppstår om en ledare utsätt för magnetiskt flöde.
  * Magnetiskt flöde är ett magnätfält som ändras.
  * Är grunden för de flesta elektriska tillämpningar.
  * Upptäcktes av Michael Faraday
  * Beskrev i Maxwells ekvationer av James Clerk Maxwells och Oliver Heaviside.
