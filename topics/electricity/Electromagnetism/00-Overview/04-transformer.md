---
author: krm
created: 2018-09-03
links:
  - introduktion-to-three-phase-transformers
---
![se]
# Vad är transformator?

Två spolar intil varandra. Den ena spolen induserar en ström i den andra.

  * Kan ändra späning på en växelström.
  * Kommer då också påpverka strömmen.
  * Mer späning, mindre ström och tvärt om.
  * Antalet varv i spolarna avgör hur transformatorn fungerar.





