---
author: krm
created: 2018-09-03
links:
  - build-a-single-DC-motor
  - how-does-an-Induction-Motor-work
  - nikola-Tesla-vs-Thomas-Edison
  - rap-Battles:-Nikola-Tesla-vs-Thomas-Edison
---
![se]
# Elektriska motorer

En ström kan ge upphov till ett magnetsikt fält som på olika sätt kan bli till ett magnetiskt flöde som i sin tur kan sätta igång en rörelse.

  * Det finns väldigt många olika typer av elektriska motorer.
  * Både baserade på DC och AC.
  * Dessa kan ha väldigt många olika egenskaper.
  * En AC moter som är väldigt användbar kallas induktons moter eller Tesla motor.





