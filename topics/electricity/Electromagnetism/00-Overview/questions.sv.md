# Om Elektromagnetism

  1. Vilket av följande sätt ökar inte magnetfältstyrka i en spole?    
    * Öka spolens hastighet.    
    * Mer ström går genom spolen.     
    * Flera varv i spolen.    
    * En järnkärna sätts in i spolen.
  2. Vilket är det minsta antal spolar du behöver ha i en transformator?      
    * 1    
    * 2  
    * 3    
    * 4
  3. Vad är det som överför energi mellan spolar i en transformator.     
    * Eter    
    * Telepati
    * Ström    
    * Magnetfält    
  4. Vad menas med generatorprincipen?   
    * Effekt lika med arbete gånger tid.     
    * När en elektrisk ledare vrids runt i ett magnetfält induceras en spänning i ledaren.    
    * Jordens roterande  magnetfält.    
    * Ett annat namn på motorprincipen.

  5. Vad menas med motorprincipen?   
    * Ett rörligt elektrisktfält ger upphov till ett magnetiskt flöde som kan orsaka rörelse.  
    * Att elektriska motorer alltid kommer vara den mest effektiva motor typen.    
    * Att Atlas får jorden att gå runt.    
    * Ett annat namn på generatorprincipen.

  6. Induktans är en viktig relation mellan vadå?
    * Elektrisktfält och ström.
    * Magnetiskt flöde och magnetiska fält.
    * Politiker och väljare.
    * Ström och magnetiskt flöde

  7. Vilken av följande påståenden är inte anledning till att man föredrar växelström över likström i elnät?
    * Det är enkelt att skapa AC med hjälp av en generator.
    * En AC motor är väldigt effektiv.
    * DC är farligare än AC, lättare att döda med.
    * Med hjälp av en transformator kan man enkelt ändra spänningen på en växelström.
