---
author: krm
created: 2018-09-03
---
![se]
# Några praktiska beräkningar om Elektrisk Enerig

Dessa formler behöver du kunna:

  * `E=Pt`                    Beräkna energin utfrån effekt och tid.
  * `P=E/t`                   Beräkna energin utfrån effekt och tid.
  * `P=UI`                    Beräkna effekten utifrån ström och späning.
  * `P=U^2/R`                 Med hjälp av Ohm's lag.
  
OBS: Vid AC är det alltid den effektiva späningen som ska användas.

