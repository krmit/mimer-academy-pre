---
author: krm
created: 2018-09-03
---
![se]
# Några definitioner för Energi

Dessa storheter är vanligt förekommande:

  * `P`                    Effekt, mätts inom elteknik i Watt("W").
  * `t`                    Tid, ofta används enheten timmar("h")
  * `E`                    Energi, mätts ofta i "kWh".

