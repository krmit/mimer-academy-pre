---
author: krm
created: 2018-06-29
questions:
    - type: none
    - type: none
    - type: none
    - type: none
    - type: none
    - type: none
    - type: none
    - type: none
    - type: none
    - type: none
---
![se]

  1. Vad är energi?
  2. Varför pratar vi ofta om effekt istället för energi när vi diskuterar en elektrisk komponeter?
  3. Hur är elektrisk energi i förhållande till andra typer av energi?
  4. Hur beräknar du energin om du känner till effekten?      
    * Effekten upphöjd i två delat med två.
    * Effekten multiplicera med tid.
    * Det går inte att räkna ut.    
    * Summera alla strömmar.
  5. Vad kan en transformator användas till?    
    * Förändra spänning på växelström.    
    * Förändra spänning på likström.    
    * Förändra frekvensen.    
    * Till att fungera som en automatisk strömbrytare.
  6. Vad menas med AC?       
    * Samma sak som DC.    
    * Ena halvan av en musikgrupp.
    * Växelström.    
    * En enkel DAC
  7. Vilken formel använder vi för att beräkna effekten i en elektrisk krets.       
    * E=Ut
    * P=UI     
    * P=RI    
    * P=Ut
  8. Vilken av följande enheter är till för energi?    
    * Nm    
    * A    
    * W
    * Wh    

  9. Du har en 40W lampa. Hur mycket energi drar den på 3 timmar?      
    * 1,2 kWh    
    * 120 Wh  
    * 12 mA    
    * 120 W

  10. Över ett motstånd på 200 Ohm går en ström på 50 mA. Hur mycket värmeenergi bildas på två timmar?    
    * 0,56 Wh   
    * 0.2 Wh    
    * 1 Wh
    * 2 kWh
