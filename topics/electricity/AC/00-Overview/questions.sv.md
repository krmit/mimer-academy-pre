# Grundläggande om AC

  1. Vilken form har växelströmmen i vårt elnät?
  2. Vilka är fördelen med växelström jämfört med likström för att överföra elkraft?
  3. Vilka två bokstavsförkortningar brukar användas för växelström och likström?
  4. Skriv formeln för växelström.
  5. Vad är amplituden av en våg?
  6. Vad är  frekvens på en våg?
  7. Vad används ett oscilloskop till?
  8. Om du känner till toppspänningen, `U_t`, över ett motstånd med resistansen `R`. Vad är då toppströmmen, `I_t`?
    * I_t=U_t 
    * I_t=RU_t/2
    * I_t=U_t/R  
    * I_t=R^2*U_t
  9. Om du känner till toppspänningen, `U_t`, över ett motstånd med resistansen `R`. Vad är då den effektiva spänningen `U_eff`?
    * U_eff=U_t/2   
    * U_eff=U_t*2    
    * U_eff=U_t`   
    * U_eff=U_t/sqrt(2)
  10. Om du känner till topp-till-topp spänningen, `U_tt`. Vad är då den toppspänningen `U_t`?
    * U_t=U_tt*2 
    * U_t=U_tt/2
    * U_t=U_tt
    * U_t=U_tt/sqrt(2) 
  11. Om vi har en växelström på 100 Hz. Hur lång är periodtiden.
    * 10 ms    
    * 10 s    
    * 1 ms    
    * 100 ms
  12. Om vi har en periodtid på 20 ms. Hur stor är då frekvensen i Hz?
    * 5 Hz    
    * 50 Hz    
    * 5 kHz   
    * 1337 Hz
  13. Vilken form har växelströmmen i vårt elnät om vi tittar på det med ett oscilloskop?
    * sågtandvåg   
    * sinusvåg 
    * rättlinje    
    * fyrkantsvåg
  14. Vad innebär effektivspänningen när vi pratar om växelström?       
    * Verkningsgrad i en motor.    
    * Spänningen som vi använder i formeln P=UI.
    * Verkningsgrad i en generator.    
    * Den högsta spänningen som växelspänningen har.
  15. Om du tittar i ett oscilloskop, vad motsvarar då amplituden på vågen?
    * Toppspäningen    
    * Toppströmen.    
    * Den effektiva spänningen.    
    * Energin.
  16. På oscilloskop vilken enhet är det på x-axeln?    
    * tid    
    * energi    
    * ström    
    * spänning
  17. Om du mäter en AC spänning med en multimeter vilken spänning vissa?    
    * Toppspäningen   
    * Topp till Topp spänning    
    * Effektivspänning
    * Några slumpmässiga tal
