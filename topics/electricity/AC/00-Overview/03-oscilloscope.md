---
author: krm
created: 2018-09-03
---
![se]
# Vad är Oscilloskop?

  * Ett instrument som visualiserar växelström
  * Har en x-axel som visar tid.
  * Har en y-axel som visar späning.

