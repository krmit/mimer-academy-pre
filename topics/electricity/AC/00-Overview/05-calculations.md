---
author: krm
created: 2018-09-03
---
![se]
# Några praktiska beräkningar om AC

Dessa formler behöver du kunna:

  * `T=1/f`                    Beräkna periodtiden utifrån frekvensen.
  * `f=1/T`                    Beräkna frekvensen utifrån periodtiden.
  * `U_t=U_tt/2`               Beräkna topspäning utifrån top till top späning.
  * `U_eff=sqrt(U_t)`          Beräkna den effektiva späningen. Detta gäller för en sinusvåg.

