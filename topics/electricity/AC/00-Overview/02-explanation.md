---
author: krm
created: 2018-09-03
links:
  - the-difference-between-AC-and-DC
  - sine-wave
---
![se]
# Vad är Växelström?

  * En ström som ändras sin späning och ström styrka.
  * Den kan vara oregelbunden och överföra information, då kallar vi dem signal.
  * Eller kan den ha en vågform så som fyrkantsvågor, sågtandsvågor eller sinusvågor.
  * En sinusvåg är vanligaste.
  * Genom att det går så pass fort så kommer växelströmmen bete väldigt likt likström.

***

Våglära är ett intressant ämne som du kommer få ägna många veckor åt på universitet. En våg är antingen en sinusvåg eller en superposition av sinusvågor, det vill säga flera sinusvågor som vi har adderad. En sinusvåg kan vi beskriva med formeln:

`y=Asin(wt+p)`

Utifrån formeln får vi följande intressanta egenskaper på en våg:

Amplituden(A), det vill säga hur hög vågen är.
Vinkelhastighet(w)(“Angular frequency”) är `2*pi*f`. Där f är frekvensen. Alla vågor har en frekvens. Observera att varje våg också har en periodtid(T) som beräknas som T=1/f.
Fasförskjutning(p) är hur förskjuten vågen är topp är ifrån sitt ursprungliga läge.





