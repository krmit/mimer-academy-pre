---
author: krm
created: 2018-09-03
---
![se]
# Några praktiska beräkningar om AC

Dessa formler behöver du kunna:

  * `T`                    Periodtiden, hur lång vågen är.
  * `f`                    Frekvensen, det vill säga antal vågor på en sekund.
  * `U_t`                  Topspäningen, späning ifrån toppen på vågen till vågens mitt.
    * Samma som  amplituden.
  * `U_tt`        		   Skillnadnen i spänning mellan högsta och lägsta toppen på vågen.       
  * `U_eff`                Den effektiva späningen, den späning som används för att beräkna effekten av vågen.

Den effektiva späningen beräknas olika beronde på vilken vågform det är. En multimeter eller likande instrument kommer alltid att visa den effektiva späningen, vilken kan vara rätt så förvirande om du tittar på vågen i ett osciloskop eller känner till vågen amplitud.

