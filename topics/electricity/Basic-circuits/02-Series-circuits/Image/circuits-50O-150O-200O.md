---
author: krm
licensing: Same as project
created: 2018-09-14
tag: circuits
source-type: link
source: http://www.falstad.com/circuit/circuitjs.html?cct=$+20+0.000005+10.20027730826997+50+5+50%0Av+144+112+320+112+0+0+40+40+0+0+0.5%0Ar+320+112+224+272+0+50%0Ar+224+272+144+112+0+200%0Ax+288+205+358+208+4+24+15%5Csk%CE%A9%0Ax+110+206+165+209+4+24+5%5Csk%CE%A9%0A
---
![se]
#  En enkel seriekoppling med resistorer på 5kΩ och 15kΩ
