---
author: krm
created: 2018-09-03
links:
  - current-in-series-circuits
---
![se]
# Vad är en Seriekoppling?

Vi beräknar den ersättningsresistansen genom denna formel: `R=R_1+R_2+R_3 …`

  * Vi får alltså högre resistans i kretsen för varje motstånd som vi seriekopplar.
  * Den totala resistansen måste vara högre det motstånd med den högsta resistansen.
  * Spänningen fördelar sig över motstånden. Detta kallar vi för spänningsfallet över ett motstånd. Alternativt kan man också säga del spänning.
  * Strömmen kommer däremot vara lika stor i hela kretsen.






