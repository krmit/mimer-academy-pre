---
author: krm
created: 2018-10-17
comment: "Efter du räknat uppgiften, simulera den i falstad och se om du räknat rätt. Tänk på prefixen."
questions:
    - type: calculation
      answer: 15 Ω
    - type: calculation
      answer: 6 kΩ
    - type: calculation
      answer: 1 kΩ
    - type: calculation
      answer: 1.5 kΩ
    - type: calculation
      answer: 400 Ω
      image: circuits-50O-150O-200O.png
      image-size: normal
    - type: calculation
      answer: 20 kΩ
      image: circuits-5kO-15kO.png
      image-size: normal
    - type: calculation
      answer: 2,8 MΩ
      image: circuits-6x300kO.png
      image-size: normal
    - type: calculation
      answer: 7 V
    - type: calculation
      answer: 20 V
---
![se]

  1. Om R_1=10 Ω och R_2=5 Ω , vad är då ersättningsresistansen?
  2. Om R_1=1 kΩ och R_2=5 kΩ , vad är då ersättningsresistansen?
  3. Om R_1=150 Ω och R_2= 850 Ω , vad är då ersättningsresistansen?
  4. Om R_1=750 Ω och R_2= 750 Ω , vad är då ersättningsresistansen?
  5. Vad är ersättningsresistansen i kretsen nedan?
  6. Vad är ersättningsresistansen i kretsen nedan?
  7. Om varje motstånd nedan 300 kΩ. Vad är ersättningsresistansen i kretsen nedan?
  8. Om vi har en krets med en total spänning på 12 V och två seriekopplade motstånd på R_1=500 Ω och R_2= 700 Ω , vad är då spänningsfallet över R_2?
  9. Om vi har en krets med en total spänning på 30 V och två seriekopplade motstånd på R_1=100 Ω och R_2= 200 Ω , vad är då spänningsfallet över R_2?
