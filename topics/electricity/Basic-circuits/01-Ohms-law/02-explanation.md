---
author: krm
created: 2018-10-17
links:
  - voltage
---
![se]
# Vad är Ohm's lag?

  * `U=I*R`
  * `I=U/R`
  * `R=U/I`
