# Beräkningar med Ohms lag

Efter du räknat uppgiften, simulera den i falstad och se om du räknat rätt. Tänk på prefixen.

  1. Om U=10V och R=5 Ω, vad är då I?
  2. Om U=30V och I=200mA , vad är då R?
  3. Om R=10 kΩ och I=2 A , vad är då U?
  4. I kretsen nedan beräkna strömmen.
  5. Beräkna spänningen i kretsen nedan.
  6. Beräkna resistansen i kretsen nedan.
  7. Vilken ström får vi i kretsen?
  8. Varför hade hänt om vi kopplat upp en liknande krets i verkligheten?
  9. Vilken av följande formler är felaktig enligt Ohms lag?       
    * R=U/I    
    * U=RI    
    * I=U/R
    * I=UR 




