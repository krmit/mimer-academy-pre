---
author: krm
licensing: Same as project
created: 2018-09-10
tag: circuits
source-type: link
source: http://www.falstad.com/circuit/circuitjs.html?cct=$+20+0.000005+10.20027730826997+50+5+50%0Av+240+176+352+176+0+0+40+100+0+0+0.5%0Aw+352+176+352+272+0%0Aw+240+176+240+272+0%0Ar+352+272+240+272+0+4000%0Ax+280+154+316+157+4+12+100%5CsV%0Ax+355+214+383+217+4+12+25%5CsA%0A
---
![se]
# En enkel seriekoppling med batteri med spänning på 100 V och mattar med 25 A
