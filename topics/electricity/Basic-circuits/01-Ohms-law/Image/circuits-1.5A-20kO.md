---
author: krm
licensing: Same as project
created: 2018-09-10
tag: circuits
source-type: link
source: http://www.falstad.com/circuit/circuitjs.html?cct=$+4+0.000005+10.20027730826997+50+5+50%0Av+240+176+352+176+0+0+40+12+0+0+0.5%0Aw+352+176+352+272+0%0Aw+240+176+240+272+0%0Ar+352+272+240+272+0+20000%0Ax+314+167+346+170+4+12+1.5%5CsA%0A
---
![se]
# En enkel seriekoppling med batteri som matar 1.5 A och ett motstånd på 20 kOhm
