---
author: krm
created: 2018-06-29
comment: "Efter du räknat uppgiften, simulera den i falstad och se om du räknat rätt. Tänk på prefixen."
questions:
    - type: calculation
      answer: 2 A
    - type: calculation
      answer: 150 Ω
    - type: calculation
      answer: 20 kΩ
    - type: calculation
      answer: 3 mA
      image: circuits-12V-4kO.png
      image-size: normal
    - type: calculation
      answer: 30 kV
      image: circuits-1.5A-20kO.png
      image-size: normal
    - type: calculation
      answer: 4 Ω
      image: circuits-100V-25A.png
      image-size: normal
    - type: presentation
      image: circuits-short.png
      image-size: normal
    - type: none
---
![se]

  1. Om U=10V och R=5 Ω, vad är då I?
  2. Om U=30V och I=200mA , vad är då R?
  3. Om R=10 kΩ och I=2 A , vad är då U?
  4. I kretsen nedan beräkna strömmen:
  5. Beräkna spänningen i kretsen nedan:
  6. Beräkna resistansen i kretsen nedan:
  7. Tänk dig kretsen nedan och svara på följd frågorna.
    1. Vilken ström får vi i kretsen?
    2. Varför hade hänt om vi kopplat upp en liknande krets i verkligheten?
  8. Vilken av följande formler är felaktig enligt Ohms lag?       
    * R=U/I    
    * U=RI    
    * I=U/R
    * I=UR 




