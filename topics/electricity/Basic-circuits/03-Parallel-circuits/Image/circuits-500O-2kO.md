---
author: krm
licensing: Same as project
created: 2018-10-17
tag: circuits
source-type: link
source: http://www.falstad.com/circuit/circuitjs.html?cct=$+20+0.000005+10.20027730826997+50+5+50%0Av+64+112+240+112+0+0+40+40+0+0+0.5%0Ar+448+688+272+688+0+300000%0Aw+64+112+64+176+0%0Aw+240+112+240+176+0%0Ar+240+176+64+176+0+1000%0Aw+240+176+240+224+0%0Aw+64+176+64+224+0%0Ar+240+224+64+224+0+1000%0Ax+123+214+178+217+4+24+2%5Csk%CE%A9%0Ax+122+165+193+168+4+24+500%5Cs%CE%A9%0A
---
![se]
# En enkel Parallellkoppling med resistorer på 500 Ω och 2 kΩ
