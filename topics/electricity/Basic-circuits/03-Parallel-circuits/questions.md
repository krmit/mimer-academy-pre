---
author: krm
created: 2018-10-17
comment: "Efter du räknat uppgiften, simulera den i falstad och se om du räknat rätt. Tänk på prefixen."
questions:
    - type: calculation
      answer: 6.7 Ω
    - type: calculation
      answer: 225 Ω
    - type: calculation
      answer: 10 kΩ
    - type: calculation
      answer: 375 Ω
    - type: calculation
      answer: 400 Ω
      image: circuits-500O-2kO.png
      image-size: normal
    - type: calculation
      answer: 20 mA
    - type: calculation
      answer: 24 mA
    - type: none
      answer: 3
    - type: none
      answer: 3
---
![se]

  1. Om R_1 = 10 Ω och R_2 = 20 Ω , vad är då ersättningsresistansen i en parallellkoppling?
  2. Om R_1 = 300 Ω och R_2 = 900 Ω , vad är då ersättningsresistansen i en parallellkoppling?
  3. Om R_1 = 20 kΩ och R_2 = 20 kΩ , vad är då ersättningsresistansen i en parallellkoppling?
  4. Om R_1 = 1,5 kΩ och R_2 = 500 Ω , vad är då ersättningsresistansen i en parallellkoppling?
  5. Vad är ersättningsresistansen i kretsen nedan?
  6. Om vi har en krets med en total spänning på 12 V och två parallellkoppling motstånd på R_1=600 Ω och R_2= 600 Ω , vad är då grenströmmen i  R_2?
  7. Om vi har en krets med en total spänning på 120 V och två parallellkoppling motstånd på R_1=10 kΩ och R_2= 5 kΩ , vad är då grenströmmen i  R_2?
  8. Vad innebär att vi belastar en krets med en motstånd?
    * Ett motstånd tynger ned kretsen så apparaten riskerar att gå sönder.
    * Ett motstånd seriekopplade med kretsen.
    * Ett motstånd parallellkopplas med kretsen.
    * Att ett motstånd införs för att minska strömmen i en krets.
  9. Om vi belastar en krets vad innebär detta för den totala strömmen i kretsen?
    * Ingen skillnad
    * Den minskar
    * Den ökar
    * Den omvandlas från DC till AC

