---
author: krm
created: 2018-10-17
links:
  - voltage-in-Parallel-circuits
---
![se]
# Vad är en parallellkoppling?

Vi beräknar den ersättningsresistansen genom denna formel:

`R=1(1/R_1+1/R_2+1/R_3 …)`

  * Vi får alltså lägre resistans i kretsen för varje motstånd som vi parallellkopplar.
  * Den totala resistansen måste vara lägre än det motstånd med den lägsta resistansen.
  * Spänningen kommer vara lika stor i hela kretsen.
  * Strömmen fördelar sig däremot över motstånden. De mindre strömmar som passar genom ett motstånd kallas för grenströmmar.





