# Klient server komunikation

  1. Vad menar vi med “request” i klient-server modellen?
  2. Vad menar vi med “respons” i klient-server modellen?
  3. På vilket sätt får en server och en klient kommunicera på webben?
  4. Vad kan detta innebära för problem?
  5. Vad innebär det att server gör “polling”?
  6. Vad innebär att ett protokoll är “stateless”?




