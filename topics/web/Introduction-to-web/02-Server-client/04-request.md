---
author: krm
created: 2018-10-02
---
![se]
# Anrop

Klienten anropar servern. I anropen kan det finns mycket information till servern.

   * Anropen innehåller en URL 
   * Vilken typ av klient och dator
   * Tid när anropet gjordes.
   * Var någonstans svaret ska skickas.
   * Vilken kryptering som ska använs
   * Data som klienten vill skicka till servern.
