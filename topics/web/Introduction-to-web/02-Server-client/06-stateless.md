---
author: krm
created: 2018-10-02
---
![se]
# Stateless

Stateless betyder att ett protokoll behandlar varje anrop separat.

   * Detta gör att protokollet inte hjälper servern eller klienten att komma ihåg vad som hänt.
   * Men det finns finns flera metoder att komma runt de problem som då skapas.
    * Webbkakor("cookies") är ett sätt att komma runt detta.
    * En webbkaka sparar nämligen lite data hos klienten.

***

Kunde tyvärr inte hitta någon bra översättning till svenska.





