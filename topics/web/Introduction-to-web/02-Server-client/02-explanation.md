---
author: krm
created: 2018-10-02
links:
  - web-servers-communication
  - client-server-model
---
![se]
# På vilket sätt komuncerar en klient med en server?

Den modell som används för webben kallas klient server modele("Client–server model")

   * Klienten är ett program på din lokala dator
   * Från din klient görs ett anrop("request") till servern.
   * Detta besvara servern med ett svar("respons") till klienten.
   * Klienten tar emot detta svar. 








