---
author: krm
created: 2018-10-02
---
![se]
# Begräsningar

Detta innebär att vi har begräsningar

   * Server anropar alldrig klienten.
   * När server har svarat på ett anrop är den klar.
   * Det innebär att servern "glömmer bort" klienten.
     * Fast i praktiken finns det många lösningar på detta.








