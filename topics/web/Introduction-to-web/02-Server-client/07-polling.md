---
author: krm
created: 2018-10-02
---
![se]
# Polling

Eftersom en server inte kan fråga en klient något blir det problem om servern behöver säga något till klienten.

   * Ett exempel på detta skulle vara en chatt app.
   * För om server får besket av en klient att den vill prata med en anna klient.
     * Hur gör den då?
   * En lösning är att alla klienter anropar server ofta, för då kan server svara.
   * Detta kallas "polling"
   * Det är inget effektivt sätt att komunicera. 








