---
author: krm
created: 2018-10-02
---
![se]
# Om hur en server kommunicerar med en kleint

Vi måste förstå hur en server kommunicerar med en klient för att kunna utveckla bra web applikationer

  * Det är viktig för att webben ger oss många begräsningar.
  * Att förstå dessa begräsningar får oss att förstå varför webben är som den är.
  * Genom att förstå lösningar på dessa problem kan vi skapa bra applikationer.





