---
author: krm
created: 2018-10-09
---
![se]
# Filnamn

Sist i sökvägen kan det vara ett filnamn.

   * På en statisk server motsvarar detta en fil i filsystemet.
   * Exempel: *http://www.htsit.se/a/topics/**index.html** *
   * Filändelsen kan användas av webbser ver för att sätta MIME type, se avsnitt om HTTP.
