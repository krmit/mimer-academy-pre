---
author: krm
created: 2018-10-09
---
![se]
# DNS

"Domain Name System" är servrar som hanterar domänanen och kopplar iop den med så kallade IP-adersser på internet. IP-adresserna kan sen användas för att hitta rätt servrer.

   * Toppdomänen och domänen kontrolleras av de organsiationer och företag som säljer domäner. 
   * Subdomänerna till en domän du eger kontroleras av dig.
   * Systemmet är hierarkiskt, servrarana frågar varandra i ordning för att få fram en IP-adress. 
   * Användarna frågar en DNS som sedan frågar en DNS server högre upp i heirarkin.
   * Om ingen server vet svaret börjar topppdoämenen fråga namnservrar som är ansluta till den.
   








