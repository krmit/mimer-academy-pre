# Om URL

  1. Vad är en URL?
  2. Förklara de olika dellarna i url:n http://valhall.htsit.se/krm/main.html:8080?
  3. Vad är en subdomän?
  4. Vad är en toppdomän?
  5. Förklara vad DNS tekniken används till.
  6. Ge exempel på protokol som vanligen används i en URL.
  7. Vem kontrollerar toppdomänen "se"?
  8. Ge minst tre exempel på toppdoämner och vad man kan förvänta sig hitta på dessa.
  9. Om du har ett domännamn men har två olika servrar. Vad använder du för att kunna göra URL:er som ledare till de olika servrarna?
 10. Du har en server men på den har du två olika webservrar. Hur kan du skilja på dem med en URL?
