---
author: krm
created: 2018-10-09
---
![se]
# Protokol

Först på en fullständig URL har vi ett protokol.

   * Det vanligaste protokollen är "http" eller "https".
   * Exempel: * **https**://xkcd.com/303/*
   * Andra exempel är svårare att hitta, men några påhittade exempel som skulle kunna finnas:
   * Exempel: * **ftp:**//ftp.htsit.se* - * **file:**///home/krm/dev/a/index.html*







