---
author: krm
created: 2018-10-09
---
![se]
# Vad är en URL?

En URL lokaliserar en webresurs på en dator i ett datornätverk.

   * Förkortning av "Uniform Resource Locator", kallas ofta för web adress.
   * Den är uppbyggd av många mindre delar som hanteras av webklienten och webserver.
   * Varjdel har sin egen syntax och betydelse.
   * Exempel: *http://htsit.se/r/web/robot-game.html?level=simplast*
   * Exempel: *https://www.xkcd.com/378/* - *https://www.svt.se/nyheter/lokalt/skane/*
   * Exempel: *https://what-if.xkcd.com/* 








