---
author: krm
created: 2018-10-09
---
![se]
#  Toppdomänen

Sist i domännamnet har vi toppdomänen.

   * Den är ofta kort med bara ett par bostäver.
   * Varje toppdomän ägs av en organistation som kontrollerar den.
   * Ofta är det tänkt att ett viss typ av innnehåll ska finnas under en toppdomän.
   * I Sverige ägs toppdämen av stiftelsen ".SE".
   * Websidor ifrån sverige kan med fördel läggas under denna topdomän.
   * Exempel: *.com företag* - *.org organisationer* - *.com: företag*
   * Exempel: *.top Allmän* - *.app För appar kontrollerat av Google.*










