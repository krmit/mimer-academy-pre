---
author: krm
created: 2018-10-09
---
![se]
# Domännamn

Domännamn är ett namn som en person eller organisation äger på internet.

   * Det kan ses som olika adresser på internet.
   * Vi köper rätten att använda dem ifrån ett företag.
   * Detta företag har fått tillstånd av dem som kontrollerar toppdomänen.
   * En avgift betals sedan varje år för att forsätta anväda domänen.
   * Så länge avgiften betals får ingen annan ta över namnet.

År 2018: Kostar det vanligen mellan 60-300 kr att registrera ett domän, men det finns dyrare och biligare. ".se" kostar 125 kronnor.








