---
author: krm
created: 2018-10-09
more:
  - OSI
---
![se]
# port

Efter domännamnet kan vi ha ett portnummer.

   * Portnummer berättar vilket server program som vi vill komma åt på server.
   * Börjar med ":" och sedan ett tal.
   * Exempel: *http://htsit.se:**8080** *
   * En webbserver lyssnar normalt på port 443(https) och 80(http)
   * En webbläsare komuniserar normalt via dessa portar.

Du behöver alldrig skriva ut portnumret om du använder standart portarna för ett protokoll. 










