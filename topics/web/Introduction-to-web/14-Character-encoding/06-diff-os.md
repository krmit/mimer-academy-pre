---
author: krm
created: 2018-09-30
links:
  - win-to-unix
---
![se]
# Skillander mellan olika OS

Olika OS kan ha olika standarer för hur tecken används. Till exempel:

   * I windows så sparas textfiler i ett så kallad “ms-dos” format.
   * Varje read avslutats då med två tecken "\r\n"
   * I unix har bara ha ett tecken för radslut "\n"
   * Detta kan leda till problem om editor inte gör det enkelt för dig.
   * Idaag är det sällan vi stötter på detta problem.

Om du vill konvertera ifrån ett windows formatera text till unix text använd:
```bash
tr -d '\15\32' < win-file.txt > unix-file.txt
``` 
