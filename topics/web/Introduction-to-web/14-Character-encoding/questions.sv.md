# Teckenkodning

  1. Beskriv med egna ord vad en teckenkodning är för något?
  2. Illustrera teckenkodning med ett exempel.
  3. Vilket är den idag dominerande teckenkodningen?
  4. Vad heter den historiska viktiga teckenkodningen som alla andra idag använda teckenkodningar bygger på?
  5. Vad är ASCII tecknet för bokstaven “a”?
  6. ASCII koden i hexadecimalt för “b” är 62 och för “B” är det 42. Är det någon fördel att ha talen på det viset?
  7. På ett ungefär när börjar ASCII användas i stor skala?
  8. Hur många bits behövs det för att representera ett tecken i ASCII?
  9. Var är “ASCII art”? 
  10. Latin-1 är en utökning av ASCII. Hur får bokstäverna i Latin-1 plats?
  11. Latin-1 är en utökning av ASCII. Vad används/användes den till?
  12. Varför har UTF-8 blivit så populärt?
  13. Hur många bytes behövs det för att koda en bokstav i UTF-8?
  14. Vad är förhållandet mellan Unicode och UTF-8?
  15. Ungefär hur många tecken finns det i UNICODE?
  16. Vilka alternativ finns till UTF-8?






