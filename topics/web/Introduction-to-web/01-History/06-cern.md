---
author: krm
created: 2018-09-14
time: 1990
---
![se]
# CERN

CERN har extremt stort behov av informations utbyte.

  * CERN är en forsknings anläggning med partikelaccelratorer.
  * De har extremt stora fysika experiment som kräver sammarbete mellan 100-tals forskare.
  * Hur alla information ska dokumenteras och delas på ett effektivt sätt var ett stort problem.
  * Webben blir en lösning på detta problem.
