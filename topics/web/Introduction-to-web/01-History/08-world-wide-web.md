---
author: krm
created: 2018-09-14
time: 1991
---
![se]
# Första webbservern

Förkortas ofta WWW och är det som flest mäniskor tänker på när de hör ordet Internet.

  * Börjar sprida sig efter 1991 när Tim Berners-Lee publiserar sin resultat.
  * Systemmet består av två program en webbläsare och en webbserver.
  * WWW spridere sig väldigt snabbt och får snart mycket hype.

  


