---
author: krm
created: 2018-09-14
links:
  - a-Brief-History-of-the-Internet
time: 
   during 1980
---
![se]
# Internet

Internet är en global samanslutning av nätverk som är uppbyg av flera routrar som sammarbetar för att komunicera data.

  * Internet använder TCP/IP.
  * ARPNET blir först med TCP/IP, men sedna ansluter allt fler nätverk.
  * Ofta är det universitetnätverk som ansluter.
  * Toppdomän införs för att kunna organisera upp Internet.
  * I Sverige är det univversitet nätverket SUNET som sammankopplas med Internet.

  


