---
author: krm
created: 2018-09-14
time:
   after: 1950
   before: 1980
---
![se]
# Datorkommunikation

  * Datorerna blir mer avnaserade och börjar få skärmmar.
  * Det uppstår behov att kommunicera mellan datorer.
  * Olika metoder och protokoll utvecklas för detta.
  * Viktigast av de olika nätverk som utvecklas är ARPANET.
  


