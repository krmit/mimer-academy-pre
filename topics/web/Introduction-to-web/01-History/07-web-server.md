---
author: krm
created: 2018-09-14
time: 1990-12-20
---
![se]
# Första webbservern

Tim Berners-Lee skapar ett protokoll html och ett format http som ska fungera som en grund för webben.

  * Detta protokoll använder sig TCP/IP för komunikation över nätverket.
  * Webben blir därför hypertext över Internet.
  * En webbserver skapas för att se att den fungerar.
  * Detta datum kan ses som Webbens födelse, men publiseringen av dessa idéer kommer först senare.

***

Ofta när man diskuterar webbens historia brukar man koncentrera sig på webbläsaren. Men för vår del blir mest intressant att titta på webbservern och dess historia. Denna är betydligt mer spretig än för webbläsaren. Vi kan bara konstatera att olika UNIX system, såsom Linux, och Apache webbserver har dominerat. Men tidvis har även Microsoft IIS och Windows haft medvind. Språk som förekommit har varit perl, php och python. På senare tid har även ruby och nodejs börjats att användas, men det finns också många andra språk.

  


