---
author: krm
created: 2018-09-14
time:
   after: 1940
   before: 1990
---
![se]
# Hypertext

Ett ord eller flera ord i en text länkar till ett annat dokument.

  * Beskrivs första gången i en novel
  * Konceptet utvecklas och får ett namn på 1960-talet
  * Första programmen kommer i slutet av 1970-talet.
  * Mycket experimenterande sker kring olika hypertext program under 1980-talet.

Sedan kommer World Wide Web skapas med websidor(html) som blir en mycket framgångsrik implementation.

  


