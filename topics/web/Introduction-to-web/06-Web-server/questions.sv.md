# Om Webbservrar

  1. Vad är en webbserver?
  2. Nämn en vanligt använd webserver för statiska filer.
  3. Beskriv hur en webbserver för vanliga statiska sidor fungerar.
  4. Vad innebär det att en webbserver har stöd för ett script språk?
  5. Namnge minst två olika webbservrar.
  6. Vad innebär att vi kan komma åt ett Web API på en server?
  7. Vilken är den mest använda webbservern?
  8. Ange några programmeringsspråk som det är vanligt att använda i en webbserver.
