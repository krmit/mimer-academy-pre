# Om protokolet HTTP

  1. Vad är HTTP en förkortning av?
  2. Ge exempel på information som du hittar i HTTP huvudet.
  3. Vad finner du i HTTP body:n?
  4. Vad menas med HTTP metoder?
  5. Vilken metod används vanligtviss för att få en sida från en webbserver?
  6. Hur skickas information till server med GET metoden?
  7. Hur skickas information till server med POST metoden?
  8. Vad menas med en statuskod i HTTP protokollet?
  9. Vad betyder statuskod 200?
  10. Vad betyder statuskod 404?
  11. Vad betyder statuskod 304?
