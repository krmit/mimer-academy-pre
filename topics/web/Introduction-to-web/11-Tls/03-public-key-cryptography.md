---
author: krm
created: 2018-07-27
link:
  - tLS-encryption
---
![se]
# Asymmetrisk kryptering

Asymmetrisk kryptering innebär att du använder en öppen("publik") och en privat("private") nyckel för att kryptera medelandet.

   * Först skickar du din öppna nyckel till motagaren.
   * Motagaren krypterar en medelande till dig med den öppna nyckeln.
   * Detta krypterade medelandet skickas tillbaka till dig.
   * Den enda som kan läsa medelandet är du som har den hemliga nyckeln.
   * Denna process är känslig för det som kallas "Man-in-the-middel" attack.

För att förstå hur en detta fungera, tänk på att det finns visa räkneoperationer som går snabbt åt ena hållet men långsamt åt det andra hållet.








