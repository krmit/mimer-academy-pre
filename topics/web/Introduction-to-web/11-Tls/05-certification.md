---
author: krm
created: 2018-07-27
---
![se]
# Att börja använda certifikat

Krävs ett cerifikat för att stoppa man-in-the-middel attacker.

   * Ditt certifikat bevisar att du är du på internet.
   * Det är bra att ha ett certifikat.
   * Du köper dessa av en företag som äger ett så kallat root certifikat.
   * Dessa kan göra nya certifikat så länge deras privta nyckel inte har blivit avslöjat.
