---
author: krm
created: 2018-07-27
---
![se]
# TLS versioner

Det har varit problem med TLS efter som tidigare versioner har visat sig osäkra och detta har periodvis kunnat utnyttjas av hacker.

   * SSL 3.0 kom 1996 och slutades användas 2015.
   * TLS 1.0 kom 1999 och det är starkt rekomenderat att du inte använder den.
   * TLS 1.1 kom 2006 
   * TLS 1.2 kom 2008
   * TLS 1.3 kom 2018  








