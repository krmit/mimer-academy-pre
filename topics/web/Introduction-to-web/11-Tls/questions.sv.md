# Om TLS och SSL 

  1. Vad betyder https?
  2. Förklara övergripande vad asymmetrisk kryptering är för något.
  3. Vad menas med en öppen nyckel? 
  4. Vad är viktigt att tänka på om man har en privat nyckel.
  5. Kortfattat vad är tls för protokoll?
  6. Varför är det egentligen fel att prata om ssl istället för tls?
  7. Bör du anvädna tls på din hemsida?
  8. Bör alla servrar använda tls? Motivera ditt svar.
  9. Varför är det så att många ännu inte har tls på sina servrar?
