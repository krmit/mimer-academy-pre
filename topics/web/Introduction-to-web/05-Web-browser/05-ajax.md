---
author: krm
created: 2018-09-30
---
![se]
# AJAX

För att undvika att ladda om en hel sajt när anvädaren vill se ny data, kan det räcka med att webbläsaren ber om denna data och uppdaterar sedan sidan med JS. Att anropa sådan data kallas AJAX.

   * Ett AJAX anrop är när webbläsaren ber om mer information ifrån webbservern.
   * Webbläsaren får ofta svar i JSON.
   * Om webbservern inta är på samma domän kan JSONP användas.
   * JS ändrar sedan information på hemsidan utfrån information man fått. 
