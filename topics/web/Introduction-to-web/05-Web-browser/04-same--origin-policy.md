---
author: krm
created: 2018-09-30
---
![se]
# "Same-origin policy"

Att en webbsida kan inte ladda ned några webbresurser från något annan ställe på internet.

   * Domännamn, protokoll och port måste vara samma.
   * Finns inga begränsningar på sökväg.
   * OBS: Ett undantag är "script" tagen och "src" attributet.
   * Detta undantagg avnädns flitligt för att hämta olika JS biblotek.
   * Det kan också användas för att överföra data med JSONP.
   * En bätre lösning på problem med "Same-origin policy" är "Cross-Origin Resource Sharing"










