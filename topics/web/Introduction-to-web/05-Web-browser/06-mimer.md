---
author: krm
created: 2018-10-09
---
![se]
# MIME

"Multipurpose Internet Mail Extensions" är en stadard för namn på filformat. Ursprunglige för e-post, men används nu i många olika sammanhang.

   * MIME namnen på filformat är updelat i två delar, en typ och en suptyp.
   * Exempel: *text/plain* - *application/json* - *image/png* 
   * Baserad på vilken fil ändelse en fil har kommer webservern att ge filen en MIME typ och subtyp.
   * Denna egenskap återfinns i http huvudet under namnet "Content-Type:"
   * Exempel: *Content-Type: text/html*
   * Felatkig MIME typ kommer göra att webbläsaren blir förvirrad och kan hantera filen på ett felkatig sätt.







