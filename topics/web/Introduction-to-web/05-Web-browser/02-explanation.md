---
author: krm
created: 2018-09-30
---
![se]
# Vad är webläsare?

Ett program  som hämtar en html sida via http, renderar den som en webbsida och visar upp en grafisk represenation för en användare. Den följer också ett antal regler för hur en webbläsare ska fyungera för att göra webben säkert.

   * Ett sådant program som alla använder.
   * Det finns idag fyra stora webläsare.
   * *Chrome* som dominerar markanden.
   * *Firefox* som har historiska rötter till den första allmänt kända webbläsaren.
   * *Safari* som är utvecklad av Apple.
   * *Edge* som är Microsoft ny satsning.
   * *Internet Explorer* som tidigare dominerat men nu är mest känd för hur dålig den var och är.








