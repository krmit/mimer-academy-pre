# Om webbläsare

  1. Namnge tre vanliga webbläsare.
  2. Beskriv med egna ord vad en webbläsare är för något?
  3. Hur använder man AJAX i en webbläsare?
  4. Vad innebär “Same-origin policy”?
  5. Vad används MIME protokollet till?
  6. Ge ett exempel på en MIME typ.


