---
author: krm
created: 2018-09-30
---
![se]
# "Sandbox"

Innebär att varje sida körs isolerat och kan inte påverka något annan sida du läser eller något annat på din dator.

   * html och css är inga programmeringspråk och kan bara på verka layouten och utsendet av vad som visas.
   * Ingen webbresurs kan göra så program körs på din dator utanför webläsaren.
   * Javascript kan inte ändra några filer på din dator, men kan spara webbkakor.
   * Javascript, html eller css ska inte kunna få din dator att kunna krasha. 

OBS: Det kan givetviss finnas buggar som gör att ovanstånde inte längre gäller.







