---
author: krm
created: 2018-09-30
source: ?
---
![se]
# Demo på en webbklient

Detta demo är för node med *request* paketet installerat.

```javascript
const request = require('request');
request('http://www.google.com', function (error, response, body) {
  console.log('error:', error); // Print the error if one occurred
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
  console.log('body:', body); // Print the HTML for the Google homepage.
});
```








