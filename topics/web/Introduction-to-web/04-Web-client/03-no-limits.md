---
author: krm
created: 2018-09-30
---
![se]
# Inga begräsningar på webben

En webbläsare har många begräsningar för att användaren ska vara säker. Det har du inte om du utvecklar en wbbklient.

   * Därför tänk på säkerheten när du utvecklar en webbklient.
   * Se till att du kontrollerar all data du får för att undvika "injektionsattacker"
   * Tänk på att inte skicak förmånga frågor till server så den blir överblastad.
   * Mem mera.

***

Å andra sidan tänkt inte för mycket på säkerheten när du skriver något litet bara för att lära dig, bara ha i åtanke att det är något du ska lära dig så småning om.








