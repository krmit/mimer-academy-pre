---
author: krm
created: 2018-09-03
---
![se]
# Hur använder vi en webserver?

  * Vi lägger upp statiska html sidor.
  * Vi använder programmering för att svara på anrop ifrån klienten.
  * Vi hämtar data ifrån en databaser och anväder det i vårt svar.
  * Vi implenterar säkra protokoll så komunikationen blir säker.
  * Vi använder ett ramverk för att underlätta detta.
  
***

För att kunna göra detta måste vi dels först det programmeringsspråk och ramverk vi använder på webbservern. Vi behöver också förstå hur en webbserver fungerar, vilka utmaningarna är för att använda den och hur vi skyddaren ifrån intrång.




