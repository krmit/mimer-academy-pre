---
author: krm
created: 2018-09-09
---
![se]
# Vad är hypertext?

Ett dokument där ett ord är länkat till ett annat dokument.

  * Har gamla rötter långt innan webben.
  * Blir ofta nödvändigt om du har mycket dokumentation som behövs i en organsiation.
  * Ofta så naturligt så vi inte tänker på det.




