# Om webben

  1. Beskriv med egna ord vad en webbläsare är för något?
  2. Beskriv med egna ord vad en webbserver är för något?
  3. Vad är en webbklient?
  4. Vad är ett protokoll?
  5. Vad är http en förkortning av?
  6. Vad är html en förkortning av?
  7. Vad innebär det att något ska utföras på klientsidan(“front-end”)?
  8. Vad innebär det att något ska utföras på serversidan(“back-end”)?



