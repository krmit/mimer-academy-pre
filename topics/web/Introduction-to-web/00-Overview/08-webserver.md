---
author: krm
created: 2018-09-09
---
![se]
# Webserver

En webserver är ett program som skickar en webbsida om den får en fråga av en webklient.
  * En webbserver arbetar alltid med protokollet http eller https.
  * Det finns ett stor antal webservrar idag med olika egenskaper.
  * Ofta har webserver en modul som gör det möjligt att programmera den.
  * En webbserver är också ofta kopplat till en databas.
  * Att utveckal för en webbserver kallas att man arbetar med "back-end".
  
***

Webbservern som kommunicerar med webbläsare som  frågar servern efter data. För detta används ett protokoll som heter HTTP(Hypertext Transfer Protocol). Detta protokoll ska vi diskutera i detalj senare. Idag har även ett annat protokoll, nämligen  “websockets”, börjats att användas. Websockets fungerar på ett annat sätt jämfört med http, men det diskuterar vi i avsnittet om websockets. Att utveckla för en webbserver brukas kallas att man jobbar med “back-end” eller på serversidan.




