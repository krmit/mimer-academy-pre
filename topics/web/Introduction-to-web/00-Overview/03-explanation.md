---
author: krm
created: 2018-09-09
links:
  - www 
  - front-end-and-back-end

---
![se]
# Vad är webben

Webben är egentligen fyra olika saker som alla bygger kring idén om hypertext.

  * Webbserver som gör det möjligt att dela webbsidor.
  * Webbklient som efterfrågar och tar emot webbsidor
  * HTTP protokollet för att få webbläsaren och webbservern att prata med varandra.
  * HTML med flera format som används för att beskriva hur en websida ska se ut. 

Webben är ett sätt att kommunicera över internet på ett effektivt sätt med hypertext. Den består av flera olika metoder och protokoll. I detta kapitel ska vi lära oss om webbservern och framför allt skriver program som kan köras på servern och svara på förfrågningar från en klient.





