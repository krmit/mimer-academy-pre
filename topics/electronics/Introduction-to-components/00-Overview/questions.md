---
author: krm
created: 2018-11-13
comment: "Tänk på komponeterna."
questions:
    - type: short
    - type: short
    - type: short
    - type: short
---
![se]

  1. Vad är en elektriskt komponent?
  2. Vad brukar finnas på ett elektriskt kretskort.
  3. Vad är ett kretskort?
  4. Vad är ett kopplingsschema?
