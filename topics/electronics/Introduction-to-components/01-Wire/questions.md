---
author: krm
created: 2018-11-13
comment: "Tänk på komponeterna."
questions:
    - type: short
    - type: short
    - type: short
    - type: short
---
![se]

  1. Vilken av följande metaller är bäst som ledare?
    * Aluminium
    * Koppar    
    * Järn    
    * Kryptonit

  2. Vad menas med en PCB?
  3. Vad är skillnaden mellan en hålmonterad och en ytmonterat komponent?

