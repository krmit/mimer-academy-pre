# Diodrar

  1. Vad är en diod?
    * En komponent som skapar en resistans för strömmen.
    * En komponent som bara leder ström i en riktning.
    * En komponent som lagrar elektrisk energi.
    * En komponent som skapar ett starkt magnetiska fält.
  2. Vad är sant om en fotodiod?
    * Den avger ljus
    * Den kan användas för att känna av ljus   
    * De är alltid väldigt dyra.   
    * De är en typ av fotograf
  3. Vad innebär framspänningsfall i en diod?
    * Vid vilken späning som dioden öpnnar sig i framåt riktningen.
    * Den negativa zenerspänningen.
    * När dioden faller framåt pga. ett magnätfält.
    * Vid vilken späning som dioden brinner upp.
  4. Ofta används den engelska förkortningen för lysdioder. Vad är denna?
    * LEN
    * PNP
    * LED
    * LOL
  5. Vad är speciellt med en zenerdiod?
    * Den är pga. av en mycket kort switch tid lämplig att använda i digitalteknik.
    * En form av buddhismen där de tänker sig att världen bara rör sig i en riktning.   
    * Att den fungerar bra som en förstärkare för audiosignaler.
    * Den har en väldefinierad backspänning då den börjar släppa igenom ström utan att gå sönder.
  6. Vad kallas den typ av diod som vi kan få att visa nästa alla färger?
    * Färg-diod.
    * Fyrbent lysdiod.
    * RGB-diod.
    * Regnbågsdiod
