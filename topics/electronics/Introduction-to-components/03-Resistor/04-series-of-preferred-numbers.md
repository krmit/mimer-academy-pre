---
author: krm
created: 2018-01-01
---
![se]
# E-serie

Resistansen på motstånd följer en serie till exempel E6. 

  * Motstånd från denna serie har något av följande värden i ohm: 
  * 1, 1.5, 2.2, 3.3, 4.7, 6.8, 10, 
  * 15, 22, 33, 47, 68, 100, 150 osv.

Vill du ha ett annan resistans på ditt motstånd måste du kombinera de motstånd som finns tillgängliga.
