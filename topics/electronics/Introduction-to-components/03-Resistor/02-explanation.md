---
author: krm
created: 2018-01-01
---
![se]
# Vad är ett motstånd?

Motstånd är den vanligaste elektroniska komponenten. 

  * Den kan användas till att fördela både ström och spänning så att varje de elektriska komponenterna får så mycket de behöver.
  * Detta genom att du parallellkopplar eller seriekopplar motstånden med komponenten.
  * Vi kan använda ohms lag för att beräkna så strömmen och spänningen blir rätt.
