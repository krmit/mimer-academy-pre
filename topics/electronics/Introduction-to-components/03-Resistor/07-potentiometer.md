---
author: krm
created: 2018-01-01
---
![se]
# Potentiometer

En potentiometer är ett variabelt motstånd. Ofta kan du vrida på en ratt och ställa in hur stor andel av den totala motståndet som du ska få ut via mitten benet. 

  * Potentiometer har tre ben, en i början och ett slutet av motståndet. 
  * Mellan dessa ben har till alltid det maximala motståndet. 
  * Logaritmiska potentiometerar följer en logaritmisk skala, vilket gör att små ändringar i vridning kan få stor effekt.






