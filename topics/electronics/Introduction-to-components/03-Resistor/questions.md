---
author: krm
created: 2018-11-13
comment: "Tänk på komponeterna."
questions:
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short

---
![se]

  1. Vad är ett motstånd?
    * En komponent som skapar en resistans för strömmen.
    * En komponent som bara leder ström i en riktning.
    * En komponent som lagrar elektrisk energi.
    * En komponent som skapar ett starkt magnetiska fält.
    
  2. Vilken av dessa värden tillhör inte E6?
    * 22
    * 33   
    * 3.1415   
    * 47
    
  3. Vilken typ av komponent är en potentiometer?
    * Diod
    * Motstånd
    * Transistor
    * Kristal

  4. Till vad använder man en potentiometer som har en logaritmisk skala?
    * För stora strömmar.
    * För höga spänningar.
    * För stora magnätfält
    * För ljud tilämpningar

  5. Vilka av följande värden är förenliga med E6 serien?
    * 10,15,20,25,30,35
    * 0,10,25,50,75,100
    * 0,1,2,3,4,5
    * 10,15,22,33,47,68

  6. Vid vilken effekt nedan kan du vara säker på att de motstånd som vi vanligen använder i labbet inte börjar brinna eller blir förstörda?
    * 10 W
    * 0.1 W
    * 1 kW
    * 1 GW

  7. Vilken typ av graf kommer vi få om vi undersöker ett motstånd och ritar upp ett U/I-diagram.
    * En kurva som liknar kurvan för x².
    * En kurva som liknar exponetialkurva.
    * En rättlinje
    * En raklinje på ett logaritmiskt diagram.
