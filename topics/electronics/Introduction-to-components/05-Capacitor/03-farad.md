---
author: krm
created: 2018-01-01
---
![se]
# Farad

Hur effektivt en kondensator kan lagra laddningar mäts i enheten Farad.

  * Denna enhet är ofta liten, den kan mätas med prefix såsom piko och nano. 
  * Vanligt för stora kondensatorer är att de mätt i antal mikrofarad.







