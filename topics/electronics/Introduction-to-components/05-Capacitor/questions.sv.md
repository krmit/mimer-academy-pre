# Kondensatorer

  1. Vad är en kondensator?
    * En komponent som skapar en resistans för strömmen.
    * En komponent som bara leder ström i en riktning.
    * En komponent som lagrar elektrisk energi.
    * En komponent som skapar ett starkt magnetiska fält.

  2. Vad kallas den kondensator typ som du bör undvika, men som du kommer vara tvingad att använda för att de är den enda typen av kondensator som har tillräcklig kapacitans?
    * Keramiskakondensatorer
    * Elektrolytkondensatorer
    * Polyesterkondensatorer
    * Tantal              

  3. Beskriv en anledning till att vi ska undvika elektrolytkondensatorer.
    * De har väldigt dålig lagringskapacitet.
    * De klarar inte stora spänningar.   
    * De klarar inte stora strömmar.   
    * De får dålig livslängd om de utsätts för värme.

  4. Vilken enhet används för att mäta kapacitans i en kondensatorer?
    * Farad (F)
    * Henry (H) 
    * Boom (B)
    * Coulomb (C)

  5. Vad har en kondensator för uppgift i en likriktare?
    * Att jämna ut strömmen och spänningen.
    * Att förstora skillnader i spänning.
    * Att begränsa strömmen.
    * Att skapa strömspikar.

  6. Om vi vill rita upp ett diagram som beskriver en kondensators storheter när den utsätts för en ström. Vilka enheter är då mest relevanta?
    * U och t
    * I och U
    * m och t
    * E och m

  7. Vilka prefix är vanliga för att beskriva kapacitansen på en kondensator?
    * kilo, mega, giga
    * zepto, atto, femto
    * nano, mikro, mili
    * mus, zebra, elefant
