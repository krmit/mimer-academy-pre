# Transistorer

  1. Vad är en transistor?
    * En komponent som skapar en resistans för strömmen.
    * En komponent där en ström/spänning styr storleken på en annan ström.
    * En komponent som lagrar elektrisk energi.
    * En komponent som skapar ett starkt magnetiska fält.

  2. Vilken av följande är inte namnet på ett ben till en bipolär transistor?
    * emittern
    * kollektorn
    * basen   
    * taket

  3. Vilket material är vanligast att bygga upp en halvledare som används i en CPU med?
    * Koppar
    * Kisel
    * Kol  
    * Kryptonit

  4. Nämn de två olika typer av bipolära transistorer vi använder oss av.
    * PP och NN.
    * TNT och NTN.
    * PNP och NPN.
    * DNA och DNS.

  5. Vad kallas de transistorer som finns i en CPU?
    * bipolära transistorer
    * JET
    * transformatorer
    * fälteffekttransistorer

  6. Vad innebär det att ett material är positivt dopat?
    * Att det har ett överskott på elektroner.
    * Att statisk elektricitet har laddad upp materialet så det är positivt laddad.
    * Att det har ett underskott på elektroner.
    * Att WADA har dokumenterat att en idrottsman är dopat sig.

  7. Vilka två användningsområden är vanliga för en transistor?
    * Förstärkare och pulsgivare.
    * Transformator och brytare.
    * Bas och diskant.
    * Förstärkare och brytare.

  8. Transistor har påverkat vårt samhälle väldigt mycket. När började den morderna transistor att utvecklas?
    * 1940-talet.
    * 1950-talet.
    * 1960-talet.
    * 1970-talet. 
