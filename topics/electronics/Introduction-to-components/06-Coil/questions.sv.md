# Spole

  1. Vad är en spole?
    * En komponent som skapar en resistans för strömmen.
    * En komponent som bara leder ström i en riktning.
    * En komponent som lagrar musik.
    * En komponent som skapar ett starkt magnetiska fält.

  2. Vilken enhet används för att mäta induktans i en spole?
    * Farad (F)
    * Henry (H) 
    * Boom (E)
    * Coulomb (C)
    
  3. I vilken av följande tillämpningar använder man inte normalt sett en spole som viktigaste komponent?
     * högtalare
     * relä
     * motor
     * skärmar

  4. Vilket av följande sätt ökar inte magnetfältstyrka i en spole?  
     * Öka spolens hastighet.
     * Mer ström går genom spolen.
     * Flera varv i spolen.
     * En järnkärna sätts in i spolen.

  5. Vilken komponent liknar spolen men fungerar delvis omvänt jämfört med spolen?
     * Kondensatorn
     * Dioden
     * Transistorn
     * Motståndet
