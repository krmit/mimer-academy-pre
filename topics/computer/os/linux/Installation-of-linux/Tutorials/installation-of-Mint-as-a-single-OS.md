# Installation av Mint som ett ensamt OS på en dator

Denna guide är till för Linux Mint som OS. Observera att allt annat på datorns minen kommer att försvinna.

  1. Fixa bootbar medium. Du kan låna ifrån krm, antingen CD eller USB. 
  2. Få datorn och boota med detta medium. Kan krävas att du gör ändringar i UEFI(Ofta kallat BIOS).
     * På många maskiner kan du tr tryck F12 under start upp och sedan välja boot medium.
  3. Se att Linux Mint live fungerar bra.
  4. Starta installationsprogram, klickar på ikonen som ser ut som en skiva.
  5. Svara på frågor om språk, observera att det kan finnas en fördel välja engelska eftersom du får datorns felmeddelande på engelska då.
  6. Välj svenskt tangentbord tangentbord och första under alternativet.
  7. Välja att ha med tredje parts mjukvara för det gör OS mer praktiskt att använda.
  8. Partitionera hårddisken, du kan välja guidad för hela hårddisken.
  9. Kryssa i rutan om att du vill installera tredjepartsprogram.
  10. Svara på frågor om plats/tidszon.
  11. Skapa en ny användare genom att ange ett användarnamn, förslagsvis ett alias du använder och ett lösenord.
  12. Linux mint normalt sätt automatisk en så kallat “mirror” server där ny och uppdaterad programvara laddas ned från. Någon som ligger nära eller i Sverige.
  13. Vänta medan program installeras.
  14. Observera att Linux Mint kommer skriva över din MBR automatiskt, dvs. hur dator bootar.
  15. Ta ut boot mediet och starta om datorn.
 
