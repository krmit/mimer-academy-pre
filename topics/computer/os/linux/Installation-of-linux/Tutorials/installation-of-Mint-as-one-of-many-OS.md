# Installation av Mint som ett av många OS på en dator

Utgå ifrån guiden [Installation av Mint som ett ensamt OS på en dator](installation-of-Mint-as-a-single-OS.html). Men du gör stegen nedan på ett annat sätt.

## Steg 3.  
När du är inne i Live CD medium. Öppna programmet Gparted:

  1. Om du har ett OS redan installerat välj att krympa den första partitionen till en lämplig storlek. Ska du bara ha ett OS så är 20 GB ofta lagom, men ska du använda detta OS och köra andra program på det så bör det vara större. 
  2. Gör en swap partition om det inte redan finns det. Du gör det gen9om att skapa en ny partition och välja swap som filsystem.
  3. Du kan nu skapa ytterligaren en primär partition, tänkt dock på att du får inte ha mer än fyra primära partitioner. Du skapa en ny partition genom att välja minnet som ännu inte tillhör någon partition och väljer skap ny partition. Sedan väljer du vilket filsystem den ska ha. Ska du installera linux väljer ext4 om du inte vet att du vill ha ett annat alternativ.
  4. Välj att skapa ett extendet partition. Välj helt enkelt detta alternativ när du skapar en ny partition. Låt denna partition ta upp allt minne som du har kvar.
  5. Nu kan du välja det tomma minnet i den nya “extendet partion” och skapa en ny partition precis som du gjorde under punkt c.
  
## Steg 8

Välj “Customise partition”.
 



