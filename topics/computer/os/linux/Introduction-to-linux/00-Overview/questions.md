---
author: krm
created: 2018-09-23
questions:
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
---
![se]

  1. Vad är POSIX?
    * En standard för OS
    * Ett realtids OS    
    * Ett OS som är avsett att fungera på routrar och switchar.    
    * Ett problem med prioriteringen av processer för olika OS
  2. Vilket av följande OS är inte POSIX liknande?
    * Mac OS
    * Ubuntu   
    * Windows   
    * Android
  3. Vilken av följande fördelar har du inte med POSIX?
    * Du kommer ha det lättare att porta dina program mella POSIX OS om du inte använder några system beroende bibliotek.
    * Du kan kompilera en gång och veta att det kommer fungera på alla POSIX system.   
    * Du kommer ha tillgång till liknande terminal program för att administrera systemet.  
    * Du kan välja mellan ett stor antal olika OS med olika egenskaper som alla är POSIX.
  4. Vad är det första som startas när du slår på en dator?
    * MBR
    * BIOS
    * TV    
    * GRUB
  5. Vad kallas de första delen på en hårddisk?
    * MBR
    * BIOS
    * FIRST    
    * GRUB
  6. När du har fler OS på en dator behöver du välja vilket du ska använda vid uppstart. Vilket av följande program kan hjälpa dig med det?
    * MBR
    * BIOS
    * FIRST    
    * GRUB
  7. Vad finns i mappen “/bin”?
    * Olika översättningar av dokumentation.
    * Stora binära filer, så kallade blobs    
    * Körbara filer av standardprogram.  
    * Mappen som innehåller alla körbara filer på ett Linux system.
  8. Du installerar ett program på vanligt sätt, var hamnar då den körbara filen till det programmet?
    * /bin
    * /tmp   
    * /home/bin
    * /share/bin
  9. Under vilken mapp kommer alla användare av systemet,  utom root, ha sin hemmapp?
    * /bin
    * /home   
    * /tmp
    * /usr
  10. Hur många olika rättigheter har en fil?
    * 6
    * 9    
    * 12   
    * 16
  11. Vad heter de olika rättigheter en fil kan ha?
    * läs, skriv och exekverbar 
    * läs, appendera och exekvera  
    * läs, skriv och ta bort   
    * läs, skriv och uppdatera
  12. Hur många grupper kan en fil tillhöra?
    * Hur många som helst, men vissa praktiska begränsningar.
    * En enda.
    * Två olika, användargruppen och en till.
    * Filer har inga grupper.
  13. Hur många grupper kan en användare tillhöra?
    * Hur många som helst, men vissa praktiska begränsningar.
    * En enda.
    * Två olika, användargruppen och en till.
    * Använder har inga grupper.
  14. Vad är namnet på kernel i Linux OS?
    * Linux
    * Windows NT    
    * BSD    
    * Kernel
