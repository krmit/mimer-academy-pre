---
author: krm
created: 2018-12-12
questions:
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
---
![se]

1. Vilket populärt os var först med Windows NT?
2. Vad heter Microsofts första OS och som också låg till grund för Windows 9x serien?
3. På ett ungefär när blev Windows dominerande som OS?
4. Vilket var det första Windows OS som använde Metro som design?
5. Nämn ett windows OS som blivit mycket framgångsrikt?
6. Nämn ett windows OS som inte varit lika framgångsrikt som sin föregångare?
7. Nämn ett OS i “Windows server” serien av OS.
8. Vilket är Microsofts bästa OS. Motivera ditt svar*
