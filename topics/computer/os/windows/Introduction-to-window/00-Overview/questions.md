---
author: krm
created: 2018-12-12
questions:
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation

---
![se]


  1. Om du vill få något som liknar DOS kommandoprompt på någon av de senaste versionerna av window vilket program ska du använda då?
  2. Vad är namnet på kernel i Windows?
    * Windows MT
    * Windows NT    
    * MSDOS    
    * Linux
  3. Vilka två olika mods använder sig Windows kerneln av?
  4. Vilka kallas det subsytem av Windows NT som Windows OS nomralt körs i?
  5. På vilket sätt är windows NT källkod tillgänglig för allmänheten?
  6. Vad är en utgåva av Windows?
  7. Namnge två utgåvor för Windows 10.
  8. Vad är en OEM licens?
  9. Vad är en “feature updates” på Windows 10?
  10. Vad är skillnaden mellan OS som kallas “Windwos Server” och andra Windows? 
  11. Vad är ett register i Windows?
  12. Nämn ett språk som vanligen används tillsammans med .Net?
  13. Vilket språk(eller snarare platform) var .Net ett försök att  efterlikna och överträffa?
  14. Vad menar vi när vi pratar om Metro i Windows sammanhang?
  15. Vad kallas den teknik som kan få många Windows datorer att samarbeta i en domän baserat nätverk?
  16. Vad får användarna för fördelar av att använda “Active directory”?
  17. Vad kallas den mer avancerade kommandotolk som har många funktioner som liknar bash/terminal?
  18. Vilket ramverk är PowerShell väl integrerat med?

