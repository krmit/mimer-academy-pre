---
author: krm
created: 2018-09-23
questions:
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
---
![se]

1. Nämn ett OS som använder sig av BSD kärna?
2. Nämn någon form av teknik som är associerad med BSD.
3. Nämn flera fördelar med BSD.*
4. Nämn någon praktisk tillämpning av BSD som har många användare.
5. På ett ungefär, när började man utveckla BSD kärnan?
6. Vilken kärna bygger Mac OS X på?
7. Vilket programmeringsspråk är UNIX skrivet i?



