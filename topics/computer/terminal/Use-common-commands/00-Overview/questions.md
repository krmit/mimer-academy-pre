---
author: krm
created: 2018-09-23
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
---
![se]

  1. Beskriv med egna ord vad kommando i terminalen är för något?
  2. Vad innebär det att ett kommando har en flaga?
  3. Ge ett exempel på ett komando med en flaga.


