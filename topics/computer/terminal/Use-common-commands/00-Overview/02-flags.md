---
author: krm
created: 2018-09-30
---
![se]
# Kommandon i terminalen har ofta flagor

  * Utgör inställningar till det program du ska köra
  * De kan stå ensamma eller ha ett argument. 
  * En flaga börjar med "-" om det är en bostavsvarain. ls -l
  * Eller med "--" om det är fler bokstavsvariant. ls --color
  * Flera en bokstavs flagor kan skriva tilsamans. ls -la


***

Gällande kommandon. Ett Unix kommandot är uppdelat i dels själva programmet, ett antal flaggor som kan förekomma på olika sätt och argument till själva programmet. En flagga är alltid en bokstav och för att skriva flaggar börjar men skriva ett “-”, observera att flagor kan kombineras.





