---
author: krm
created: 2018-09-23
---
![se]
# Vi ska titta på grundläggande kommandon du kan använda i ett skall.

  * Dessa kommandon kommer vara de som oftast förekommer.
  * Dessa kommandon är samma för de flesta POSIX system, dvs Linux, Mac och UNIX.
  * Med dessa kan du utföra många adminstrativa uppgifter på ett system.
  * Du behöver dock också använda dig av en Editor.
  * För fler och med avancerade kommandon se nästa kapitel.





