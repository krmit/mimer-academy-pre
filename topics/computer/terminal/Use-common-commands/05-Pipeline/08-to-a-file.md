---
author: krm
created: 2018-10-14
---
![se]
# Koppla en ström till en fil.

Innehållet ifrån en ström kan skrivas till en fil med operatorn ">".

  * "Ström > Fil"    Det som komme i strömmen *Ström* kommer skrivas till filöen *Fil*
  * Exempel: *echo "hej!" > fil.txt" 
  * OBS: Allt som finns i  filen kommer att tas bort. 





