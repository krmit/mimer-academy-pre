---
author: krm
created: 2018-10-14
---
![se]
# Vad är ett rör?

Det du stoppar in i en änden kommer ut i andra änden.

  * Tänk dig spelet Portal. Det som kommer in i blå portal kommer ut ur en orange portal.
  * Det är ström("stream") av data som färdas genom ett rör.
  * I ett shell är detta nästan alltid en ström av tecken.
  * Det rören gör är att koppla samman olika strömar från olika program





