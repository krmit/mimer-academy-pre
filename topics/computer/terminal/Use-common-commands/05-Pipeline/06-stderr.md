---
author: krm
created: 2018-10-14
---
![se]
# stderr

Är en ström av tecken som beskriver fel medelanden ifrån program.

  * En utskrift av alla felmedelanden ifrån ett program.
  * I terminalen skrvis både stdout och stderr ut tillsammans.
  * "&2" används ofta för att referera till denna ström.

Ett tips kan vara att koppla stderr till någon fil. 
