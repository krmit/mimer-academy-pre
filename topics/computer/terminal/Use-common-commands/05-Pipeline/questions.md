---
author: krm
created: 2018-09-23
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
---
![se]

  1. Beskriv med egna ord vad rör/"pipes" för något?
  2. Vad menas med stdin på ett program?
  3. Vad skriver ett program i stderr?
  4. Vad är stdout?
  5. Vad är en ström?
  6. Vad gör operatorn "|"?
  7. Vad gör operatorn ">"?
  8. Vad gör operatorn ">>"?
