---
author: krm
created: 2018-10-14
---
![se]
# Pipe

Rör avänds för att rikta om strömmar. Att i ett skall kopla samman stdin och stdout gör med tecknet "|".

  * *Program1 | Program2*      stdout på *program1* kopplas till stdin på *program2*.
  * Exempel: *ls | grep txt*
  * Koppla betyder att du riktar om en ström med ett rör.





