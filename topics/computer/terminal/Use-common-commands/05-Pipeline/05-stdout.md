---
author: krm
created: 2018-10-14
---
![se]
# stdout

Är en ström av tecken som kommer ut ifrån ett program.

  * Skriver ut resultatet av programmet.
  * Kan användas för att kontrollera om programmet ger rätt utdata.
  * Windows: Även här finns har program stdin.





