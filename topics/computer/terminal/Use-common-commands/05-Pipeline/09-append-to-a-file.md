---
author: krm
created: 2018-10-14
---
![se]
# Lägga till en ström till en fil.

Innehållet ifrån en ström kan skrivas till slutet av en fil med operatorn ">>".

  * "Ström >> Fil"    Det som komme i strömmen *Ström* kommer skrivas sist i filen *Fil*
  * Exempel: *echo "hej!" > fil.txt; echo "hej då!" >> fil.txt" 
  * Detta skulle kunna användas till att skriva en fill log till exempel.





