---
author: krm
created: 2018-10-14
---
![se]
# Socket

Socket är det grundläggande API:et för nätverkskomunikation. Det gör så man kan skapa rör över internet.

  * Dessa rör är grundern för datakommunikation.
  * Socket utgör ett bra exempel på vad man kan använda pipes till
  * Vill du experimentera så sök efter information om ett program som kallas *netcat*.





