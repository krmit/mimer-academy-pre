---
author: krm
created: 2018-10-14
---
![se]
# Hårda länk

Hård länk är ett namn som länkar till en fil i filsystemmet. Det är helt enkelt ett filnamn.

  * En hård länk som skapas kommer fungera på samma sätt som om det vore ett filnamn.
  * Tar du bort den så kommer filen försvina om den inte har har något annat filnamn.
  * Att flytta filen fungerar som vanligt.
  * Att skriva eller kopiera filen fungerar som om det var filen.
  * Filen som är länkat är helt enkelt en fil med två filnamn.
  * Hårda länkar kan också länka till mappar. 




