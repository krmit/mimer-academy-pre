---
author: krm
created: 2018-10-14
---
![se]
# Symboliska länkar

Är ett namn på en länk till en fil. Denna länk kan vara en relativ sökväg. 
 
  * En symbolisk länk fungera mera som en länk på webben.
  * Tar du bort den kommer bara själva länken försvinna, inte själva filen.
  * Om länken är en relativ sökväg, kan det hända länken pekar på en anna fil.
  * Att skriva eller kopiera filen fungerar som om det var filen.
  * Filen har helt enkelt ett filnamn och den symbooliska länken är helt enkelt en länk till denna.
  * Symboliska länkar kan också länka till mappar. 
 
Ofta vill du bara använda absoluta sökvägar för att undvika problem. 




