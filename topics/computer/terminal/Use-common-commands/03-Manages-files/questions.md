---
author: krm
created: 2018-10-14
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Beskriv med egna ord vad man göra med en fil?
  2. Vilket kommando skulle du använda om du vill kopiera en fil?
  3. Vilket kommando skulle du använda om du vill flytta en fil?
  4. Vilket kommando skulle du använda om du vill kopiera en mapp?
  5. Vilket kommando skulle du använda om du vill radera en fil?
  6. Vilket kommando skulle du använda om du vill skapa en länk?

