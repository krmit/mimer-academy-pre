---
author: krm
created: 2018-10-14
---
![se]
# Förslag på en "fuzzy finder"

Det finns flera program som kan göra detta effektiv, men inget som är standart. Rekomenderar fzf eller fzy.

  * Gå till [fzf](https://github.com/junegunn/fzf) elelr [fzf](https://github.com/jhawthorn/fzy) och följ installations instruktionerna.
  * Skriv bara kommandot *fzf* eller *fzy* och sedan vad du söker efter.
  * Programmen kommer interaktiv visa möjliga filer som du letar efter.





