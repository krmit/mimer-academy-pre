---
author: krm
created: 2018-10-14
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
---
![se]

  1. Hur skulle du göra för att hitta en fil?
  2. Vilket kommando skulle du använda om du vill hitta alla filer och mappar med sökvägar?
  3. Vilket kommando skulle du använda om du skulle skriva ut något på skärmen?
  4. Vilket kommando skulle du använda om du vill filtrera en ström?
  5. Hur skulle du göra för att finna alla filer som innehåler ordet "help"?

