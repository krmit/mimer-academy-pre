---
author: krm
created: 2018-09-23
---
![se]
# find

find är ett kommando som listar alla filer och mappar.

  * *find .*         Kommer list alla filer i aktuell map och alla undermappar till denna.
  * *find Mapp*      Kommer list alla filer i mappen *Mapp* och alla undermappar till denna.
  * Listar med sökväg till filen.
  * Har många flagar för att hitta olika typer av filer, använd *man*.
