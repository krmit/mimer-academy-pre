---
author: krm
created: 2018-09-23
---
![se]
# grep

En kommando som filtrerar bort allt som inte innehåller ett villlkor.

  * *grep Namn*			Filtrerar bort alla rader som inte innehåller *Namn*
  * OBS: Många tecken tolkar av grep som om de vore kommandon.
  * Använd "\" för att escape ett sådan tecken.
  * Det finns många alternativ för att söka, använd *man*.





