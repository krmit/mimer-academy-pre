---
author: krm
created: 2018-09-30
---
![se]
# Aktuell mapp

Aktuell mapp("current directory"), vi kommer alltid befinna oss i en mapp. Det är i denna mapp som alla kommandon kommer utföras i.
  
  * Vi kan använda flera kommandon för att byta aktuell mapp.
  * Många grafiska filhanterar kan låta oss öppna en terminal i den mapp vi har öppen.
  * Ofta vissas vilket mapp som är aktuell i prompten. 





