---
author: krm
created: 2018-09-23
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
---
![se]

  1. Beskriv med egna ord vad mapp är för något?
  2. Vad är en absolut sökväg?
  3. Ge exempel på en absolut sökväg.
  4. Vad är en relativ sökväg?
  5. Ge exempel på en relativ sökväg.
  6. Vad händer om du skriver "~" i en terminal?
  7. Vilka kommando skulle du använda för att lista alla filer och mappar i en mapp?
  8. Vilket kommando skulle du använda för att byta nuvarande mapp?
  9. Vilket komando skulle du använda för att skappa en ny mapp?
  10. Vilket mapp skulle du se aktuell sökväg?
  


