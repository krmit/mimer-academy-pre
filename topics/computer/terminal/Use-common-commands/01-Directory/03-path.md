---
author: krm
created: 2018-09-30
---
![se]
# Sökväg

En sökväg(“path”) är en lista på de mappar som behöver gås igenom för att hitta en fil. 

  * Till exempel “/home/linus/linux.txt”, säger oss var filen “test.txt” är lagrad i minnet.
  * Om en sökväg är tilll en mapp avslutas den med "/".  t.ex. "home/mimer/"
  * Om den slutatar på ett annat sätt är den en fil. t.ex. "home/mimer/document.md"
  * Eller "home/mimer/README"
  * Tips: Dessa sökvägar är liknade dem som förekommer i en URL som du skickar till en webserver.





