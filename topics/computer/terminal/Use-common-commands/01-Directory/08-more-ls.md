---
author: krm
created: 2018-09-30
---
![se]
# Mer om ls

Kommandot ls har väldigt många olika flagor

  * *ls -R*        Gör ls kommandot på alla undermappar.
  * *ls -h*        Storleken på filerna visas på ett läsvänligt sätt.
  * *ls -lha*      Gör en detaljerad lista över alla filer även dolda i en mapp och storleken på filerna visas på ett läsvänligt sätt.





