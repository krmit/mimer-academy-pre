---
author: krm
created: 2018-09-30
---
![se]
# mkdir

För att skapa nya mappar.

  * *mkdir Mapp*            Skapar en ny mapp “Mapp”.
  * *mkdir -p Mapp1/Mapp2*  Skapar alla mappar i en sökväg, i detta fall Mapp1 och sedan Mapp2 i Mapp1.

Tips: *mkdir -p* kommandot ger inget felmedelande om mappen som ska skapas redan finns, men det gör däremot mkdir utan p flaga.




