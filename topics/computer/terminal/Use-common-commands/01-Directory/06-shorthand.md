---
author: krm
created: 2018-09-30
---
![se]
# Förkortningar

Det kan också förekomma förkortningar i sökvägar.

  * ~ är namnet på hemkatalogen. 
  * Så “~/image/tux.png” betyder alltså samma sak som “/home/mimer/linux.txt”.

Obs! Om din kommado inte körs i ett skall kan det ibland ske att dessa förkortningar inte tillämpas. Det kan orsaka fel som är svåra att förutse.


