---
author: krm
created: 2018-09-30
---
![se]
# Relativ sökväg

Relativ sökväg(“relative path”) utgår ifrån den aktuella katalogen. 

  * En enkel punk(“.”) betyder den aktuella katalogen
  * Så “./tux.png” betyder filen “tux.png” i aktuell katalog
  * Två punkter “..”, betyder att vi går tillbaka ett steg i filesystems hierarki.
  * Detta kan upprepas "../..", betyder två steg.

Om du vill köra en fil som inte finns i din sökväg för körbara filer får du skriva "./" först. Till exempel "./server.js" eller "./a.out".





