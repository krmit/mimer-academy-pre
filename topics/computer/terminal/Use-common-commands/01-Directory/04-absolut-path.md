---
author: krm
created: 2018-09-30
---
![se]
# Absolut sökväg

Absolut sökväg(“absolute path”) utgår ifrån filsystemets root och innehåller alla mappar fram till filen eller mappen som eftersökt.
  
  * Till exempel:
    * "/var/www/"
    * "/root/.bashrc"
    * "/bin/ls"
  * I en webserver utgår en absolut sökvägt ifrån domänamnet. 





