---
author: krm
created: 2018-09-23
questions:
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
---
![se]

  1. Beskriv med egna ord vad bash script är för något?
  2. Vad används den första raden till i ett skript?
  3. Hur avslutar man en rad i ett shell script?
  4. Varför kan det vara bra att skriva shell script?
  5. Varför kan det vara olämpligt att programmera bashscript?


