---
author: krm
created: 2018-10-14
---
![se]
# Var är en skalscript?

Vårt skal kan fungera som en tolkare på samma sätt som vi har tolkare för javascript, python eller perl.

  * Ett skalscript är en fullständigt programmeingsspråk.
  * Det är väldigt bra för att skriva skript som sköter adminstration av din dator.
  * Mycket programmering för system programmering är gjorda i skal, framför allt i bash.
  * Eftersom alla POSIX system har ett konmpatibelt skal är de väldigt kompatibla.
  * Ingen ny tolkare behöver installeras för skallet finnsredan.
  * Bash har fler funktioner och finns på de flesta POSIX system, men inte alla.





