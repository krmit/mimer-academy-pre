---
author: krm
created: 2018-10-14
---
![se]
# Grunderna för script programmering i ett skal

  * Att kunna skriva några enkla skript kan vara mycket användbart.
  * Det kan också vara användbart att läsa andras script för att förstå vad de gör.
  * Vi undviker dock mer svåra frågor om skall programmering, eftersom det bör undvikas.
  * Lika så undviker vi saker som är specifikt för bash, om du verkligen ska programmera i skalet vill du nog lära dig dem.
