---
author: krm
created: 2018-10-14
---
![se]
# Shebang #!

Ett roligt namn, inte sant?

  * Skrivs på första raden av ett scriptfil för att säga vilken tolkare som ska användas.
  * *#!/bin/sh* för att ett skal ska användas för att tolka filen.
  * *#!/bin/bash* för att ett skal ska användas för att tolka filen.
  * Det finns andra tolkare som kan användas.
  * Exempel: *#!/usr/bin/python*, *#!/usr/bin/node* och *#!/usr/bin/perl*.





