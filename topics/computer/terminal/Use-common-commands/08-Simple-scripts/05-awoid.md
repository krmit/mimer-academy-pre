---
author: krm
created: 2018-10-14
---
![se]
# Varför du inte ska programmera i ett skal

Syntaxen för skal programmering är verkligen konstig och svår. Det finns många som programmerar mycket i ett skal, men det finns antagligen lättare spåk att använda om ditt skript:

  * Är längre än ett par dusin rader.
  * Du behöver använda mer än en if sats.
  * Du behöver använda en for sats.
  * Vad som är för mycket beror givetviss på situationen.

Andra språk du skulle kunna använda är python, go, rust eller nodejs.




