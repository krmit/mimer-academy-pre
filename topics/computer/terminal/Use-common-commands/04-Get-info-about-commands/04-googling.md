---
author: krm
created: 2018-10-14
---
![se]
# Googling

Du bör alltid googla dina problem.
  
  * Genom att googla hittar du andra som har haft samma problem som du har.
  * Till din fråga lägger du några söktermer för att få lämpliga resultat, t.ex. "linux", "bash" eller "terminal".
  * Du kan hitta information i forum för programmet eller dokumentation för programmet.
  * Superuser är en sida men svar på många frågor [här](https://superuser.com/).

OBS! Det anses vara mycket oartigt att fråga om saker som du kan hitta genom en enkel googl sökning.


