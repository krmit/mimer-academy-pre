---
author: krm
created: 2018-10-14
---
![se]
# Hjälpsamma flagor

Det finns en flaga "--help" som ger en kortare hjälp medelande som kan vara hjälpsammt.

  * En del program använder förkortning "-h" för denna flaga.
  * Du får en beskrivning av hur programmet ska anropas.
  * Samt ofta detaljer för de viktigaste flagorna. 
  * Ofta visas den information eller en sammanfattning av den när en användare använder programmet fel.

Om du skriver ett eget program se till att ha med en hjälp flaga.




