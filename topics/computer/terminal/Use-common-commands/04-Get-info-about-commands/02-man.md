---
author: krm
created: 2018-10-14
---
![se]
# man

man är ett komando som visar manualen för andra kommandon. Manualen kan vara updelat i olika sektioner, dessa har olika tal assosierat med dem.
  
  * *man Komando*              Manual sidan för *Kommando* kommer att visas.
  * *man Tal Kommando*         Manual sidon för sektion *Tal* för *Kommandot*.
  * Oftas behöver du inte skriva vilken sektion du vill ha utan det finns bara en relvant sida som *man* automatisk visar.
  * Om någon vill referena till en speciel sektion i manualen görs det genom en paratens efter komandot. 
  * Exempel: "printfs(3)" betyder sektion 3 för kommandot *printfs*. 
  * *man 3 printfs*

OBS! på visa system har man tagit bort manual sidorna för att spara plats.Du kan alltid hitta dem på internet. 



