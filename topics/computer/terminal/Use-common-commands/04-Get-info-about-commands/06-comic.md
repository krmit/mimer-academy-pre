---
author: krm
created: 2018-09-30
---
![se]
# Komiska serier

Det kan också var kul att bar läsa något roligt om unix kommanodn, on inte annat för att förstå att det inte är så tråkigt.
  
  * [xkcd](https://www.xkcd.com/) har haft några roliga skämt baserad på system adminstration.
  * [Linux comics zin](https://jvns.ca/linux-comics-zine.pdf) av Julia Evans. Tar upp mycket som vi behandlat här.
  * [CommitStrip](http://www.commitstrip.com/en/?) en serie om programmering och system adminstation. Mycke livsvisdom för en utveklare och/eller admin. 

Tar tacksamt mot fler förslag.


