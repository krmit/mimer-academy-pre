---
author: krm
created: 2018-10-14
---
![se]
# Syntax

Variablar i ett skal skriva alltid med $ i början.
  * Om du vill ha värdet i en variabel, skriv $VARIABEL
  * Exempel: *echo $USER*
  * Exempel: *ls $HOME*
  * Om du vill sätta ett värdet i en variable, skriv VARIABEL="Hej!"
  * Exempel: *TESTA="Detta är ett test"*

Tänk på att syntaxen för hur du sätter en variabel är strikt. Du måste börja raden med variabelnsnamn och du får inte ha några mellan rum kring "=" tecknet.
