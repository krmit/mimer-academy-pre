---
author: krm
created: 2018-10-14
---
![se]
# PATH

Är kanske den variabel som du gör betydelsefylla ändringar till.

  * Är en lista av sökvägar till de mappar som skallet ska leta efter körbara filer i.
  * Det vill säga de program som du kan köra ifrån terminalen.
  * Tecknet ":" används för att dela upp  listan, därför:
  * *export PATH=$HOME/bin/:$PATH;*
  * Instälningar för PATH lägger du i din konfigurationsfil för skalet, för bash är det *.bashrc*

Tips: Det kan vara bra att han en bin mapp i din hem mapp där du kan lägga körbara filer.





