---
author: krm
created: 2018-10-14
---
![se]
# Vanliga miljövariabler

Det finns många miljö variblar, men här är några stycken.

  * $USER            Namnet på nuvarande användaren.
  * $HOME            En absolut sökväg till hemkatalogen.
  * $PWD             En absolut sökväg till den aktuella katalogen.
  * $LANG            Vilket språk som används.
  * $EDITOR          Vilken editor som bör användas.
  * $TERM            Vilken terminal som används, alternativt vilken terminal som terminalen är kompatibelt med.





