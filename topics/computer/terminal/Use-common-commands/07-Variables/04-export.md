---
author: krm
created: 2018-10-14
---
![se]
# Exportera en variabel

Om du deklarerar en variabel såkommer den inte automatisk följa med till andra skript. Därför vill du ofta exportera en variabel.

  * *export Variabel=Värde*              Sätter en variabel *Variabel* till *Värde* och exporterar resultat.
  * Exempel: *export TEST_LIB=~/dev/test*    
  * om du bara vill sätta en variabel för ett scipt eller program kan du skriva "env"
  * Exempel: *env EDITOR="vim" mutt*

Du kommer anatagligen bara stötta på "export" i skript skrivna av andra. Om du kopierar kod se till att inte ta bort "export". 




