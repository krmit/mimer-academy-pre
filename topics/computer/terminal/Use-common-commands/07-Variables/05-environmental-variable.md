---
author: krm
created: 2018-10-14
---
![se]
# Miljö variablar

Det finns variabler som är definerat av OS eller skallet och som påverkar hur dessa fungerar.

  * Dessa variablers värden kan använda av alla program som körs.
  * Variablerna används för att anpassa programmet till den miljö de körs i.
  *





