---
author: krm
created: 2018-10-14
---
![se]
# Visa alla miljö variablar

Det finns kommandon frö att visa alla miljövariabler.

  * *printenv*         Visar alla miljövaribler i systemet. Finns inte för alla system.
  * Om du vill ha en bra utskrift av en varibel som innehåller en lista, kan du i bash använda följande kommando:
  * *echo -e ${PATH//:/\\n}*




