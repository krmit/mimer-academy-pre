---
author: krm
created: 2018-10-14
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
---
![se]

  1. Beskriv med egna ord vad en fil är för något?
  2. Vilket kommando skulle du använda om du skulle skriva ut något på skärmen?
  3. Vilket kommando skulle du använda om du vill göra en utskrift av en hel fil.
  4. Vilket kommanod skulle du använda om du vill skriva ut börja av en fil?
  5. Vilket kommando skulle du använda om du vill skriva ut slutet av en fil?
  6. Vilket kommando skulle du använda om du vill läsa igenom en lång fil?


