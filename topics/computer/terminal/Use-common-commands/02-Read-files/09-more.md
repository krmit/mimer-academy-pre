---
author: krm
created: 2018-09-23
---
![se]
# more

Är ett program för att visa innehållet i en lång fil. Skriver egentligen inte ut uatan använder ett text baserad gränsnitt.
 
  * *more Filnamn*   Visar upp innehållet i Filnamn
  * Du kan använda piltangeterna för att navigera.
  * När du har läst klart trycker du på "q" för att avsluta.
  * Du kommer då tillbaka till din vanliga terminal.
  * På visa system finns det enklare programmet *less* som har liknade funktioner.	





