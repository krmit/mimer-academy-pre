---
author: krm
created: 2018-10-14
---
![se]
# echo

Används för att skriva ut något på skärmen.

  * *echo Text*      Skrivet ut *Text* på skärmen.
  * *echo -n Text*   Skrivet ut *Text* men ingen ny rad.
  * *echo -e Text*   Skrivet ut *Text* och tillämpar specialtecken.
  * Exempel: *echo -en "Test\n;)"*
  * *echo* har många tillämpningar.

OBS! Om du använder *-n* kommer Text först tolkas av skallet vilket gör att visa tecken kommer ge konstiga resultat.





