---
author: krm
created: 2018-10-14
---
![se]
# Anväönda echo för att skapa fil

Vi behöver ofta skapa filer för att testa kommandon. Vi kan använda *ehco*

  * *echo "Hej!" > test.txt*   Skapar en fil som innehåller en text "Hej!"
  * *>* kommer förklaras i en senare avsnitt. 
  * Du skulle också kunna skapa en tom fil med kommadot *touch Filnamn*.





