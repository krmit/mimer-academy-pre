---
author: krm
created: 2018-10-14
---
![se]
# tail

Visar de sista raderna i en fil.

  * *tail Filnamn*             Visar de sista raderna i filen *Filnamn*
  * *tail -n Finamn*           Visar de sista *n* raderna i *Filnamn*.

Kan vara bra om du vill titta i en lång log fil och se vad som hände senast. Till exempel om programmet har kraschat och du vill veta varför.




