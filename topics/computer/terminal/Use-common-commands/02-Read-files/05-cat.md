---
author: krm
created: 2018-10-14
---
![se]
# cat

cat använder vi till att skriva ut innehållet i en fil.

  * *cat Filnamn*        Skriv ut innehållet i filen *Filnamn*.
  * *cat -n Filnamn*     Skrivet ut innehållet med radnumreringar.
  * Om texten blir för lång använd kommando "more" istället. 

*cat* kombineras ofta med *pipes*.




