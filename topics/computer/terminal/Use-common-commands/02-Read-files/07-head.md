---
author: krm
created: 2018-09-23
---
![se]
# head

Visar de översta raderna i en fil.

  * *head Finamn*             Visar de översta raderna i en *Filnamn*. 
  * *head -n Finamn*          Visar de första *n* raderna i *Filnamn*.

Kan vara bra om du vill titta i en lång log fil och se vad som hände när logen startades.





