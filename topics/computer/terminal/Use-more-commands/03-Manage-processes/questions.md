---
author: krm
created: 2018-10-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Vad menas med en process i ett OS?
  2. Vilket program i terminalen kan du använda för att få se en interaktiv lista över processerna i en dator.
  3. Vilket kommando kan du använda om du vill se alla processer i ett OS?
  4. Vilket kommando kan du använda för att stänga ned en process baserad på process id?
  5. Det finns ett enkelt kommando för att stänga ned en process baserat på dess nman. Vad heter det komandot.
  6. Hur skulle du göra för att starta om en linux system om du inte har tillgång till den fysiska maskinen?
  7. Vad means med en demon("Deamon") i ett operativsystem?
  8. Förklara ett server program kan köras på en dator även när du loggar ut.

