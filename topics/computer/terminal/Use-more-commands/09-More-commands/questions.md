---
author: krm
created: 2018-10-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Vad används kommandot alias till?
  2. Vad används kommandot date till?
  3. Vad används kommandot wget till?
  4. Vad används kommandot cal till?
  5. Vilket kommando skulle du tipsa någon om att använda?
