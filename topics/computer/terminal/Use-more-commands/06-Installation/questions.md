---
author: krm
created: 2018-10-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Vilken packethanterare föredrar du?
  2. Om du är på ett debian baserat distro så som Ubuntu, Mint med flera. Vilken packethanterar använder du då?
  3. På vilket sätt använder man en packethanterare på ett UNIX system?
  4. Nämn en packethanterare som främst är till för att installera mjukvara ifrån sin källkod?
  5. Hur installerar du ett padket med apt?
  6. Hur tar du bort ett packet med apt?
  7. Nämn ett sätt att installera program utan att använda en packethanterare.
