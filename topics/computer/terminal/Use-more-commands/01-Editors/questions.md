---
author: krm
created: 2018-10-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Vilken är din favorit editor?
  2. Om du har använt ssh för att komma in i en terminal. Vilket program skulle du kunna använda för att änddra en fil?
  3. Vilken editor är standar för alla POSIX system?
  4. Vilka kommandon skulle du använda för att spara en fil och avsluta editorn i vi/vim?
  5. Hur kan du göra för att editera en fil på server som du bara kommer åt med ssh, med en grafisk editor?
  6. Varför tror du det är så stor konflikt mellan vim och emacs användare? Ge en bra motivering.

