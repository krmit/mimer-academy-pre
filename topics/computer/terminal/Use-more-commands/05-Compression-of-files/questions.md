---
author: krm
created: 2018-10-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Vilket program skulle du välja för att komprimera filer? Varför?
  2. Vad innebär det att en fil blir komprimerad?
  3. Finns det andra skäll att använda ettkomprimeringsprogram än att få mindre storlek på filer?
  4. Vad är fördelen med att använda zip jämför med andra komprimeringsformat?
  5. Hur ska du använda tar för att pack upp något?
  6. Hur ska du använda tar för att komprimmera något?
  7. Vilka fördelar har 7-zip programmet.
  8. Nämn några komprimeringsprogram förutom zip, tar och 7-zip.
  

