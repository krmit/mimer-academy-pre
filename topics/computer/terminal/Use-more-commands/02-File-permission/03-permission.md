---
author: krm
created: 2018-11-14
---
![se]
# Olia rättigheter

Varje fil har tre olika typer av rättigheter. Dessa kan representeras med en bokstav eller ett tal.

  * Läsbar("read")       **r**        **4**
  * Skrivbar("write")    **w**        **2**
  * Körbar("execute")    **x**        **1**

Valet talen är förståligt om du tänker på det binärt. 4 - 100, 2 - 010 och 1 - 001. 
  





