---
author: krm
created: 2018-10-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Vilka olika typer av rättigheter finns det för en fil?
  2. Vilka bbokstäver brukar vi använda för att beskriva rättigheterna på en fil?
  3. Vilken olika användare har vi av en fil?
  4. Vad används kommandot chmod till?
  5. Vilket kommando ska du använda om du vill byta grupper på en fil?
  6. Vilken kommando ska du använda om du vill byta användare på en fil?

