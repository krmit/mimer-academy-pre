---
author: krm
created: 2018-10-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Vad menas med en arbetsminne?
  2. Vilken typ av minne har vi i en hårddisk?
  3. Vilka olika typer av mine har vi i en dator.
  4. Ge exempel på saker som kan förvira när man beräknar minnes använding på en dator.
  5. Vilken koammndo kan man använda för att se hur mycket minne det fins kvar på olika lagringsenheter?
  6. Vilket kommando kan man använda för att se hur stor en mapp är?
  7. Vilket kommando kan man använda för att se hur mycket arbetsminne som vi använder? 

