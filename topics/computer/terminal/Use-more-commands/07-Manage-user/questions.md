---
author: krm
created: 2018-10-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 
    - type: short
      answer: interpretation 

---
![se]

  1. Hur skapar du en ny användare?
  2. Vilka problem finns med komandot useradd?
  3. Vilka problem finns med komandot adduser?
  4. Ge exempel på när det kan vara bra att ha flera användare.
  5. Vad är root användare och vad gör den speciel?
  6. Hur gör du för att skapa en ny grup?
  7. Hur gör du för att lägga till en till grupp till en användare.
  8. Vad har root användaren för hemkatalog?
  9. Vad gör sudo kommandot?
  10. Varför kan det vafra bra att ha en sudo grup? 
