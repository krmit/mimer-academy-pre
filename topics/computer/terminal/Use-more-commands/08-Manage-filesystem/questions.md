---
author: krm
created: 2018-10-29
questions:
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short

---
![se]

  1. Vad är ett filsystem?
  2. Hur skulle du göra för att ta reda på vilket filsystem som används?
  3. Vad menar vi med en enhet("device")?
  4. Vad finns i filen /etc/fstab/ i ett vanligt linux system?
  5. Hur använder du kommanodt "mount" för fästa en enhet på mapp?
  6. Om du måste specifiera vilket filsystem som ska användas vilken flaga anvönder du då?
  7. Vilket program använder du för att ta bort ett filsystem som har blivit monterad på en mapp?
  
