---
author: krm
created: 2018-08-30
---
![se]
# Olika roller i ett projektarbete

  * **Beställaren**, den som betalar för projektet och har rätt att fatta beslut på beställarens vägnar.

  * **Projektledaren**, den som är ledare för projekt och fördelar arbetsuppgifter samt ansvarar för utvärdering.

  * **Utvecklaren**, den som utför själva arbetet. Observera att beroende på förmåga och andra faktorer kan det vara bra att även beskriva olika roller för utvecklare, till exempel kan man tänka sig att vi har testare.





