---
author: krm
created: 2018-08-30
---
![se]
# Andra roller

Dessa roller förekommer i arbetslivet och kan vara lite kul att kunna. Observeraa att dessa benämningar är egentligen något godtyckla och det beror på vilken företagskultur som råder på den aktuella arbetsplatsen.

  * **Junior devloper:** Är en utveklare utan erfarenhet ifrån arbetslivet eller likande. Utveklaren håller på att lära sig yrket.
  * **Senior devloper:** Är en utveklare med flera års efrenhet av teknologin som används. Har ofta en ledande roll i projekt.
  * **Tech lead:** Eller "Lead programmer". Är en utveklare som är ansvarig för en viss teknologi inom en organisation. Förväntas ha stora kunskaper inom detta område.




