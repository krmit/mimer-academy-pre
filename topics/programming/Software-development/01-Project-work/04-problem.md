---
author: krm
created: 2018-08-30
---
![se]
# Vilka problem har vi när vi jobbar i ett projektarbete/grupparbete?

  * Ansvaret kan bli otydligt.
  * Uppgifter blir inte gjorda eller utförs dåligt för ingen tar ansvar.
  * Kommunikation inom gruppen kan bli en vässentlig del av arbetet och mindre tid läggs till att lösa problemmen.
  * Dokumentation tar upp för mycket tid och blir ett mål i sig.
  * Bra inovativa lösningar blir inte av eftter som de är för svåra att motivera i en grupp.





