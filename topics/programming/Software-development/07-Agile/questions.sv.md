# Frågro om Agile

  1. Hur går man tillväga för att lösa en uppgift enligt Agil?
  2. Nämn minst en konkret metod som bygger på Agil.
  3. Rita upp en tänkt utvecklingscykel för ett programmeringsprojekt som använder agil. 
  4. Vilka fördelar har Agil?
  5. Vilka nackdelar har Agil?


