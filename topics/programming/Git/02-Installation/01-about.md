---
author: krm
created: 2018-06-28
---
![se]
# Om installation av git 

Det finns många variantera för att installera git beronde på OS och vilken utvecklingsmiljö du använder.

  * Många Linux varianter kommer med git förinstallerad. Testa genom att skriva "git" i terminalen.
  * Det finns mång grafiska alternativ till att använda git ifrån terminalen. Sök eget *git clients GUI*.
  * Många program har inbyggd stöd för git. Var dock försiktig när du använder dem så du använder git på rätt sätt.






