---
author: krm
created: 2018-09-03
---
![se]
#  Installera på windows

Det finns olika alternativ. Leta upp en installations instruktion för det alternativ du tycker är bäst.	

  * msysgit - ett alternativ som är likt hur du använder git ifrån konsolen på linux.
  * tortoiseGit - Ett klient program för windows som är grafiskt och väl integrerad i Windows.
  * Många utvecklingsmiljöer så som *Visula Studio* har integrerad stöd för git.







