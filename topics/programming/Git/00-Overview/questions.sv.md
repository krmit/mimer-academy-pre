# Om versionshantering

Versionshantering används idag av de flesta programmerare.

1. Vad är ett versionshanteringssystem?
2. Vilket program är idag vanligast?
3. Vad menar vi med i detta sammanhang med ett “arkiv”?
4. Vad är det för fördel att vi har flera självständiga arkiv i stället för ett centralt?
