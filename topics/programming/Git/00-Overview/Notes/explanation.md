---
author: krm
created: 2018-09-03
---
![se]
# Vad är git?

Ett av det stora praktiska problemet man har som programmerare och framför allt bland programmerare som arbetar i större projekt är att hålla ordning på sin källkod. Lösningen på detta problem är ett versionshanteringssystem.  Detta program håller reda på källkoden och alla ändringar i det. Programmerare arbetar med versionshanteringssystemet genom att bokföra alla ändringa som programmeraren gör, sedan kan programmerare sprida dessa ändringar till andra som arbetar med samma program. Platsen som dessa ändringar sparas på kallas arkiv.

Under årens lopp har det funnits många versionshanteringssystem, men under de senaste åren har ett program blivit de facto standard nämligen git. Att kunna hantera git är en nödvändighet för en yrkesverksam programmeraren eller åtminstone för någon som vill bli det.

Gir är ett distribuerat system det betyder att vi har ett lokalt arkiv dit vi för för in ändringar, sedan kan vi ha flera andra arkiv som vi synkar mot. Vanligaste är att vi har ett arkiv på en webbserver som är vår “master” och till detta arkiv skickar vi våra ändringar när vi är klara nöjda med vår nya kod i programmet. Alla arkiv är dock innehåller lika mycket information och är självständiga.



