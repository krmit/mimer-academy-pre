---
author: krm
created: 2018-09-01
---

![se]

# Om detta avsnitt

- Git används ofta tillsammans med en webbsida
- Men här tittar vi bara på git som ett progam.
- De grunläggande funktionerna vi här diskuterar är inte svåra att förstå.
- Men det finns mer att lära sig för att kunna fungera bra i ett utvecklingsteam.
