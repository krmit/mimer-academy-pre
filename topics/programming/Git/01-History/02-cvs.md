---
author: krm
created: 2018-09-04
time: 1986
---
![se]

# CVS skapas

  * “Concurrent Versions System” mer känd under sin förkortning cvs publiceras.
  * Programmet blir så småning open-source och får stort genomslag.
  * Med åren får den också mycket kritik och många vill ersätta det.
