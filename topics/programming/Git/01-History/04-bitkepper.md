---
author: krm
created: 2018-09-03
time: 2005
---
![se]

# BitKeeper tar upp kampen

  * BitKeeper börjar användas för att hantera källkoden till Linux kärnan.
  * BitKeeper var ett kommersiellt program som kunde användas grattis för vissa ändamål. 
  * Detta leder till kritik inom öppenkällkods rörelsen.
  * Tillslut haverar sammarbetet mellan BitKepper och Linux hackers.
