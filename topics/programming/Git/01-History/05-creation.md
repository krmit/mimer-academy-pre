---
author: krm
created: 2018-09-03
time: 2005-04-07
---
![se]

# Git publiseras

  * Efter att ha tänk på problemmet med versionhanterare en längre tid skriver Torvald sitt eget program.
  * Det var ett litet program som bara tagit 4 dagar att skriva.
  * Den framgångrika idén ligger i hur data är organiserat.
  * Det fungerar bra för proffersionella ända mål, men är svårt att använda för vanliga användare.
  
***

Linus Torvalds tröttnar på allt bråk kring BitKeeper och skapar baserad på verktyg som han använde för att hantera kernel tidigare. Programmet heter “git” och tanken är att det ska undvika mycket av de dumheter som tidigare versionshanteringssystem har haft i sin design. Git är först mest till för kernel programmerare och svår använt, men det sprider sig snart och användningen förenklas.
