---
author: krm
created: 2018-09-03
time: 2011
---
![se]

# Github tar över

  * Blir sajten github störst bland webb arkiv för öppen källkod.
  * Github har idag en särställning inom programmering världen som en sajt för att dela källkod mellan programmerare.
  * Samtidigt tynar de flesta konkurrerande system till git bort.
