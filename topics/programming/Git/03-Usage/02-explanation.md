---
author: krm
created: 2018-09-05
---
![se]
# Hur använder man git?

Vanligt viss gör man på följande sätt om du använder en terminal.

  * Du går till den aktuella mappen.
  * Du skriver ditt git commando.
  * Komandot består dels av *git* följd av ett underkomando.
  * Det kan också förekomma en flaga den börjar med "-".

***

Detta är en enkel instruktion för att du ska kunna använda git till enklare uppgifter. Det är dock värt att noter att git är ett mycket kraftfullt program som kan användas till all sorts programmeringsutveckling, även till mycket stora och komplicerade projekt. Därför har “git” väldigt många funktioner som en användare inte behöver kunna för att göra enkla saker. Det räcker ofta med att kopierar kommandona ifrån detta avsnitt och sedan använda dem i enlighet med de resurser som finns.





