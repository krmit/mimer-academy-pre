---
author: krm
created: 2018-07-27
---
![se]
# Några funderingar

Fundera gärna över frågorna nedan.

  * Vad är programmering?
  * Hur får vi en dator att göra vad vi vill?
  * Hur "tänker" en en dator?
  * Hur fungerar den?
  * Varför ska vi programmera?
  * Men framför allt hur programmerar vi den?







