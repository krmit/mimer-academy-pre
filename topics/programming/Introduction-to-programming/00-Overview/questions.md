---
author: krm
created: 2018-06-29
questions:
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation
    - type: short
      answer: interpretation
---
![se]


  1. Vad är ett program?
  2. Vad är ett programmeringsspråk?
  3. Vad är en funktion?
  4. Vad är en parameter?
  5. Vad betyder det att en funktion returnerar ett värde?

