---
author: krm
created: 2018-07-27
---
![se]
# Mer om att lära sig programmera

Om du tycker vi går fram för långsamt här eller känner för fler utmaningar så får du n gärna programmera på egen hand. Det finns idag många olika sätt att lära sig programmera på internet. Du kan själv söka upp någon och testa. Om du söker en riktig utmaning så finns programmeringsolympiaden. Det är en tävling för alla gymnasister i sverige i programmering. Vinnaren tävlar utomlands i internationella tävlingar. Även om du inte vill tävla är uppgifterna i tävlingen riktigt bra om du vill förbättra dig som programmerare.
