---
author: krm
created: 2018-07-27
---
![se]
# Om funktioner och spelet "Light bot"

Låt oss tänka på en funktion och vad det är för något. För funktionerna är grunden för all programmering. Om vi vill att vårt program ska göra något så får vi ofta använda oss av funktioner på ett eller annat sätt. Funktionerna tar ett eller flera värden och lämnar ifrån sig ett annat värde.

Vi kan också säga att funktionen har ett eller flera parametrar, dvs säga värden som vi ger funktionen och att den returnerar ett värde. Funktionens viktigaste uppgift är att förenkla din kod så den blir lättare att skriva.

