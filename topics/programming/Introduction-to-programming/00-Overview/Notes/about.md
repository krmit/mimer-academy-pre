---
author: krm
created: 2018-07-27
---
![se]
# Om programmering

Vad menar vi med ett program? Ett program är lista av kommandon som styr en dator. Programmen skrivs i ett programmeringsspråk, det finns väldig många olika programmeringsspråk och de kan skiljer sig högst väsentligt ifrån varandra. Både hur de ser ut, men även den bakomliggande filosofin. Det är viktigt att förstå att alla programmeringsspråk kan användas till allting, men det är finns vissa områden där språket är opraktiskt att använda. Därför finns det många olika programmeringsspråk med sina styrkor och svagheter. Du har ofta många alternativ att välja på för att utföra en viss uppgift. Det kan vara bra att välja ett enkelt språk när du börjar programmera och sedan lära dig andra när du blir bättre på som programmerare. Men framför allt vill du lära dig ett tankesätt, du vill lösa problem som en programmerare.

