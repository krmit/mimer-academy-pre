---
author: krm
created: 2018-07-27
---
![se]
# Om spelet "Light bot"

Det bästa sättet att bli bra på och förstå programmering är att programmera. Försök att alltid hitta egna små programmeringsprojekt att göra. Eller varför inte testa något programmeringsspel för att komma igång?

Ett sådant spel är "Light bot". Spelet är ett så kallat “flashspel” och har funnits mycket länge. Det har blivit så populärt att det har kommit en version 2 som är mycket omfattande. Men den första version har fördelen av att vara enklare och mer fokuserat på ett mål. Nämligen att få dig som spelare att förstå varför man använder sig av funktioner.

