---
author: krm
created: 2018-09-27
---
![se]
# Hur vet vi om något är ett nummer?

Det finns flera sätt att göra detta på.

  * *isNaN* är sant om parameter inte är ett tal. Används tillsammans med inte operatorn "!".
    * *!isNaN(5)* *isNaN("Hello")*
  * *isFinite* är sant om parametern är ett tal och inte är "Infinity".
    * *isFinite(5)* *isFinite(Infinity)*
  * *isInterger* sant om parametern är ett heltal.
    * *isInteger(5)* *isInteger(3.14)* 







