---
author: krm
created: 2018-09-27
---
![se]
# Några kommentar om Numbers

Number beter sig oftast "snällt", men det finns tillfällen då datorns implementering av Number har betydelse.

  * Representerar heltal upp till 15 siffror korrekt. *9999999999999998+1*
  * Avrundning fel för decimaltal kan förekomma. *0.2+0.1*

Obs! Dessa problem är inget unikt för js utan finns i alla språk när du använder dig av flyttal.







