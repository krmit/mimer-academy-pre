---
author: krm
created: 2018-09-27
---
![se]
# Konvertera mera

Det finns några egendomliga som uppkommer pga. ovanliga Number konverteringar.

  * *Number({}) === NaN*
  * *Number([]) === 0*
  * *Number(true) === 1*
  * *Number(false) === 0*

Konverteringen av booleans är logisk eftersom det är de värden som true och false har i matematiken.






