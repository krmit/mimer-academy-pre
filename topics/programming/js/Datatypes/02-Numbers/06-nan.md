---
author: krm
created: 2018-09-30
---
![se]
# Vad är NaN?

NaN är ett speciall siffra som används i js för att säga att detta är inget tal.

  * NaN är förkortning av "Not a Number"
  * *typeof NaN === "number"*
  * *Number("abc") === NaN*
  * *-NaN*
  * *NaN*NaN+3*





