---
author: krm
created: 2018-09-30
---
![se]
# Vad är Infinity?

Infinity är ett speciall siffra som används i js för att säga att detta är resultatet av en odefinerad beräkning. Infinit kan ha posetivt eller negativt tecken.

  * *typeof Infinity === "numbers"*
  * *1/0*
  * *-5/(5-5)*
  * *Infinity+Infinity* *Infinity*Infinity*
  * OBS! *Infinity-Infinity===NaN*




