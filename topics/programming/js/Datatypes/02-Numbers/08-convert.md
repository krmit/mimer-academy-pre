---
author: krm
created: 2018-09-27
---
![se]
# Att konvertera till ett Number.

För att  konvertera till ett Number använder vi funktionen Number().

  * *Number(5)* Tips: Du kan nu använda de inbyggda funktionerna för Number.
  * *Number("5")* *Number("3.14")* *Number("555.555")*
  * *Number("-5")*
  * Dessa är NaN: *Number("5/5")* *Number("One")*







