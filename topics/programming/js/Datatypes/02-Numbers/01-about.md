---
author: krm
created: 2018-09-30
---
![se]
# Om hur js hanterar tal

Till skilnnad ifrån många andra språk finns det bara en typ för tal i js, men tal kan ändå ställa till många problem.

  * Många av exemplerna på hur dåligt js är kommer ifrån konstiga tal konverteringar.
  * Tal kan ge upphov till egendomliga avrundningsfel och detta är inbyggd i js.
  * Vi försöker här undersöka vad tal är och ovanstånde problem.





