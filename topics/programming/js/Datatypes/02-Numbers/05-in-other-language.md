---
author: krm
created: 2018-09-27
---
![se]
# Hur ser det ut i andra språk?

I många nadra språk så som C, C00, C#, java med flera skiljer man på olika typer av tal.

  * Det finns *double* eller *float* för flytal.
  * Och *int* med flera för heltal.
  * Antalet byte kan skilja mellan olika typer av tal.
  * Att optimera minnes användingen kan vara en viktig del.







