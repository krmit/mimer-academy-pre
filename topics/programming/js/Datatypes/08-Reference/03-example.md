---
author: krm
created: 2018-09-10
---
![se]
# Exempel på hur referenser kan missförstås

```javascript
let a = 5;
let b = a;
b=0;
console.log(a);   //5

let o={};
o.a = 5;
b=o;
b.a=0;
console.log(o.a);  //0
```

Egentligen inte så förvirande om du tänker på objekt och referenser.







