---
author: krm
created: 2018-09-27
---
![se]
# Att kopiera Object

Det kan vara svårt att kopiera ett objekt eftersom det kan vara svårt att samtidigt kopiera eventuella referenser.

  * En djup kopia ("deep copy") innebär att alla data kopiera till ett nytt objekt och det sker rekursiv för alla objekt som finns. Resultatet blir att inga referenser blir kvar mellan det ursprunglia objektet och kopian.
  * En yttlig kopia("shallow copy") innebär att alla värden i objektet kopieras. Det innebär att referesner till bli kopierade men inte själva objekten som kommer vara samma i både orginalet och kopian. 





