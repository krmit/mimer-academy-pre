---
author: krm
created: 2018-09-27
---
![se]
# Finns Object i andra språk?

Objekt i JS har funktoner som återfinns i många olika koncept i andra språk.

  * I C++, java eller C# används klasser för att skapa objekt.
  * Det finns också en datatype Map i många språk som har en liknade funktion.
  * I python kallas denna för *Dictionaries*.






