---
author: krm
created: 2018-09-27
---
![se]
# Att sätta in ett värde i ett Object

Ett värde kan tilldelas genom att använda "=" och en hård parantes.

  * Exempel: *let o={}* - *o["tre"]=3* - *o["points"]=35*
  * Exempel: *o[1]="ett"* - *let a="result"; o[a]=35*
  * Fry gåt också istället för paranteser bara använda en punkt "." och namnet på egenskapen.
  * Exempel: *o.tre = 3* - *o.points = 35*

Den senare syntaxen är den som föredras i många samnahang. Parantesrna ska bara användas om det inte går med punkt.
  







