---
author: krm
created: 2018-09-27
---
![se]
# assign

Om vi istället vill ha en yttlig kopia kan vi använda kommandot *assign*.

  * Assign används egentligen för att kombinera två objekt.
  * För att göra en kopia kan du göra detta:

```javascript
let my_copy = assign({},)
```





