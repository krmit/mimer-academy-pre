---
author: krm
created: 2018-09-27
---
![se]
# Method för att göra en djup kopia av ett Object

Det finns ingen inbyggd funktion för dettam utan man får använda sig av olika metoder. 
  * Denna metod som presenteras här är kanske inte intuitiv
  * men det är den mest effektiva på de flesta system.
  * Vi omvandlar helt enkelt objetet till ett text och parsar den sedan för att få tillbaka objektet.

```
let my_copy = JSON.parse(JSON.stringify(my_object));
```




