---
author: krm
created: 2018-09-27
---
![se]
# Vad kan vi använder Object till?

Till nästan allt!

  * För att representera något i vår kod
  * För att hålla ordning på metoder i ett bibliotek.
  * För att kapsla in värden saå att koden inte blir krånglig.
