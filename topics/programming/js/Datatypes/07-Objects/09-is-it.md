---
author: krm
created: 2018-09-27
---
![se]
# Är detta ett Object?

Det går att använa typeof eftersom den returnerar "object", men tänk på:

  * Att detta är sant även för Array:er. Eventuellt får du undersöka om det inte är en Array.
  * Det är inte sant för funktoiner där *typeof* ger värde "function".

Ofta vill du bara testa om ett värde har en egenskap heller inte. Då räcker det att testa och blir resultatet "undefined" finns den inte.

```javascript
if(o !== undefined && o.p !== undefined) {
   console.log("Objectet o har en egenskap p!");
}
```





