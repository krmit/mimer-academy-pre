---
author: krm
created: 2018-09-10
---
![se]
# Vad är ett object?

Object är ett samling av egenskaper.

  * En egenskap består i sin tur av namn och värden.
  * Namnet används för att hitta ett värde.
  * Ett värde kan vara av vilken typ som helst.
  * Om värdet är en funktion kallas egenskapen för en metod.
  * Ett namn är en String eller en Symbol. Används andra typer som namn kommer de konverteras till String. 







