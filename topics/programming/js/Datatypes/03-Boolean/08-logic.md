---
author: krm
created: 2018-10-21
---
![se]
# Logik

Ofta använder vi logiska operatorer tillsammans med Boolean. 

  * Och: "&&"
  * Exempel: *true && true //true* - *true && false //false*
  * Eller: "||"
  * Exempel: *true || false //true* - *false || false //false*
  * Inte: "!"
  * Exempel: *!true //false* - *!false //true*

Observer de dubla teckena för "&&" och "||", att bara använda ett tecknena kommer ge oss en helt annan operator.






