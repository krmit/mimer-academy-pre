---
author: krm
created: 2018-10-21
---
![se]
# Vad är Symbole?

Symbole är ett unikt värde som inte lika med något annat än sig själv i din kod.

  * Varje gång *Symbole* anropas skapas ett nytt värde.

```javascript
let s1 = Symbole();
let s2= nSymbole();
s1 === s2 //false
```

  * Du använder inte syntaxen *new Symbol()* för att skapa en ny symbol.
  * Du kan sicka med symbile en text för att påmina dig om vad symbole är tillför.
  * Exempel: *new Symbole("Hej");*




