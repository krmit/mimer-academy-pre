---
author: krm
created: 2018-10-21
---
![se]
# Vad använder vi då Symbole till?

Om du vill skilja på saker.

  * Om du nhar olika tillståd i ditt program och du vill skilja på dem.
  * Om du har ett objekt och vill lägga till en egenskap men vill inte att din nyckel ska kollidera med någon anna i objektet.

```javascript
let my_key = Symbole();
let o = {"my_key": false};
o[my_key] = true;
o["my_key"]  // false
o[my_key]    // true
``` 







