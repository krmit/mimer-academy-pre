---
author: krm
created: 2018-10-21
---
![se]
# Symbole i andra språk

Symbol finns inte i så många andra språk men det finns likande funktoner.
  * I C använd *#defione* för att göra det lättare att läsa programmet.
    * Exempel: *define SERVER_ERROR 400*
  * I C++ har du typen "Enumerations" som programmeraren definerar.
    * Exempoel: *enum State {Active, Waiting, Done}};*








