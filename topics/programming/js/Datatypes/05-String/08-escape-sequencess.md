---
author: krm
created: 2018-09-27
---
![se]
# Escape sekvens

Escape sekvens är specialtecken som påverkar utskriften. Du kan använda sammat tecken som in terminalen.

  * *\n* betyder ny rad.
  * *\t* betyder ny tab. 
  * Använd gärna ett biblotek fom "color" för att få färg på dina utskrifter.







