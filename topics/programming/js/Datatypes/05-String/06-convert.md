---
author: krm
created: 2018-09-27
---
![se]
# Hur konvertera något till en sträng?

Det är ofta nödvändigt att typomvandla till en sträng.

  * let one = String(1);
  * ""+1;
  * Number(1).toString();
  * Om du har ett objekt så kan du implmentera en method *toString* för att få automatisk omvandling.







