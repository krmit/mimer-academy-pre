---
author: krm
created: 2018-09-27
---
![se]
# Hur vet vi att en variabel är en sträng?

Det är faktiskt rätt så lätt.

  * let b = "Bamse";
  * typeof "Bamse" === "string";
  * typeof "abba";







