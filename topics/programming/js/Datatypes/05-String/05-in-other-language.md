---
author: krm
created: 2018-09-27
---
![se]
# Strängar i alla språk?

Stängar finns i alla språk.

  * I visa äldre språk så som C har inte stöd för UNICODE.
  * Visa enkla metoder för sträng hantering sakans i t.ex. C++.
  * I php blir mycket automatiskt typomvandlat till strängar.







