---
author: krm
created: 2018-09-27
---
![se]
# Några kommentar om Arrayer

En array är av typen objekt. array är en under typ till object.

  * Så allt du kan göra med objekt kan du också göra med en Array.
  * *typeof* av en Array kommer vara "object".
  * Du bör försöka att undviak att använda en Array som ett Object.
