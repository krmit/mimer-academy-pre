---
author: krm
created: 2018-09-10
---
![se]
# Vad är Arrayer?

Array är en lista av värden. Värden kan vara av vilken typ som helst.

  * En lista defineras på fölajnde sätt:
```
 let my_list = [1,"b",true];
```

  * Vi kna ha listor i listor också.
```
 let my_list = [1,"b",true];
```





