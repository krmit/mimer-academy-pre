---
author: krm
created: 2018-09-27
---
![se]
# Finns Arrayer i andra språk?

Alla språk har stöd för någon typ av Array, men syntaxen kan vara betydligt krångligare. Anledningen är att Array är en rätt så ineffektiva datastruktur och om du hanterar minnet mer så kan du försöka använda något mer effektivt.

  * I C och C++ föredrar man inte dynamsika listor som också kallas Array.
  * I Python hanteras array på ungefär samma sätt som i JS.
  * PHP har också arrayer men dessa beter sig mer som objekt i JS.






