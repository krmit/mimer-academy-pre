---
author: krm
created: 2018-09-27
---
![se]
# Vad kan v använder Arrayer till?

Listor kan användas för att hantera data på många olika sätt.

  * Det finns två vanliga sätt att använda en Array:er på:
    * Som en kö, först in blir också först ut.
    * Som en stack, dvs hög, Sist in blir först ut.
