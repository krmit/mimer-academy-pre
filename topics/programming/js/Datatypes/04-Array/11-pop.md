---
author: krm
created: 2018-09-27
---
![se]
# Pull

Du kan ta bort sista värdet och returnera det genmom att använda metoden "pop".

```javascript
let arr=["a","b","c"];
let c = arr.pop();
console.log(arr);
// ["a","b"]
console.log(c);
// c
```

Du kan ta bort sista värdet och returnera det genmom att använda metoden "shift".

```javascript
let arr=[0, 1, 2];
let zero = arr.shift();
console.log(arr);
// [1,2]
console.log(zero);
// 0
```
