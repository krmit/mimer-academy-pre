---
author: krm
created: 2018-09-27
---
![se]
# for-of

En bättre forsatts om du arbetar med arrayer.

  * Du ska använda *for-of*, inte *for-in* eller någon anna variant. 

```javascript
let arr = ["a", "b", "c"];

for(const letter of arr) {
  console.log(letter);
}
```






