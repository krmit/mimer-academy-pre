---
author: krm
created: 2018-09-27
---
![se]
# Är detta en Array?

Det går inte att bara använda "typeof" eftersom Array också är ett objekt.

  * Använd metoden *Array.isArray(array)*, *array* är metoden du vill undersöka.
  * Exempel: *Array.isArray([])* - *Array.isArray({})* - *Array.isArray(true)*
  * På riktig gammal webbläsare fungerar inte det, men det är inget problem för oss.
  
Ett annat förslag är något liknade *[] instanceof Array*, men denna metod har tyvärr en del problem som du  märker om du testar med *{} instanceof Array*. 






