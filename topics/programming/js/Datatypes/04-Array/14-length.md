---
author: krm
created: 2018-09-27
---
![se]
# length

Är kanske den alla vanligase använda egenskaper hos en array.

  * Ger helt enkelt längden på en array
  * Används ofta i *for* satser.
  
```javascript
let arr = ["a", "b", "c"];

for(let i = 0; i < arr.length; i++) {
  console.log(i+": "+arr[i]);
} 
```





