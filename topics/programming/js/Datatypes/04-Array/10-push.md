---
author: krm
created: 2018-09-27
---
![se]
# Push

Du kan lägga till ett värde sist i en Array genom att använd metoden *push*. 

```javascript
let arr=["a", "b"];
arr.push("c");
console.log(arr);
// ["a","b","c"]
```

Du kan lägga till ett värde först i en Array genom att använd metoden *shift*. 

```javascript
let arr=[1, 2];
arr.shift(0);
console.log(arr);
// [0,1,2]
```






