---
author: krm
created: 2018-09-27
---
![se]
# splice

Splice är en metod som tar bort element i en array, den kan också lägga till värden på en valfritt index. Den har två eller fler parametrar.
  * En start index där element ska tas bort eller läggas till ifrån.
  * En längd som avgör hur många element som ska tas bort.
  * Ett antal parametrar som kommer adderas.
  * Det är den array som anropar slice som kommer förändras.

```javascript
let arr = ["a", "q", "d"];
arr.splice(1,1,"c","d");
console.log(arr);
// ["a". "b", "c", "d"]
```







