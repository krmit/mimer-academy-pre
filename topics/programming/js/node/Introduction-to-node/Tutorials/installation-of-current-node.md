# Installation av senaste node

Vi kommer här installerra senaste versionen av node.

OBS! Denna metod kommer bara installera node för en vanlig användare, du kommer inte kunna köra dessa skript som node.

Nedan så betyder "X.Y.Z" den akturlla version du vill använda. Sist detta dockument uppdaterades var det 10.11.0.

  1. Se till att du har en map där du vill spara node js programmet i och öppna en terminal i den mappen.
    * Till exempel en *app* map direkt under din hem mapp.
```bash
     mkdir app
     cd app 
```
    
  2. Gå in på nodejs hemsidan: [node](https://nodejs.org/)
  3. Tryck på knappen för att ladda ned senaste version "X.Y.Z Current" och spara filen i mappen 
    * Du kan också ladda ned direkt i terminalen med följande komando:
```bash
   wget https://nodejs.org/dist/vX.Y.Z/node-vX.Y.Z-linux-x64.tar.xz"
```

  4. Packa upp arkivet.
```bash
   tar -xvf node-vX.Y.Z-linux-x64.tar.xz
```

  5. För att få ett enklare namn byt namn på mappen till bara "node".  
```bash
   mv node-vX.Y.Z-linux-x64 node
```

  6. Öpnna filen ".bashrc" i din hemkatalog och lägg till följande rad.  
```bash
   export PATH=$HOME/app/node/bin:$PATH
```
  * Om du redan använder krms konfigurations filer så finns redan denna rad.
  * Du kan också skiva följande kod:
```bash
   echo "export PATH=\$HOME/app/node/bin:\$PATH" >> ~/.bashrc
```

  7. Du kan köra nodejs program med "node" och installa packet med "npm". Observera dock att alla blir installerade för den lokala användaren ocxh du kan inte köra något program med sudo. 
