---
author: krm
created: 2018-11-25
questions:
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short


---
![se]

  1. Vad menas med *this* i js kod?
  2. Vad används objektet *this* till?
  3. Från vilket objekt kommer *this* få sin egenskaper? 
  4. Vad gör metoden "Object.call"?
  5. Ge ett exempel när det kan vara bra att använda *this*.
  6. Vilka problem kan uppstå  när man använder parametern *this*?
