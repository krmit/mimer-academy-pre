---
author: krm
created: 2018-09-24
---
![se]
# Om exempel med javascript

Dessa exempel är inte tänkt att vara representiva eller användbara, utan:

  * Visa på olika sätt JS kan användas.
  * Visa roliga tillämpningar.
  * Visa interaktiva tillämpningar.

