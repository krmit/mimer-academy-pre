---
author: krm
created: 2018-09-24
links:
  - js-example-calculator
  - js-examples
---
![se]
# Javascript använda framför allt i webdocument

Det är kanske inte så konstigt vid tanke på dess historia.

  * js kan användas för att skapa bildspel.
  * För att validera indata i ett formulär.
  * För att få animeringar på sidan.
  * För att skapa interaktivitet.
  * För att ahntera ut och inloggningar.
  * Och mycket mer.

   
