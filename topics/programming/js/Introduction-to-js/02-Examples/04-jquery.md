---
author: krm
created: 2018-09-24
links:
  - jquery
  - jquery-examples
  - jquery-examples-more
---
![se]
# JQuery är ett biblotek för js på webben

JQuery var extrempt populärt när webläsarna var sämre. Har blivit mindre påopulärt men kan göra mycket kul.

  * Med JQuery kan du välja ut HTML element på samma sätt som i CSS.
  * På de elemnt som du valt kan du sedan göra modifikationer.
  * Detta blir ett enkelt sätt att göra mycket intressanta effekter.
  * Det finns ett stort antal plug-in till JQuery.
