---
author: krm
created: 2018-10-16
---
![se]
# Kombinera funktioner

Du kan skicka returnvärdet ifrån en funktion som parameter till en anna funktion.

  * Detta är rätt så vanligt.
  * Men kan vara svårt för en nybörjare.
  * Därför behöver du öva!
  * Exempel: *alert(Math.sqrt(16))*
  * Exempel: *alert(Math.sqrt(Math.sqrt(16)))*




