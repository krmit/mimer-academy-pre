---
author: krm
created: 2018-10-16
---
![se]
# Vad är en parameter?

Det är ett värde som skicka till en funktion.

  * Många funktioner kan ta emot värden när de anropas.
  * Exempel: *alert("Hello")*
  * Det är vanligt att vi behöver använda "." för att komma åt vår funktion. 
  * Exempel: *console.log("Hello again!")*

Det sist nämnda betyder att funktionen är definerat i ett objekt.


