---
author: krm
created: 2018-10-16
---
![se]
# Returnvärde

Många funktioner returnerar ett värde.

  * Det betyder att de lämnar ifrån sig ett värde efter det blivit anropade.
  * Exempel: *Math.sqrt(9)*
  * En funktion kommer att lämna ifrån sig som mest ett värde, aldrig mer. 




