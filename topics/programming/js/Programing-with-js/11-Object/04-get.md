---
author: krm
created: 2018-09-27
---
![se]
# Att få ett värde ifrån ett Object

Ett värde kan fås genom att helt enkel skriva namnet på egenskapen du behöver innan för hårda paranteser.

  * Exempel: *o["tre"]*
  * Exempel: *o.points + o[3]* 
  * Exempel: *o.tre* 







