---
author: krm
created: 2018-09-16
---
![se]
# Hur namnger du en variabel?

En variabel ska ha ett bra namn som förklarar vad den gör för något.

  * Använd gärna ett kort ord, men det få inte var förvillande.
  * Använd bara stora och små bokstäver a till z.
  * Du kan använda siffror om de är informativa i slutet av variabelnamnet.
  * Du kan inte använda mellanslag, använder "_" istället.

Att ge bra namn till variabler är det bästa sättet att dokumentera ditt program.



