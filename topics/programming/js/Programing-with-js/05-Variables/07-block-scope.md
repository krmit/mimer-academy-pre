---
author: krm
created: 2018-09-16
---
![se]
# Synlighet

En variabel är bara synlig i ett viss område. Kallat "block scope".

  * Ett "block scope" börjar med en "{"
  * Det avslutas sedan med ett "}"
  * Alla variblar som defineras i scopet kommer inte vara definerade utanför.
  * Denna typ av block använda påmånga ställen i js kod.




