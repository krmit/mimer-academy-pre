---
author: krm
created: 2018-09-16
---
![se]
# Vad är en Variabel?

En variabel är som en låda däe vi kan lägga våra värden.

  * För datorn är det ett sätt att hålla reda på olika minen.
  * I JS kan vi lägga vilka värden vi vill i vilket variabel som helst.
  * Att deklarera en variabel betyder att vi säger att vi ska använda en variabel med det namnet.
  * När vi deklarera en variabel ger vi den också andra egenskaper.
  * Här kommer vi deklarera variabler med koandona "let" och "const".

Du kommer i andras JS kod se att man ofta använder "var" för att deklara variabler, av olika orsaker vill vi undvika det här.




