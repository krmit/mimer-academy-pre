---
author: krm
created: 2018-10-16
---
![se]
# Jämförelse

Det vanligaste vilkoret är en enkel jämförelse av två värden.

  * x === y				Är sant om x är lika med y
  * x > y				Är sant om x är mindre än y
  * x < y				Är sant om x är mindre än y
  * Exempel: *1===1*, *"a"==="a"*, *1<2*, *1>0, *1<1* 


OBS: Det ska vara tre =, inte en för det är för tilldelning. Det ska heller inte vara två efter som det kan leda till konstigheter i visa fall.




