---
author: krm
created: 2018-10-17
---
![se]
# else if

Annar utför koden men bara om ytterligare ett vikor är uppfyld.

   * Ibland finns det behov att göra något beronde på ytterligare ett vilkor.
   * I vissa fall kan det vara bättre att använda ett switch-sats i dessa falld. 
  
```javascript
let tal = Number(prompt("Skriv ett tal mer än 5"));
if(tal > 5) {
    console.log("Bra, du vet vad som är större än 5.");
} else if(tal === 5) {
    console.log("Nej, det skulle vara _större_ än 5.");
} else {
    console.log("Du vet inte vad är större än 5.");
}
```




