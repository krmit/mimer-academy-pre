---
author: krm
created: 2018-10-16
---
![se]
# Vilkor

Du kan använda logiska operatorer för att skapa intressanta vilkor.

  * Du kan använda "och", "eller" och "inte" för att skapa vikor.
  * Example: *a%3 === 0 && a%5 === 0*
  * Example: *3 &lt; a && a &lt; 5*
  * Example: *a==="one" || a==="two"*




