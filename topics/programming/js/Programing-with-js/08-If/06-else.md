---
author: krm
created: 2018-10-17
---
![se]
# else

Annars om if-satsen är falskt utförs else istället.

   * Ofta finns det behov att göra något bara om vikoret är falskt.
  
```javascript
let tal = Number(prompt("Skriv ett tal mer än 5"));
if(tal > 5) {
    console.log("Bra, du vet vad som är större än 5.");
} else {
    console.log("Du vet inte vad är större än 5.");
}
```




