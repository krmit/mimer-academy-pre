---
author: krm
created: 2018-09-10
---
![se]
# Vad du behöver för att programmera JS

  * Du behöver en webläsare. 
  * Svårt att säga vilken som är bäst, men testa både Chrome och Firefox.
  * Du behöver en editor.
  * Notepad++ finns till Windows, men det går med vanliga Anteckningarna även om den inte är lika bra som andra alternativ.
  * Geany är en enkel editor till Linux, men vilke som följer med din disto är antagligen mycket bra.
  * Det finns hur många olika editorer som helst, testa några olika och se vilken du tycker om.



