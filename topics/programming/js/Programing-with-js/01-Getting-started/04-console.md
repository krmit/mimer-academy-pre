---
author: krm
created: 2018-10-14
---
![se]
# Utveckla med konsolen

Moderna webbläsare har en konsol vi kan använda.

  * Du kan öppna den genom att trycka *F12*
  * Alternativ kan du använda *Ctrl+Shift+I*
  * Du kan nu skriva js kod längst ned på skärmen.
  * Exempel: 1+1
  * Efter du trycker "Enter" kommer det visas 2 på skärmen.

Så när du har en webbläsare så kan du programmera i js. Gör det på skoj så ofta som möjligt!







