---
author: krm
created: 2018-10-14
---
![se]
# JS i HTML 

Vi kan också skapa en fil på hårdisken och döpa den till "prog.js". Öppna den i en editor och skriv:

```html
<script>
console.log("Hello World");
</script>
```

Nu kan du skriva kod mellan script tagarna.


