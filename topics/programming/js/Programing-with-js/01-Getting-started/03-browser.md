---
author: krm
created: 2018-10-14
---
![se]
# Gällande webläsare

Alla webläsare funker i princip. Men det är bäst att använda senaste version av Firefoxx och Chrome.

  * Firefox är öppen källkod och utvecklas av en stiftelse.
  * Chrome är också öppen källkod och utvecklas av Google.

OBS: Dessa browser fungera lika, men visa detalker kan skilja sig mellan dem och ha stor betydelse för hur ditt program är eller arbetsflöde.



