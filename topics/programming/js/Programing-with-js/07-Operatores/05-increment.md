---
author: krm
created: 2018-10-16
---
![se]
# Öka med ett

Det finns flera operator som är så kalla "syntactic sugar". De går att koda åp andra sätt men det blir färe tecken om du använder operatorn.

  * *v++*               Ökar en variabel v med ett. Samma sak som *v=v+1*
  * *v--*               Ökar en variabel v med ett. Samma sak som *v=v-1* 
  * *v+=3*              Ökar en variabel v med tre. Samma sak som *v=v+3*

Det finns många fler. Använd dem om du får enklare och trevligare kod, annars undvik dem. 



