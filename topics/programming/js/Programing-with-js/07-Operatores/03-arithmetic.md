---
author: krm
created: 2018-10-16
---
![se]
# Aritmetik

Alla matematiska tecken som du använder: "+", "-", "/" och "*" är operatorer.

  * De fungerar oftast som du förväntar dig när de får nummer som parametrar.
  * Example: *1+1* *2\*4* *-2/2*
  * Prioriteringsregler gäller även för dessa operatorer.
  * Example: *1+1* *2\*4* *-2/2*

OBS: "+" fungera även på *strings* med kommer då bara slå samman texterna. Exempel: "1"+"1"
  




