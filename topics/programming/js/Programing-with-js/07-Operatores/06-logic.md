---
author: krm
created: 2018-10-16
---
![se]
# Logik

Även boolska värden har några operator för sig.

  * *A && B*         Är *true* om *A* och *B* är *true*
  * *A || B*         Är *true* om *A* eller *B* är *true*
  * *!A*             Betyder helt enkelt inte *A*. Om *A* true så false. Om *A* false så *true*.
  * Exempel: *!false || false* *(true && true) || (false && false)*

Tillsammans med jämförelse kan man bygga upp anvacerade vilkor med hjälp av denna logik. 




