---
author: krm
created: 2018-10-16
---
![se]
# Modulus

Resten när du delar första talet med andra talet.

  * Om du har `3/4 = 1 + 3/4`, så är vår rest 3. 
  * Exempel: *4%3* *6%7* *15%3*
  * Denna operator är mycket användbara. Till exempel om du vill veta vilak tal som är delbara med 3.
  * Exempel: *9%3===0*
  




