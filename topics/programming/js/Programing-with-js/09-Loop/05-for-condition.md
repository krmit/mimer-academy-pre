---
author: krm
created: 2018-09-10
---
![se]
# Vilokor för for-satsen

I mitten står ett vilkor, eter varje varv kommer datorn se om vilkoret är sant, om det inte är det så avbryts loopen.

  * Om du räknar upp och använder "<" måste du du läga på ett för att räkna upp alla tal.
  
  ```javascript
 let tal = Number(prompt("Räkna upp till:"));
 for(let i = 0; i < tal+1; i++) {
      console.log(i);
 }
```



