---
author: krm
created: 2018-09-10
---
![se]
# Efter varje loop i for-satsen

Sist i for-satsen är kod som kommer utförsa efter varje varv i for-loopen.

  * Du kan ändra den kod för att få en anna steg längd på räknaren.

```javascript
 for(let i = 0; i < 20; i=i+2) {
      console.log(i);
 }
```


