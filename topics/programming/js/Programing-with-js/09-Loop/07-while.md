---
author: krm
created: 2018-09-10
---
![se]
# while

Är som en for-sats men har bara vilkoret.

  * Kan vara användbara om du inte har en räknare i koden.

```
while(prompt("En gång till?") === "ja") {
   console.log("Du är bäst!");
}

```
  




