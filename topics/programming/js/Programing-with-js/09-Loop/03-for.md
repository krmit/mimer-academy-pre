---
author: krm
created: 2018-09-10
---
![se]
# for

Är en sats som så länke vilkoret är sant kommer att repeter koden i satsen. Före koden körs förta gågne kommer kod att utföras och varje gång efter loopen har utförs .

  * Denna är en typ av vanliga for-sats som förekommer i många olika fall.
  
```javascript
 for(let i = 0; i < 10; i++) {
      console.log(i);
 }
```




