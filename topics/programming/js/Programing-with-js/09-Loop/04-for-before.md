---
author: krm
created: 2018-09-10
---
![se]
# Innan for-satsen

Koden som står först i for-satse, ofta "let i = 0" kommer att utföras en gång innan koden körs.

  * Om du vill att en räkning ska börja på ett anat tal, byter du bara ut nollan till det du vill.

```javascript
 for(let i = 5; i < 15; i++) {
      console.log(i);
 }
```




