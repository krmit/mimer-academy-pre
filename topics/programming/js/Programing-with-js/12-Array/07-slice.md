---
author: krm
created: 2018-09-27
---
![se]
# slice

Slice är en metod som retrnerar en del av en Array. Den tar två parameter: 

  * En start index där den nya array börjar. 
  * Ett slut index, den nya arrayen slutar innan det indexet.
  * Den ursprungliga arrayen ändras inte alls.

```javascript
let arr = ["a". "b", "c", "d"];
let short_arr = arr.slice(1,3);
console.log(short_arr);
// ["a". "b", "c", "d"]
```







