---
author: krm
created: 2018-09-27
---
![se]
# Push

Du kan lägga till ett värde sist i en Array genom att använd metoden *push*. 

```javascript
let arr=["a", "b"];
arr.push("c");
console.log(arr);
// ["a","b","c"]
```

Du kan ta bort sista värdet och returnera det genmom att använda metoden "pop".

```javascript
let arr=[0, 1, 2];
let zero = arr.shift();
console.log(arr);
// [1,2]
console.log(zero);
// 0
```






