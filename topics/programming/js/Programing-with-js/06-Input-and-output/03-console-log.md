---
author: krm
created: 2018-10-16
---
![se]
# console.log

I varje browser har du en dev konsol. I den kan du läsa utskrifter ifrån "consol.log".

  * Anropa med den text du vill skriva ut.
  * Exempel: *console.log("Hello World");*
  * Om du vill skriva ut flera nya rader använd tecknet som skrivs "\n"
  * Exempel: *console.log("Socrates\nPlato\nAristotle");* 

OBS: Det finns inget sätt att undvika att det bli en ny rad efter att *console.log* skrivit ut.




