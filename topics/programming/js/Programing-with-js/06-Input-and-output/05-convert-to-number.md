---
author: krm
created: 2018-10-16
---
![se]
# Konvera ett text till ett tal.

Du behöver ofta konvertera en text till tal.

  * Om du har en text som är ett tal och du vill använda som ett tal får du konvertera det.
  * Exempel: Number("3.14")
  * Exempel: Number("Bad number")
  * Exempel: let tal = Number(prompt("Skriv ett tal?"));

Det sista exempel är mycket vanligt om du vill att en användare ska skriva in ett värde.



