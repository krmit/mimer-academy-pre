---
author: krm
created: 2018-10-16
---
![se]
# Vad är input och output?

Grunderna för att program ska fungera är att vi kan ta in information till programmet och skicak ut det igen.

  * Input är det data som en användare ger till ett program.
  * Output är det som som programmet ger till en användare.




