---
author: krm
created: 2018-10-16
---
![se]
# prompt

Frågar användare om ett värde och returnärar sedan det.

  * Du kan också skicka med en fråga som en parameter.
  * Exempel: *let name = prompt("Vad är ditt namn?");*
  * Du kan alltid skicka värdet direkt till en annan funktion.
  * Exempel: *console.log(prompt("Eko?"))*




