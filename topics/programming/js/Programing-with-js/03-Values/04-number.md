---
author: krm
created: 2018-09-10
---
![se]
# Number

Är ett tal, det kan både vara ett heltal eller ett decimaltal.

  * Om du vill skriva ett tal så bara skriver du det.
  * Exempel: *3* *3.14* *1024*
  * OBS: Du skriver tal på amerikanskt vis med "." istället för ","

Detta skiljer JS från många andra språk där man har flera olika typer för heltal eller ett decimaltal.



