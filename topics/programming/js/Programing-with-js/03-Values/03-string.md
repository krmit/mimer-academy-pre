---
author: krm
created: 2018-10-14
---
![se]
# String

Är en text, det vill säga en lista av bokstäver.

  * Texter skrivs innanför "-appostrofer.
  * Exempel: *"Heoo World!"*
  * Alternativ kan man använda en '-appostrof.
  * Exempel: *'Heoo World!'*
  




