---
author: krm
created: 2018-10-14
---
![se]
# Vad är ett värde?

Ett värde är representaton av information i datornsmine.

  * Värdet kan alltid skrivas direkt i JS koden. Testa i konsolen.
  * Att två saker ser lika ut för oss mäniskor betyder inte att de är samma sak för datorn.
  * Datorn bryr sig bara om hur värdet faktiskt är sparat i minnet.
  * Men när värde presenteras för oss görs det på ett sätt som är lätt att läsa.
  * Detta kan ge upppjov till missförstånd. Pröva tillexempel att skriva *0.1+0.2* i konsolen.
