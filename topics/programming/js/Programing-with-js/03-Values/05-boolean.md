---
author: krm
created: 2018-09-10
---
![se]
# Boolean

Är antingen sant eller falskt, inget annat.

  * Ett värde är: *true*
  * Det andra är: *false*
  * Används ofta indirekt när vi ska jämföra saker eller skriva vilkor.




