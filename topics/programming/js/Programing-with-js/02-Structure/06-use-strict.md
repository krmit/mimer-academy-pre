---
author: krm
created: 2018-10-16
---
![se]
# Vi använder strikt mode.

JS har många design val som anses konstiga. Vi kan undvika några av dem genom att skriva i strikt mode. 

  * Börja din kod genom att skriva "use strict".
  * Nu kommer all din kod vara i strikt mode.




