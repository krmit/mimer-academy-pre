---
author: krm
created: 2018-10-16
---
![se]
# Kommmentera koden

Vi får inte glömma kommentarerna.

  * Kommentarer sysn i koden men de kommer ignoreras när koden körs.
  * Vill du kommentera bort en rad skriv *//*
  * Vii du kommentera bort flera rader börja med */\**
  * Du avslutar sedan kommenteren med *\*/*
  * Du kan kommentera bort kod när du vill testa.
  * Du kan också kommentera för att förklara koden.
  
Men kommentera inte för mycket! Förutom i träningsyfte bör du inte kommentera din kod så mycket. Se till att den är välorganiserat istället.








