---
author: krm
created: 2018-09-10
---
![se]

# JavaScript

JavaScript är ett dynamiskt programmeringspråk som är lätt att lära sig men svårt att bemästra. JS har syntax ifrån C och inspiration ifrån funktionella språk som Lisp. Ett mer korrekt namn JavaScript är EcmaScript, men kan också kallas för js. 
