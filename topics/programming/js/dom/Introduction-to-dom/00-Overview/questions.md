---
author: krm
created: 2018-09-24
questions:
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short
    - type: short

---
![se]

  1. Beskriv med egna ord vad DOM är för något?
  2. Vad står forkortningen DOM för?
  3. Vad står forkortningen BOM för?
  4. Hur kommer det sig att du kan använda metoder i objectet windows som om de vore vanliga funktioner?
  5. VAd är skillnaden mellan en node och ett element?
  6. Nämn några sätt som du kan välja ut ett element ifrån ett dokument?
  7. Om du vill få ett värde som ett attribute har. Vilket metod ska du använda då?
  8. Om du vill ändra innehållet i en node i dokumentet hur gör du då?
  9. Om du vill att något ska ske när någon klickar på element. Hur ska du göra då?
 10. Vilken metod ska använda om du vill göra något på din hemsida som ska uprepas med en kort paus på din hemsida?
