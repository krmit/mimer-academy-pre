by: krm
---

# Lösning med for-loopar

Definera en heltals variabel "factor" som ska vara **1**.

Börja med att definera en funktion **Absolut**.
Funktionen ska ta en heltals parameter **Tal**. 
I funktionen gör följande:
    Om **Tal** är större än 0:
        Returnera **Tal**.
    Annars
        Returnera **-Tal**.
       
