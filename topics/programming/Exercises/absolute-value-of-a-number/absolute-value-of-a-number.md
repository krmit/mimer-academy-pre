---
author: Magnus Kronnäs
credit: 
    -krm 
    -sweecrow 
created: 2017-08-23
level: easy
type: exercise
id: EFM1
status: beta
tags: 
    -function 
    -math
---
![en]
# Absolut value of a number

Make a function that calculates the absolute amount of a decimal number. You can choose the appropriate data type yourself, but you can not use a function from the math library.
   
A user should be able to test your function.

![se]
# Absolutbelopp av en funktion

Gör en funktion som beräknar absolutbeloppet av ett decimaltal. Du kan själv välja passande datatyp, men du får inte använda dig av en funktion från math bibliotek.

En användare ska kunna testa din funktion.
