---
author: David Ström
credit: 
    -David Ström 
    -krm
created: 2018-04-25
level: hard
type: exercise
id: NM1
status: alpha
tags: 
    -math
---
![se]
# Köpa tuppar, höns och kycklingar

En person ska köpa 100 fåglar för 100 kr. Tupp: 5 kr/st, höna 3 kr/st och tre kycklingar för 1 kr. Minst en av varje (annars kan man även köpa 25 hönor och 75 kycklingar). Hur många fåglar av resp. sort kan personen köpa?
 
