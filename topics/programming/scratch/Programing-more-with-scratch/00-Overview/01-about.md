---
author: krm
created: 2018-09-10
---
![se]
# Om Scratch programmering i detta kapitel

  * Scratch är ett språk som är lätt att lära sig.
  * Det bästa är att lekfullt experimentera med scratch.
  * Strukturen på kapitel är till för att undersöka alla grundläggande koncept av scratch.
  * Samma struktur kan du sedan använda dig för att lära dig andra programmeringspråk. 





