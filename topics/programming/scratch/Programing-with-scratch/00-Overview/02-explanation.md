---
author: krm
created: 2018-09-10
---
![se]
# Vad är scratch?

Scratch är ett visuellt programmeringspråk som är lätt att lära sig.

  * Det är ett "riktigt" programmeringsspråk.
  * Det är dock inte praktiskt användbart till många applikationer.
  * Språket har visa problem som diskuteras sist i kapitlet.
  * En anna viktigt del är att det har en utvecklar sajt där programmeringen sker.







