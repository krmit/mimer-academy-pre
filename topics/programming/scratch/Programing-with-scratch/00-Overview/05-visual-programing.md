---
author: krm
created: 2018-09-10
---
![se]
# Vad är visuel programmering?

Visuel programmering är när du använder ett språk som är helt visuellt för att programmera.

  * Allt kod representeras visuellt ofta som block och/eller ikoner.
  * Det finns en inbyggda program där du kan manipulera din kod.
  * Visuella programmering förekommer innnom industrin, men anses för opraktiska för mer avacerade program.







