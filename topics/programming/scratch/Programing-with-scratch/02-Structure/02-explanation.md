---
author: krm
created: 2018-09-10
---
![se]
# Förklaring till Struktur 

  * Välj “My Stuff” ifrån din profil.

*** 

För att börja programmera i Scratch väljer du “My Stuff” ifrån din profil och sedan “New Project”. Du får nu se webbsidan där vi ska programmera.Vi kan kalla den för vår “Editor”. 

I Scratch visas alla resultat upp i ett grafikfönster som kallas scen. Programmet är sedan uppbyggt av sprites som kan visas upp på scenen. Varje sprite har en “kostym” som är den bild som visas upp där spriten är. Du har också en speciell sprite som är bakgrunden till scenen. 

  * Pröva att skap en ny sprite genom att trycka på sprite ikonen vid katten. Flytta omkring dem på scenen och lek lite.

https://scratch.mit.edu/projects/238998602/ 

Du startar ett program genom att trycka på den gröna flaggan. Då kommer du inte längre kunna flytta på sprite då.

Du kan också rita egna kostymer och importera bilder från din dator som kan bli en kostym. En sprite har en position på scen och också en riktning som den pekar mot.
