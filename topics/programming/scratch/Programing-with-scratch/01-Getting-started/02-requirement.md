---
author: krm
created: 2018-09-10
---
![se]
# Vad du behöver för att programmera Scratch

  * Du börjar programmera Scratch genom att registera sig på deras hemsida.
  * Du behöver bara en vanlig webläsare.
  * Du behöver inte installera något på din dator.
  * Men om du inte använder Scratch 3 så måste du ha flash på din webläsare.




