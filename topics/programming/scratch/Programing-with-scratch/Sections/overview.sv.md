# En översikt över Scratch

# 👁️‍🗨️ Om Scratch programmering i detta kapitel

  * Scratch är ett språk som är lätt att lära sig.
  * Det bästa är att lekfullt experimentera med scratch.
  * Strukturen på kapitel är till för att undersöka alla grundläggande koncept av scratch.
  * Samma struktur kan du sedan använda dig för att lära dig andra programmeringspråk. 

# ❓ Vad är scratch?

Scratch är ett visuellt programmeringspråk som är lätt att lära sig.

  * Det är ett "riktigt" programmeringsspråk.
  * Det är dock inte praktiskt användbart till många applikationer.
  * Språket har visa problem som diskuteras sist i kapitlet.
  * En anna viktigt del är att det har en utvecklar sajt där programmeringen sker.

# 📜 Historia

Scratch är skapat på *MIT Media Lab*.

  * Första versionen kom 2002
  * Version 1.0 kom 2007 och var ett skrivbordsprogram.
  * Version 2.0 kom 2013 och var baserad på flash.

# Hur programmerar man i scratch?

Språket är visueltt och det mesta gör du genom att dra och klicka. 

  * Du registerar dig på scratch hemsida.
  * Du skriver program som innehåller en grafisk presentation.
  * I ditt program finns det: 
    * Block som representerar ett kommando.
    * En scen som visar upp det som finns i programmet.
    * En sprite är ett grafisk objekt som programmeraren styr.
  
# Vad är visuel programmering?

Visuel programmering är när du använder ett språk som är helt visuellt för att programmera.

  * Allt kod representeras visuellt ofta som block och/eller ikoner.
  * Det finns en inbyggda program där du kan manipulera din kod.
  * Visuella programmering förekommer innnom industrin, men anses för opraktiska för mer avacerade program.
