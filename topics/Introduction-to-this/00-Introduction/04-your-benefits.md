---
author: krm
created: 2018-07-24
---
![se]

# Dina fördelar

Jag tänkte man skulle ha en sådan här sida för om du tror läromedlet är bra så blir det bra. Men tro inte allt detta är sant :)

  * Du kan lära dig allting.
  * Detta läromdel kan följa dig genom hela ditt liv.
  * Du kan skapa ditt eget läromedel.
  * Den gör dig till en superhjälte!
