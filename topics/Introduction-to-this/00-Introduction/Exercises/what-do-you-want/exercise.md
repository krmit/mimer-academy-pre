---
author: krm
created: 2018-08-17
level: normal
status: alpha
type: writting
tags: 
    -evaluation
---
![se]
# Vad är det du vill göra?

Beskriv vad det du vill uppnå med ditt liv. Vad är det du vill göra när du är klar med din utbildning?

# Redovisning

Skriv en halv A4(250 ord) om detta.
