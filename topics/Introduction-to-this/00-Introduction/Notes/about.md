---
author: krm
created: 2018-06-28
---
![se]
# Om

Detta läromedel började skriva av Magnus Kronnsä("krm"). De första idérna går tillaka till 2010 eller tidigare, men i sin nuvarande form påbörjades arkivet att skrivas i slutet av vårterminen 2018 som förberedelse inför hösterminen 2018. Det bygggde mycket på det läromdel krm skrivit efter skolverket ändrade kursplanerna för många IT ämnen under sommaren 2017. Men en del är också hämtat ifrån saker som krm skrev åren innnan dess. Se också (Mimers Academins historia)[../01-Mimer-academy/history]. 


