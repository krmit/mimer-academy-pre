---
author: krm
created: 2018-08-16
---
![se]

# Välkommen till detta undevisning material!

Varmt välkommen till detta läraomedel! Vi som skrivit det hopppas att du kommer uppsakata det. Du är fri att använda det på vilket sätt du vill och du får gärna göra fötbättringar om du vill.

Detta läromedel är helt inriktat på att lära ut saker. Att göra en kompliserad värd förstålig och lätt att lära. Detta försöker vi åstakomma genom att samla länkar till olika resuser på internet som kan fungera som hjälp för dig att förstå ett ämne bättre.

