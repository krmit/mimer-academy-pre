---
author: krm
created: 2018-06-28
notes:
  - welcome 
  - about
links:
  - homepage
  - starting-off-rigth
---

![se]

# Om Akademin

- Är en samling pedagogisk material.
- Updelat i olika ämnen och kapitel.
- Innehåller många länkar till andra resurser.
- Innehåller många frågor och övningar.
- I kombination med projektet "gy11-kurser" kan det användas som läromedel för många IT kurser.
