---
author: krm
created: 2018-06-29
questions:
    - type: yes-or-no
      answer: true
    - type: year
      answer: 2018
    - type: yes-or-no
      answer: false
    - type: free-text
    - type: multi-choose
      answer: 1
    - type: yes-or-no
      answer: [false, true]
---
![se]

  1. I detta arkiv kommer vi diskutera ämnesmål?
  2. Vilket år började detta akriv skriva i sin nuvande form?
  3. Kommer arkivet någons sin bli klart?
  4. Vad vill du att detta läromedel ska innehålla?
  5. Vad betyder "kollaborativt"?
      1. Att sammarbeta för något.
      2. Att utveckla via konkurens.
      3. Att försöka göra något roligt.
      4. Något som alltid ska undvikas.
  6. Kan denna kurs göra dig till en superhjälte?
