---
author: krm
created: 2018-06-29
---

![se]

# Varför detta läromdel?

- Detta läromdel är helt inriktat på att lära dig saker på bästa sätt.
- Detta läromedel är inte begränsad utan kommer kunna innehålla allt du behöver veta i ett ämne.
- Detta läromedel är helt grattis och du kan använda det fritt.
- Du kommer alltid ha tillgång till det och kan återkomma till även efter skolan.
- Du kan alltid själv förbättra detta läromedel om du tycker det är dåligt.
