---
author: krm
created: 2018-07-24
---
![se]

# Om strukturen på läromedlet

Läromedlet är uppdelat i följande delar, som förklaras i detalj senare i detta kapitel.

  * Ämnen("Topics") - Är en samlinga av kapitel som har ett gemensamt tema.
  * Kapitel("Chapter") - Är en pedagogisk model för något som ska läras ut.
  * Avsnitt("Session") - Är en updelning av kapitel i små enheter som följer efter varandra.
  * Sidor("Slides") - Tillsammans utgör det en kort sammanfattade presentation av något som ska sägas inom ett ämen.
