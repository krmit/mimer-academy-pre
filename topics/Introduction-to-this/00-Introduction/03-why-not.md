---
author: krm
created: 2018-06-29
---
![se]

# Varför inte något annat läromedel?

Jag har läst en hel del olika läromedel för gymnasiet och det jag ogillar
  * Påstår sig följa kursplanen, men 
     * Motiverar inte sin tolkingar.
     * Ignorerar visa mål och kriterier.
     * Inkluderar material som inte ingår i kursmaterial.
  * Inaktuell kunskaper, inom it-ämnen kan ett par år var väldigt lång tid.
  * Kompliserade ämnen trivialiseras, eftersom man inte vill försöka ge förklaringar som inte alla förstår.
  * Höga kostnader, bland annat pga. det ofta är ett tryckt medium.
  * Utnyttgar inte de många bra gratis resurser som finns på nätet.

