---
author: krm
created: 2018-06-29
---
![se]
# Så bra som möjligt

Den är bara inriktat på att lära ut på ett så bra sätt som möjligt.

  * Ingen hänsyn tas till eventuella skolmyndigheters åsikter.
  * Vill man använda detta i en kurs så får man plocka ut de delar som passar den kursen utifrån sin tolkning av kursen.
  * Se mitt andra projekt (gy11-courses)[] för en sådan tolkning.
