"use strict";
const fs = require("fs-extra");
const mark = require("mark-twain");
const matter = require("gray-matter");
const toHTML = require("onml").stringify;
const format = require("pretty");
const glob = require("glob");
require("colors");

const jsome = require("jsome");
const path = require("path");
const {md2jsonML} = require("./toJsonML.js");

module.exports.renderTopic = renderTopic;
function renderTopic(topicPath) {
	
const topic_path = topicPath;
const chapter_list = glob.sync(topic_path+"/[A-Z]*/").filter((dir) => {
	return !(dir.endsWith("Exercises/") || dir.endsWith("Links/") || dir.endsWith("Notes/"))});
const topic_list = glob.sync(topic_path+"/[a-z]*/");

const info_path = path.resolve(topic_path, "topic.md");
const info_text = fs.readFileSync(info_path, { encoding: "utf-8" });
const info_jsonml = md2jsonML(info_text, "se");
const title = info_jsonml[2][1];
const description = info_jsonml[3][1];

const revale_jsonml = [
  "html",
  [
    "head",
    ["meta", { charset: "utf-8" }],
    [
      "link",
      { rel: "stylesheet", href: "/tools/node_modules/flag-icon-css/css/flag-icon.css" }
    ],
    [
      "link",
      { rel: "stylesheet", href: "https://use.fontawesome.com/releases/v5.2.0/css/all.css" }
    ],
    [
      "link",
      { rel: "stylesheet", href: "/a/tools/css/basic.css" }
    ]
  ],
  [
    "body",
    ["div", {"id":"container"},
    [
      "h1", title 
    ],
    [
      "p", description
    ],
    [
      "h1", "Ämnen" 
    ],
    ["ol"],
    [
      "h1", "Kapitel" 
    ],
    ["ol"]
    ]
  ]
];

if(topic_list.length !== 0) {
for (let topic of topic_list) {

const topic_path = path.join(topic, "topic.md");
const topic_text = fs.readFileSync(topic_path, { encoding: "utf-8" });
const topic_jsonml = md2jsonML(topic_text, "se");
const topic_title = topic_jsonml[2][1];
const topic_description = topic_jsonml[3][1];

const topic_path_array = topic.split("/");
const topic_name = topic_path_array[topic_path_array.length-2];
const topic_url = "./"+topic_name+"/index.html";
revale_jsonml[2][1][5].push(["li", ["a", {"href": topic_url}, topic_title], ["p", topic_description]])

}
}
if(chapter_list.length !== 0) {
for (let chapter of chapter_list) {
const chapter_path = path.join(chapter, "chapter.md");
const chapter_text = fs.readFileSync(chapter_path, { encoding: "utf-8" });
const chapter_jsonml = md2jsonML(chapter_text, "se");
const chapter_title = chapter_jsonml[2][1];
const chapter_description = chapter_jsonml[3][1];

const chapter_path_array = chapter.split("/");
const chapter_name = chapter_path_array[chapter_path_array.length-2];
const chapter_url = "./"+chapter_name+"/index.html";
revale_jsonml[2][1][7].push(["li", ["a", {"href": chapter_url}, chapter_title], ["p", chapter_description]])

}
}


const topic_html_path = path.join(topic_path, "index.html");

// This is because of bug in onml stringify that not give correct html.
fs.writeFileSync(topic_html_path, format(toHTML(revale_jsonml))
.replace(/<script src="(.+)" \/>/, 
'<script src="$1"></script>')
.replace(/<i class="(.+)" \/>/g, '<i class="$1"></i>'));
}

if (require.main === module) {
    renderTopic(path.resolve(__dirname, process.argv[2]));
}

