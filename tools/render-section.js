"use strict";
const fs = require("fs-extra");
const mark = require("mark-twain");
const matter = require("gray-matter");
const toHTML = require("onml").stringify;
const format = require("pretty");

require("colors");

const jsome = require("jsome");
const path = require("path");

const {
  md2jsonML,
  md2slideJsonML,
  md2noteJsonML,
  md2questionJsonML
} = require("./toJsonML.js");

module.exports.renderSection = function renderSection(sectionPath) {
  const revale_jsonml = [
    "html",
    [
      "head",
      ["meta", { charset: "utf-8" }],
      [
        "link",
        {
          rel: "stylesheet",
          href: "/a/tools/node_modules/reveal.js/css/reveal.css"
        }
      ],
      [
        "link",
        {
          rel: "stylesheet",
          href: "/a/tools/node_modules/reveal.js/css/theme/night.css"
        }
      ],
      [
        "link",
        {
          rel: "stylesheet",
          href: "/a/tools/node_modules/flag-icon-css/css/flag-icon.css"
        }
      ],
      [
        "link",
        {
          rel: "stylesheet",
          href: "https://use.fontawesome.com/releases/v5.2.0/css/all.css"
        }
      ],
      ["link", { rel: "stylesheet", href: "/a/tools/css/basic.css" }],
      [
        "link",
        {
          rel: "stylesheet",
          href: "/a/tools/node_modules/reveal.js/lib/css/zenburn.css"
        }
      ],
      ["script", { src: "/a/tools/node_modules/reveal.js/js/reveal.js" }],
      [
        "script",
        {
          src:
            "//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.13.1/build/highlight.min.js"
        }
      ]
    ],
    [
      "body",
      ["div", { class: "reveal" }, ["div", { class: "slides" }]],
      [
        "script",
        'window.onload = function() {Reveal.initialize({controlsTutorial: false,width: "150%",height: "200%",margin: 0.1,minScale: 0.5,maxScale: 0.5});};hljs.initHighlightingOnLoad();'
      ]
    ]
  ];

  const lession_path = sectionPath;
  const notes_path = path.resolve(lession_path, "Notes");
  const question_path = path.resolve(lession_path, "questions.md");
  const section_list = fs.readdirSync(lession_path);

  for (let file of section_list) {
    const file_array = file.split(".");
    const file_name = file_array[0];
    const file_prefix = file_array[1];
    let section_list = [];
    try {
      if (!isNaN(Number(file.slice(0, 2)))) {
        const file_path = path.join(lession_path, file);
        console.log(file_path);
        const text = fs.readFileSync(file_path, { encoding: "utf-8" });
        const section_jsonml = md2slideJsonML(
          text,
          lession_path,
          "se",
          Number(file.slice(0, 2))
        );
        revale_jsonml[2][1][2] = revale_jsonml[2][1][2].concat(
          section_jsonml.slice(2)
        );
      }
    } catch (err) {
      console.log("\nSection file is: " + file + "\n");
      throw err;
    }
  }

  if (fs.existsSync(question_path)) {
    console.log(question_path);
    const text = fs.readFileSync(question_path, { encoding: "utf-8" });
    revale_jsonml[2][1][2] = revale_jsonml[2][1][2].concat(
      md2questionJsonML(text, "se").slice(2)
    );
  }
  const lession_html_path = path.join(lession_path, "index.html");

  // This is because of bug in onml stringify that not give correct html.
  fs.writeFileSync(
    lession_html_path,
    format(toHTML(revale_jsonml))
      .replace(/<script src="(.+)" \/>/g, '<script src="$1"></script>')
      .replace(/<i class="(.+)" \/>/g, '<i class="$1"></i>')
      //Since oml sometimes make relly bad stuff.
      .replace(/width: "150% "/g, 'width: "150%"')
      .replace(/height: "200% "/g, 'height: "200%"')
  );
};

if (require.main === module) {
  renderSection(path.resolve(__dirname, process.argv[2]));
}
