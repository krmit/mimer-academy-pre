#!/usr/bin/node
"use strict";
const fs = require("fs-extra");
require("colors");
const glob = require("glob");
const { exec } = require('child_process');
const { readdirSync, statSync } = require('fs');
const { join, resolve } = require('path');
const moment = require("moment");
require("colors");
const jsome = require("jsome");
const yaml = require("js-yaml");
const path = require("path");

const Enquirer = require('enquirer');
const enquirer = new Enquirer();
 
enquirer.register('checkbox', require('prompt-checkbox'));
enquirer.register('list', require('prompt-list'));

const file_path = path.join(__dirname, "/links.yml");
const links_text = fs.readFileSync(file_path, { encoding: "utf-8" });
const links_data = yaml.safeLoad(links_text);


var questions = [

  {
    name: 'link',
    message: 'What is the link?',
  },
  {
    name: 'name',
    message: 'What is the name of the link?',
  },
  {
    name: 'nameSE',
    message: 'Vad är det svenska namnet på länken?',
  },
    {
    name: 'author',
    message: 'How was it created?',
    default: 'krm'
  },
   {
    type: 'list',
    name: 'type',
    message: 'What is the type of link?',
    default: 'wikipedia',
    choices: links_data.type
    },
 {
    type: 'list',
    name: 'instruction',
    message: 'What is the reading instruction?',
    default: 'about',
    choices: links_data.instruction
    },
 {
    type: 'list',
    name: 'language',
    message: 'What is the language?',
    default: 'gb',
    choices: links_data.lang
    },
 {
    type: 'checkbox',
    name: 'tags',
    message: 'What has it for tags?',
    choices: links_data.tags}
]
 
enquirer.prompt(questions)
  .then(function(answers) {
	answers.created = moment().format("YYYY-MM-DD");
    let text =
`---
author: ${answers.author}
created: ${answers.created}
link: ${answers.link} 
type: ${answers.type}
instruction: ${answers.instruction}
language: ${answers.language}
tags:
`
for(let tag of answers.tags) {
    text += `  - ${tag}\n`
}

text +=`---
![se]

# ${answers.nameSE}    

![gb]

# ${answers.name}
`;
let name = answers.name[0].toLowerCase()+answers.name.slice(1);
let file_path = "./"+name.split(" ").join("-")+".md";
return fs.writeFile(file_path, text);
  });


