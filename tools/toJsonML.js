"use strict";
const fs = require("fs-extra");
const mark = require("mark-twain");
const matter = require("gray-matter");
const format = require("pretty");
const moment = require("moment");

require("colors");

const jsome = require("jsome");
const path = require("path");

module.exports.md2jsonML = md2jsonML;

function md2jsonML(text, lang) {
  const matter_object = matter(text);
  const doc_jsonml = mark(matter_object.content).content;
  let tmp_jsonml = [[], {}];
  let result_jsonml = [];

  for (let i = 0; i < doc_jsonml.length; i++) {
    if (
      doc_jsonml[i][0] == "p" &&
      doc_jsonml[i][1].hasOwnProperty("type") &&
      doc_jsonml[i][1].type == "imageReference"
    ) {
      if (doc_jsonml[i][1].identifier === lang) {
        tmp_jsonml = result_jsonml;
      } else {
        tmp_jsonml = [];
      }
    } else {
      tmp_jsonml.push(doc_jsonml[i]);
    }
  }
  const result = ["markdown", matter_object.data].concat(result_jsonml);
  return result;
}

module.exports.md2slideJsonML = function(text, rootPath, lang, id) {
  const result_jsonml = md2jsonML(text, lang);
  let doc_jsonml = result_jsonml.splice(2, result_jsonml.length - 2);
  let extra_note = [];

  let counter = 0;
  for (let node of doc_jsonml) {
    counter++;
    if (node[0] === "p") {
      node.splice(1, 0, { class: "slideText" });
    }

    if (node[0] === "hr") {
      extra_note.push(["h1", { class: "nodeHeadline" }, doc_jsonml[0][1]]);
      extra_note = extra_note.concat(doc_jsonml.slice(counter));

      for (let n of extra_note) {
        if (n[0] === "p") {
          n.splice(1, 0, { class: "nodeText" });
        }

        if (n[0] === "ul") {
          n.splice(1, 0, { class: "nodeList" });
        }
      }
      doc_jsonml = doc_jsonml.slice(0, counter - 1);
      break;
    }
  }

  if (result_jsonml[1].image !== undefined) {
    const image_element = ["img", { src: "Image/" + result_jsonml[1].image }];
    doc_jsonml.push(image_element);
    if (result_jsonml[1]["image-size"] !== undefined) {
      const image_size = result_jsonml[1]["image-size"];
      image_element[1].class =
        "image" + image_size.charAt(0).toUpperCase() + image_size.slice(1);
    }
  }

  if (result_jsonml[1].time !== undefined) {
    const time_element = [
      "p",
      { class: "slideTime" },
      time2JsonML(result_jsonml[1].time)
    ];
    doc_jsonml.splice(1, 0, time_element);
  }

  result_jsonml[2] = ["section", { id: id }];

  if (
    result_jsonml[1].hasOwnProperty("notes") ||
    result_jsonml[1].hasOwnProperty("links") ||
    extra_note.length > 0
  ) {
    result_jsonml[2].push(["section"].concat(doc_jsonml));
    if (extra_note.length > 0) {
      result_jsonml[2].push(["section"].concat(extra_note));
    }
    if (result_jsonml[1].hasOwnProperty("notes")) {
      for (let note_name of result_jsonml[1].notes) {
        const file_path = path.join(rootPath, "Notes", note_name + ".md");
        const text = fs.readFileSync(file_path, { encoding: "utf-8" });
        result_jsonml[2].push(
          ["section"].concat(md2noteJsonML(text, rootPath, lang).slice(2))
        );
      }
    }
    if (result_jsonml[1].hasOwnProperty("links")) {
      for (let link_name of result_jsonml[1].links) {
        const file_path = path.join(rootPath, "Links", link_name + ".md");
        const text = fs.readFileSync(file_path, { encoding: "utf-8" });
        result_jsonml[2].push(
          ["section"].concat(md2linkJsonML(text, rootPath, lang).slice(2))
        );
      }
    }
  } else {
    result_jsonml[2] = result_jsonml[2].concat(doc_jsonml);
  }
  return result_jsonml;
};

module.exports.md2noteJsonML = md2noteJsonML;

function md2noteJsonML(text, rootPath, lang) {
  const result_jsonml = md2jsonML(text, lang);

  const doc_jsonml = result_jsonml.splice(2, result_jsonml.length - 2);

  for (let node of doc_jsonml) {
    if (node[0] === "p") {
      node.splice(1, 0, { class: "nodeText" });
    } else if (node[0] === "h1") {
      node.splice(1, 0, { class: "nodeHeadline" });
    }
  }

  return result_jsonml.concat(doc_jsonml);
}

function md2linkJsonML(text, rootPath, lang) {
  const result_jsonml = md2jsonML(text, lang);
  let doc_jsonml = result_jsonml.splice(2, result_jsonml.length - 2);

  for (let node of doc_jsonml) {
    if (node[0] === "p") {
      node.splice(1, 0, { class: "linkText" });
    } else if (node[0] === "h1") {
      node.splice(1, 0, { class: "linkHeadline" });
      node[2] = ["a", { href: result_jsonml[1].link }, node[2]];
    }
  }

  let type;

  switch (result_jsonml[1].type) {
    case "video":
      type = "fa-video";
      break;
    case "site":
      type = "fa-sitemap";
      break;
    default:
      type = "fa-bug";
  }

  //This code must be made language neutral.
  let instruction;
  let description;
  switch (result_jsonml[1].instruction) {
    case "fun":
      instruction = [
        "p",
        ["strong", "Underhållning:"],
        "Denna länk är bara för skoj."
      ];
      break;
    case "read":
      instruction = [
        "p",
        ["strong", "Läs:"],
        "Läs igenom denna text och fundera ingenom den. Ställ eventuella frågor."
      ];
      break;
    default:
      instruction = [
        "p",
        ["strong", "Inga:"],
        "Denna länk har inga instruktioner, gör vad du vill."
      ];
  }

  let container = ["div", ["div", { class: "linkContainer" }]];
  let container_insert = container[1];

  // Bug some where, probably front-end, will mess up div:s, theirfor the extra div.
  doc_jsonml.push(container);
  container_insert.push(["i", { class: "linkTyp fas " + type }]);
  container_insert.push([
    "div",
    { class: "linkLang flag-icon flag-icon-" + result_jsonml[1].language }
  ]);

  if (result_jsonml[1].tags.length === 1) {
    container_insert.push([
      "div",
      { class: "linkTags" },
      result_jsonml[1].tags[0]
    ]);
  } else {
    container_insert.push([
      "div",
      { class: "linkTags" },
      result_jsonml[1].tags.join(", ")
    ]);
  }
  doc_jsonml.push(["div", { class: "linkInstruction" }, instruction]);

  return result_jsonml.concat(doc_jsonml);
}

module.exports.md2questionJsonML = md2questionJsonML;

function md2questionJsonML(text, lang) {
  const result_jsonml = md2jsonML(text, lang);
  const doc_jsonml = result_jsonml
    .splice(2, result_jsonml.length - 2)[0]
    .splice(1);

  result_jsonml[2] = ["section", ["h1", "Frågor!"]];

  let index = 0;

  for (let node of doc_jsonml) {
    node[1].splice(1, 0, { class: "questionText" });

    if (result_jsonml[1].questions[index].image !== undefined) {
      const image_node = result_jsonml[1].questions[index];
      const image_element = ["img", { src: "Image/" + image_node.image }];
      node.push(image_element);
      if (image_node["image-size"] !== undefined) {
        const image_size = image_node["image-size"];
        image_element[1].class =
          "image" + image_size.charAt(0).toUpperCase() + image_size.slice(1);
      }
    }
    result_jsonml[2].push(["section"].concat(node.slice(1)));

    for (let n of node) {
      if (n[0] === "ol") {
        n.splice(1, 0, { class: "questionList" });
      }
    }
    index++;
  }

  return result_jsonml;
}

module.exports.time2JsonML = time2JsonML;

function time2JsonML(time) {
  jsome(typeof time);
  if (typeof time === "string") {
    return time;
  } else if (typeof time === "number") {
    return time;
  } else if (typeof time === "object") {
    if (time instanceof Date) {
      return moment(time).format("YYYY-MM-DD");
    } else if (time.hasOwnProperty("during")) {
      return "Under: " + time.during;
    } else if (time.hasOwnProperty("after") && time.hasOwnProperty("before")) {
      return time.after + " - " + time.before;
    } else {
      return "No implementation for this type of date.";
    }
  } else {
    return "No implementation for this type of date.";
  }
}
