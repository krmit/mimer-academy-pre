#!/usr/bin/node
"use strict";
const fs = require("fs-extra");
require("colors");
const glob = require("glob");
const { exec } = require("child_process");

const { readdirSync, statSync } = require("fs");
const { join, resolve } = require("path");

const { renderChapter } = require("./render-chapter.js");
const { renderSection } = require("./render-section.js");
const { renderTopic } = require("./render-topic.js");

const section_dirs = glob
  .sync(__dirname + "/../topics/**/[A-Z]*/[0-9]*/")
  .filter(dir => {
    return !(
      dir.includes("/Exercises/") ||
      dir.endsWith("Links/") ||
      dir.endsWith("Tutorials/") ||
      dir.endsWith("Notes/") ||
      dir.endsWith("Image/") ||
      dir.endsWith("Examples/")
    );
  });

console.log("\nSections:\n".green.bold);

for (let section of section_dirs) {
  console.log(section.green);
  renderSection(section);
}

const chapter_dirs = glob
  .sync(__dirname + "/../topics/**/[A-Z]*/")
  .filter(dir => {
    return !(
      dir.endsWith("Exercises/") ||
      dir.endsWith("Links/") ||
      dir.endsWith("Tutorials/") ||
      dir.endsWith("Notes/") ||
      dir.endsWith("Image/") ||
      dir.endsWith("Examples/")
    );
  });

console.log("\nChapters:\n".green.bold);

for (let chapter of chapter_dirs) {
  console.log(chapter.green);
  renderChapter(chapter);
}

const topic_dirs = glob
  .sync(__dirname + "/../topics/**/[a-z]*/")
  .filter(dir => {
    return !dir.includes("/Exercises/");
  });

topic_dirs.push(__dirname + "/../topics/");

console.log("\nTopics:\n".green.bold);

for (let topic of topic_dirs) {
  console.log(topic.green);
  renderTopic(topic);
}

console.log("\nTutorials:\n".green.bold);
const tutorials_dirs = glob.sync(__dirname + "/../topics/**/Tutorials/");

for (let tutorial of tutorials_dirs) {
  console.log(tutorial.green);
  process.chdir(tutorial);
  exec(`ghmd *.md`);
}

console.log("\nOther:\n".green.bold);

console.log("Create index.html".green);
process.chdir(__dirname + "/..");
exec(`ghmd index.md`);
