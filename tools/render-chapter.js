"use strict";
const fs = require("fs-extra");
const mark = require("mark-twain");
const matter = require("gray-matter");
const toHTML = require("onml").stringify;
const format = require("pretty");
const glob = require("glob");

require("colors");

const jsome = require("jsome");
const path = require("path");

const {md2jsonML,md2slideJsonML, md2noteJsonML, md2questionJsonML} = require("./toJsonML.js");

module.exports.renderChapter = function renderChapter(chapterPath) {
	
const chapter_path = chapterPath;
const section_list = fs.readdirSync(chapter_path);

const title_path = path.resolve(chapter_path, "chapter.md");
const title_text = fs.readFileSync(title_path, { encoding: "utf-8" });
const section_jsonml = md2slideJsonML(title_text, chapter_path, "se");
const title = section_jsonml[2][2][1];

const revale_jsonml = [
  "html",
  [
    "head",
    ["meta", { charset: "utf-8" }],
    [
      "link",
      { rel: "stylesheet", href: "/a/tools/node_modules/flag-icon-css/css/flag-icon.css" }
    ],
    [
      "link",
      { rel: "stylesheet", href: "https://use.fontawesome.com/releases/v5.2.0/css/all.css" }
    ],
    [
      "link",
      { rel: "stylesheet", href: "/a/tools/css/basic.css" }
    ]
  ],
  [
    "body",
    ["div", {"id":"container"},
    [
      "h1", title 
    ],
    ["ol"]
    ]
  ]
];

for (let section of section_list) {

if (!isNaN(Number(section.slice(0, 2)))) {
const section_path = path.resolve(chapter_path, section);
const slide_list = fs.readdirSync(section_path);

const title_path = path.join(section_path, "00-title.md");
const title_text = fs.readFileSync(title_path, { encoding: "utf-8" });
const section_jsonml = md2slideJsonML(title_text, section_path, "se");
const title = section_jsonml[2][2][1];


const slide_ol = ["ol"];
const section_url = "./"+section+"/index.html";
revale_jsonml[2][1][3].push(["li", ["details", ["summary", ["a", {"href": section_url}, title]], slide_ol]]);

let section_is_empty=true;

for (let file of slide_list) {
  
  if (!isNaN(Number(file.slice(0, 2))) && file !=="00-title.md") {
	  section_is_empty=false;
    const file_path = path.join(section_path, file);
    const text = fs.readFileSync(file_path, { encoding: "utf-8" });
    const section_jsonml = md2slideJsonML(text, section_path, "se", Number(file.slice(0, 2)));
    const slide_url = section_url+"#/"+Number(file.slice(0, 2));
    
    if(section_jsonml[2][2][0]==="section") {
		
    slide_ol.push(["li", ["a", {"href":slide_url}, section_jsonml[2][2][1][1]]]);
} else {
    slide_ol.push(["li", ["a", {"href":slide_url}, section_jsonml[2][2][1]]]);
}

}
  }
  if(section_is_empty) {
	slide_ol.push(["li", "Inte klart ännu."]);
}
}
}

// Tutorials

const tutorials = glob.sync(chapter_path +  "/Tutorials/*.md");

if(tutorials.length !== 0) {

const tutorial_jsonml =  [[
      "h2", { id: "tutorialHeadline" }, "Guider" 
    ],
    ["ol", { id: "tutorialList" }]
    ];

let ol_list = tutorial_jsonml[1]

for(let tutorial of tutorials) {
    const text = fs.readFileSync(tutorial, { encoding: "utf-8" });
    const tutorial_url = "./Tutorials/"+tutorial.split("/").slice(-1)[0].split(".")[0]+".html";
   ol_list.push(["li",["a", {"href":tutorial_url}, mark(text).content[1][1]]])
}
revale_jsonml[2][1] = revale_jsonml[2][1].concat(tutorial_jsonml);
}

//Examples

const examples = glob.sync(chapter_path +  "/Examples/*");

if(examples.length !== 0) {

const examples_jsonml =  [[
      "h2", { id: "tutorialHeadline" }, "Exempel" 
    ],
    ["ol", { id: "tutorialList" }]
    ];

let ol_list = examples_jsonml[1]

for(let example of examples) {
    const example_file = example.split("/").slice(-1)[0];
    let example_name = example_file.split(".")[0].split("-").join(" ");
    example_name = example_name.charAt(0).toUpperCase() + example_name.slice(1);
   ol_list.push(["li",["a", {"href":"./Examples/"+example_file}, example_name]])
}
revale_jsonml[2][1] = revale_jsonml[2][1].concat(examples_jsonml);
}
//jsome(revale_jsonml);

const lession_html_path = path.join(chapter_path, "index.html");

// This is because of bug in onml stringify that not give correct html.
try{
fs.writeFileSync(lession_html_path, format(toHTML(revale_jsonml))
.replace(/<script src="(.+)" \/>/, 
'<script src="$1"></script>')
.replace(/<i class="(.+)" \/>/g, '<i class="$1"></i>'));
}
catch (err){
	console.log(lession_html_path.red)
	jsome(revale_jsonml);
    console.log(err);
} 
}


if (require.main === module) {
    renderChapter(path.resolve(__dirname, process.argv[2]));
}
