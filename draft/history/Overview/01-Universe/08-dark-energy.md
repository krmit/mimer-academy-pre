---
author: krm
created: 2018-05-05
---
![sv]
# Mörk energi
  * Universum expanderar allt snabbare.
  * Detta måste drivas av en energi.
  * Mörk energi motsvarar 73% av universum.
