---
author: krm
created: 2018-05-06
---
# Svart hål

  * Så kompakta att inte ljus kan lämna objektet.
  * Tiden står stilla!
  * Händelsehorisont är “point of no return”.
