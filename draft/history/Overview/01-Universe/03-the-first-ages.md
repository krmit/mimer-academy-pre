---
author: krm
created: 2018-05-03
time: 1 s ebb
links:
   - chronology-of-the-universe
---
![sv]
# De första tidsåldrarna

  * 10^-43 s ebb Gravitationen blir en egen kraft.
  * 10^-36 s ebb Elektromagnetism blir en egen kraft.
  * 10^-32 s ebb Inflation(extrem expansion) av universum avslutas.
  * 10^-12 s ebb Kvarkar bildas och naturlagarna liknar dagens.
  * 1 s      ebb Protoner och neutroner bildas.
  * 3 min.   ebb Väteatomer börjar bildas.

