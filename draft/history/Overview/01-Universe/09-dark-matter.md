---
author: krm
created: 2018-05-05
---
![sv]
# Mörk massa
  * Massa som inte är synlig i någon våglängd.
  * Mörk materia utgöt 23% av Universium.
  * Det synliga universum är altså bara 4%.
  * Det mesta vet vi inget om.
