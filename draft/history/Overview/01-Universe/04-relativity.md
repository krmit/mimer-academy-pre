---
author: krm
created: 2018-05-03
time: 1 s ebb
image: einstein
---
![sv]
# Något om relativitetsteori

  * Ljuset har en konstant hastighet.
  * Tid, avstånd och massa kan observeras på olika sätt beroende på hastighet och gravitation.
  * Alla observationer är lika mycket värda.


