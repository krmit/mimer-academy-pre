---
author: krm
created: 2018-05-07
---
# Supernova

  * En exploderande stjärna.
  * Oerhörda mängder energi.
  * Om en supernova förekommer inom 100-200 ljusår innebär det jordens undergång.
