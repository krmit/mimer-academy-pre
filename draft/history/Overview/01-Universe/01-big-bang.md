---
author: krm
created: 2018-05-02
image: IHU1
time: 13.7 GYA
---
![sv]
# Big bang

  * All started with a big bang.
  * Singulariteter med oändlig densitet.
  * Där allt även tid är förenad i ett.
  * Sedan ökar avståndet mellan partiklarna mycket snabbt.
