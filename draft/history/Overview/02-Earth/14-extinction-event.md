---
author: krm
created: 2018-05-16
time: 65 MYA
---
![sv]
# Massutdöende

  * Fem gånger har de flesta stora djur dött ut.
  * Senaste gången bidrog en metroid.
  * Ger möjlighet till nya djurarter komma fram.
