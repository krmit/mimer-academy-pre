---
author: krm
created: 2018-06-21
time:
   after: 3.4 MYA
   befor: 3.3 KYA
---
![sv]
# Stenåldern

  * Tillverkade verktyg av flinta (i Asien bambu).
  * Flinta är vast och hårt.
  * Kräver mycket skicklighet för att tillverka.
  * Kräver till gång till bra råmaterial.
