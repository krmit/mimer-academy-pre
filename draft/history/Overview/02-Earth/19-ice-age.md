---
author: krm
created: 2018-05-20
time: 2.6 MYA
---
![sv]
# Istid

  * Istiderna har avbrott
  * Den senaste istiden slutade för 10 000 år sedan.
  * Norra delen av halvklotet täcks av stora glacierer.
  * Har stor betydelse för landskapet framför allt i Sverige.
  
