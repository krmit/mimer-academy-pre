---
author: krm
created: 2018-05-16
time: 60 MYA
---
![sv]
# Däggdjur

  * Djur som ammar. (Mammal)
  * Jämnvarma.
  * Första kommer 225 MÅS.
  * Får ett uppsving när de flesta dinosaurierna dör.
  * Ny jätte däggdjur kommer fram.
