---
author: krm
created: 2018-05-13
time: 175 MYA
---
![sv]
# Pangea

  * Sista gången vi hade en jättekontinent
  * Skapades för 335 MÅS och uplöstes för 175 MÅS.
  * Påverkar klimatet, efter Pangea blir det kallare.
