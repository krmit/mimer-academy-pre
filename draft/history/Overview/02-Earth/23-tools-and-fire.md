---
author: krm
created: 2018-06-21
time: 1.5 MYA
---
![sv]
# Verktyg och eld

  * När mäniskan börjar använda eld förändras mycket.
  * Det blir lättare att få tag på bra och näringsriktig mat.
  * Det göra att mer energi kan användas till hjärnan och mgsmältningen förändras.
  * Ökat använding av vertyg får också stor betydelse.
  * Framför allt använing av bärhjälpmedel så som korgar.
