---
author: krm
created: 2018-05-20
time: 2.6 MYA
---
![sv]
# Nedkylning av jorden

  * Börjar under den nuvarande geologiska perioden kvartär 
  * Det skapar en "istid", med kortare varmare perioder.
  * Många olika orsaker, men inga bevis för vilka som är avgörande.
     * På vilket sätt jordenslandmass är i förhållande till varandra.
     * Atmosfärens sammansättning
     * Variatoner i jordensbanna kring solen.
     * Ändringar i hur mycket av solljuset som reflekteras.
