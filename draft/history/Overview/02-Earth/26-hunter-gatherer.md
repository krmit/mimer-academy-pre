---
author: krm
created: 2018-06-24
time: 200 KYA
---
![sv]
# Jägare-samlare

  * Den ursprungliga livsstilen för människor.
  * Mycket fritid, 4 timmars arbete per dag. 
  * Kräver ständig rörelse, därför Nomader. 
  * Varje människa kräver stora ytor för att få mat.
