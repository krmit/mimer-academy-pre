---
author: krm
created: 2018-06-18
time: 2.9 MYA
---
![sv]
# Människor

  * Männsikosläktet skapas när mäniskoapor i afrika börjar gå på savanen.
  * Det finns idag bara en art av mäniskor kvar.
  * Under historien har flera arter utvecklas ifrån äldre.
  * Dessa har sedan dött ut.
    * Homo Habilis                2,5 MÅS
    * Homo Erectus                1,8 MÅS
    * Homo Neanderthalensis       250 TÅS
    * Homo Sapiens                200 TÅS
  
