---
author: krm
created: 2018-05-12
time: 3 GYA
---
![sv]
# Cyanobakterier
  * Fotosyntes
  * Skapar syre från koldioxid.
  * Ändrar jorden fullständigt!
  * Olika oxider
  * Syrehalt runt 21%
  * Möjliggör annat liv.
