---
author: krm
created: 2018-05-13
time: 270 MYA
---
![sv]
# Plattektonik och omformningen av värden.

  * Jorden är uppbyggt av plattor som flyter omkring.
  * Jorden har sett olika ut genom tiden.
  * Vid flera tillfällen har det funnits en jättekontinent.
