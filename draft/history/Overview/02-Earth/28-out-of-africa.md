---
author: krm
created: 2018-06-24
time:
   after: 80 KYA
   befor: 60 KYA
---
![sv]
# Utvadring ifrån Afrika

  * Mäniskor lämnar Afrika och sprider sig över värden.
  * Osäkert när och i vilken omfattning man beblandade sig med äldre mäniskoarter.
  * Istiden gör det möjligt att ta sig till Amerika.
  * Homo sapiens sprider sig över hela jorden.
  * Andra människoarter dör ut och många andra djur med.
