---
author: krm
created: 2018-05-01
---
lang: sv
# Om tid

  * Tänk dig att vikten av ett riskorn är ett år.
  * Ett riskorn väger 1/64 gram.
  * I sådan fall börjar vår historia för 210 ton år sedan!
  * Eller med andra ord för två blåvalar sedan.
  * Det är väldigt länge sedan.

