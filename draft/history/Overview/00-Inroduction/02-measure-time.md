---
author: krm
created: 2018-05-01
---
lang: sv
# Hur vi räknar tid

  * Riskorn är inte ett bra sätt att räkna.
  * Vad som passar bäst beror på vad vi diskuterar
  * För universium: 1 000 000 000 år sedan = 1 GÅS
  * För evolution: 1 000 000 år sedan = 1 MÅS
  * För tidig historia: 1 000 år sedan = 1 KÅS
  * För senare historia: 1 år före vår tidnränkning = 1 fvt 
  * För senare historia: 1 år efter vår tidnränkning = 1 evt
  * För tid direkt efter big bagn: 1 s efter big bang = 1 s ebb


[#Detta/är/en/länk]
[#VEF1]
[En beskrivning av länken][Länk]
