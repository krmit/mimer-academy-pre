---
id: 
description:
theme: 
---
![sv]

# Historia

Ämnet handlar om vilka vi är och varför vi är här. Dett utifrån att titta på vad som har hänt tidigare. Definitionen av historia är att vi måste åtminstone delvis utgår ifrån skriftliga källor. Men här inkluderar vi även sådan som har hänt innan olika mänskliga kulturer utvecklade skrift konst. Detta kallas ofta för för-historia. Även översikter av kosmologi och evolution räknar vi hit.

Historia kan vara mycket känslomässigt. Många  har sin identitet baserad på olika historiaks händelser.
