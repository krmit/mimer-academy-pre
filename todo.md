# Att göra nu

## Datateknik
  * Skriv avsnitt om editor
  * Skriv Instruktioner för fakta prov.
  * Skriv Inlämningsuppgift om terminal kommandon och OS installation.

### Nästa
    
### Senare

## Programmering 2
  * Kompletera kapitlet om git.
  * Skriv kapitel om closure
  * Skriv Inlämningsuppgift om terminal
  
### Nästa

### Senare

## Programmering 1
  * Skriv Inlämningsuppgift om programmering

### Nästa
  
### Senare
 * Skapa kapitel om Scratch
 * Dela upp lektionerna i kapitel Introduction-to-programming till tre olika om spel. Förrsta om programmering, nästa om computional thinking och sista om olika spel.
 * I Introduction-to-programming lägg till ett avsnitt om andra resurser.

## Webbserverprogrammering
 * Skriv Instruktioner för fakta prov.
 * Skriv Inlämningsuppgift om nodejs
 
### Nästa

### Senare 
  * Komplettera Introduction-to-web/01-History, dela upp den i tre delat: socialt, webläsare och webserver.
  * Lägg till ett kapitel i overview om cookies.
  * Gör ett bättre demo om webbklienter

## Elektronik

### Nästa

### Senare 
