# Programmering i JS en översikt

Om att skapa enkla program med javascript.

Om du vill lära dig att programmera är det viktigaste att öva och att öva mycket.

Ta och testa kod nedan i en konsol i din webläsare så att du ser vad den gör. Du får fram konsolen genom att trycka "F12".

## Struktur

  * Ett program består av kod och kommentarer.
  * Kommentarer förklarar koden
  
```
Kommentarer kommenterar koden
//Den var bra

/* Den var bra
 *
 *  Lång kommentar
 */

```

  * Varje kod rad utför ett eller flera kommandon och går sedan till nästa rad.

```js
let a=3
a=a+1
console.log(a)
```

  * Du kan avsluta en rad med ett semikolon “;”, men inte om det är ett kommando som fortsätter på nästa rad, t.ex. efter if och for satser.

```js
a=2*a+1;
console.log(a);

// Inte som du tror, kommer skriva ut "hej" 
if(2<1); {
   console.log("hej");
}
```
  * Kommandon fungerar så att det tar emot värden, så kallade parametrar, och returnerar ett värde, så kallade returvärde.
```js
math.sqrt(9);
```

  * Tänk på att kommandona ofta anropar varandra, ett returvärde ifrån ett kommando blir nästa kommandos parametrar.
```js
console.log(math.pow(2+1, math.sqrt(9)));
```

  * Läs kod genom att gå igenom varje rad och fundera på vad varje kommando tar emot och returnerar.
  * Rader kan sedan delas upp med så kallade fiskmås paranteser {}.
  
## Värden

  * Det finns tre olika enkla typer av värden i JS
    * Texter var typ kallas _String_:
    * Att något är en text och inget kommando markeras med "-apostrof eller med '-apostrof.
```js
"hej" 
'adjö' 
"1"+"1"
```
  * Tal vars type kallas "Number"
    * Är både heltal och decimaltal.
    * Skrivs på engelska form med “.” istället för “,”,  t.ex. 3.14
    * Om du har en String som motsvara ett tal. Kan du omvandla det till ett tal genom att skriva skicka det till funktionen _Number_.
```js
Number("3.14") === 3.14;
```

  * Om du vill ha ett ascii värde istället, så använder du metoden _charCodeAt_. In parametern är vilken bokstav som ska omvandlas till ASCII.
```js
   let letter="abc".charCodeAt(0);
   // Kommer skriva ut "97"
   console.log(letter)
```    

  * Sanningsvärden som kallas Boolean
    * Har bara två värden true eller false.
```js
true;
console.log(false);
```

## Variabler
  * Alla variabler bör deklareras.
  * Det betyder helt enkelt att vi definerar dem, vilket gör att vi kan undvika många olika sorters fel.
  * Tills vidare kan du deklare variablar antingen mer _let_ eller _var_.

```js  
let a = 3;
```
 * Variabelnamn ska beskriva värdet som de innehåller.
 * Du kan använd _ i ett namn.
 
```js  
// Några exempel på bra variabel namn
let start_time;
let my_money;
let target;
let filename;
```

## Skriva ut
  
  * För att skriva ut något använder vi funktionen _console.log()_

```js
console.log("hej")
```

  * Observera att om du vill skriva ut flera saker kan du sätt ihop en längre mening med __+__. Allt omvandlas till en String och skrivs ut.
```js
console.log("God "+"dag!");
```
  * Om du vill skriva ut en ny rad får du göra det med ett special tecken, nämligen "\n".
```js
  console.log("God dag!\nHej då!");
```

## Läsa in

  * För att läsa in en String i en variabel.
```js
let text = prompt();
```

  * För att läsa in ett tal, observera att talet måste parsas med hjälp av _Number_ funktionen.
```js
let tal = Number(prompt());
```

## Matematik

  * Du kan använda js likt än miniräknare. Testa!
```js
1+1
4+(2*3)
5/2
```
  * Det finns många bra matematiska funktioner.  Till exempel:
    * _Math.sqrt(9);_
    * _Math.sin(0);_
    * _Math.cos(1);_
    * _Math.PI;_ Är inte en funktion utan en konstant variabel som är bra att ha.
    * _Math.pow(3,2);_
  * Det finns också ett para operatorer förutom +, -. *, och /. 
    *   __%__ ger ifrån sig resten vid division. Så om 4%3 blir 1.
      * Mycket mer användbar pga. olika uppräkningar som behöver göras.
      * _x%2===0_ betyder att x är ett jämt tal.
    * __\*\*__ betyder upphöjd i. _3**2===9_  


## if-satser
  * Villkor är ett uttryck som utvärderas till antingen sant eller falskt.
  * För att visa att något är samma använder vi “===”.
```js
3===3
“hej”===”då”
```
  * Vi kan också använda olikheter.

```js
4>3
3>3
3<4
3>=3
3>=3
4<=4
```
  * Vi kan också föra samman olika villkor med operatorn &&("och") och ||("eller").
```js
3===3 && 4>=3
false || 1===1
```

  * En if-sats består utav ett villkor och om detta är sant så utförs kod som står mellan måsvingarna.
```js
if(2>1) {
    console.log(“Efter ett kommer två”);
}
```

  * Nästan alltid har du variabler i en if-sats.

```js
let a=”a”;
if(a===”a”) {
    console.log(“aaaaaaaaaaaaaaaaaaaaa”);
}
```
  * En _if-sats_ kan också följas av en _else_ sats som utför om _if_ är falskt.

```js
let tal=Number(prompt(“Skriv 1”));
if(tal===1) {
    console.log(“Rätt!”);
} else {
    console.log(“Fel!”);
}
```

  * En if-else-sats kan i sin tur följs av en till if sats. 

```js
let tal=Number(prompt(“Skriv 1”));
if(tal===1) {
    console.log(“Ett!”);
} else if(tal===2) {
    console.log(“Två!”);
}
else {
    console.log(“Alla andra tal!”);
}
```

## For-loopar
  * Består av ett startuttryck, som ofta ger ett startvärde till en variabel.
    * startsats, görs precis innan loopen. Ofta _i=0_, _0_ är det tal som vi börjar räkna ifrån. 
    * vilkor, loopen utörs så länga detta är sant. Ofta _i<11_, _11_ är det tal som vi räknar mot men stannar innan vi kommit till.
    * eftersats, utförs efter varje loop. Ofta _i++_ som innebär att i ökar med ett i arje repetion.
  * Test


```js
for(let i = 0; i < 10;i=i++) {
      console.log(i);
}
```

  * Vi kan ha många olika varianter på denna loop.
  * En loop som räknar ned.

```js
for(let i = 10; i > 0;i=i-1) {
      console.log(i);
}
```

  * En loop som skriver ut alla jämna tal ifrån 4 till 16.
      

 ```js
for(let i = 4; i < 17;i=i+2) {
      console.log(i);
}
```

  * Vi kan ockås ha flera _for-loopar_ i varandra.
  
```js
for(let i = 0; i < 10;i++) {
     for(let j = 1; j < 10;j++) {
         console.log(i*j);
     }
     console.log("\n");
}
```

## Funktioner

  * Du kan deklarera dinna egna funktioner.

```js
function myFunktion(myNumber) {
    return "My number is: "+myNumber;
}
```
  * Ovan är:
    * _myFunktion_ är namnet på funktionen.
    * _myNumber_ är namnet på inparametern.
    * Det utryck som kommer efter __return_ kommer att returneras av funktionen.
  * Vi kan ha många olika former på våra funktioner.
  * Fler inparametrar:
```js
function myFunktion(x,y,z) {
    return x**2+y**2+z**2;
}
```
  * Sidoeffekter.
```js
function dublifierare(myNumber) {
            let result = myNumber*2;
            console.log(result);
    return result;
}
```

## Listor

  * En _array_ i js motsvara en lista av olika värden.
```js
let My_Array=[1,2,3. "Ute", "ska", "du", "vara nu"]
```  
  * Observera att vi börjar räkna ifrån 0.
  * Vill du ha ett element ur listan anger du vilket nummer den har.
```js
   // Returnera 1
   My_Array[0];
      // Returnera "Ute"
   My_Array[3];

```   
  * Du ändrar ett värde genom att sätta ett elementet lika med det.

```js
   My_Array[0]=4;
```

## Object

  * Ett object har egenskaper som består av två delar, en nyckel och ett värde.
```js
{"namm":"krm", "yrke":"lärare", "poäng":10000}
```

 * Du kan få ett  värde genom att ge kommandot:
```js
   // Returnera 1
   My_Array["name"];

```

 * Du kan sätta ett  värde genom att ge kommandot:
```js
   My_Array["name"]="demon";
```

## Problemlösning

  * Se till att du förstår problemet. Vad ska resultatet bli?
  * Dela upp problemet i mindre hanterbara delar.
  * Bestämd dig för en del som du börjar jobba med.
    * Försök förstå vad som ska göras.
    * Tänk igenom hur detta ska göras steg för steg.
    * Skriv kod för varje steg.
    * Testa din kod. 
  * Stämmer det överens med din förståelse? Om inte börja om med att försöka förstå vad som ska göras. Se också nedan.
  * Om du inser att din uppdelning av problemet är fel, börja om genom att försöka förstå problemet.  
  * Annars fortsätt med en annan del av problemet.
  * När du är klar undersöka om uppgiften stämmer överens med det som efterfrågades.

## Utveckling
  * Du laddar om en sida genom  att skriv F5. Tänk på att du måste ladda om en sida om du ändrar kod.
  * Tänk på att du kan starta en utveckling konsol genom att trycka på F12.
  * Läs igenom felmeddelanden noggrant.
  * Titta på den rad som anges i felmeddelandet.
  * Läs denna rad noggrant.
  * Om det är ett parentes fel kan det var en rad ovanför som orsakar felet. Gå igenom alla parenteser, editorn borde markera parenteser som hör ihop.
  * Om felmedelande är långt med många fel ifrån ett bibliotek, så är det fortfarande din kod som antagligen har orsakat felet. Antingen genom att din kod har fel men som upptäcks förs i biblioteket eller att du använder bibliotekets API på ett felaktigt sätt.
