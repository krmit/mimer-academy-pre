Idé - En idé om vad som får oss näramre det vi vill genom att förstå vad som sker.
Implentera - Skriv koden som gör detta. Anvädnd lämpliga verktyg och läs alla felmedellanden.
Testa - På olika sätt, både med de värden vi förstå tänka och andra som kan vara intressanta.
Analysera - Försöka att förstå varför vi får det resultat vi har fått.

I alla dessa faser så gör du hela tiden sökningar på internet för att försöka se om du kan hitt lösning åp de problem som uppstår.
